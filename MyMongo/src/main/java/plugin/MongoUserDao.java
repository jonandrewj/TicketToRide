package plugin;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

import java.util.HashSet;
import java.util.Set;

import model.Account;
import util.Serializer;

/**
 * Created by Andrew Jacobson on 8/3/2017.
 */

public class MongoUserDao {

    // COLLECTION OF USERS FROM THE TICKET TO RIDE DATABASE
    MongoCollection<Document> userCollection;

    public MongoUserDao(MongoDatabase database){
        userCollection = database.getCollection("users");
    }

    public void addUserAccount(Account account){
        String json = Serializer.toJson(account);
        userCollection.insertOne(Document.parse(json));
    }

    public Set<Account> getUserAccounts(){
        Set<Account> accounts = new HashSet<>();

        MongoCursor<Document> cursor = userCollection.find().iterator();
        try {
            while (cursor.hasNext()) {
                String json = cursor.next().toJson();
                accounts.add((Account)Serializer.fromJson(json, Account.class));
            }
        } finally {
            cursor.close();
        }

        return accounts;
    }

    public void clear(){
        userCollection.drop();
    }
}
