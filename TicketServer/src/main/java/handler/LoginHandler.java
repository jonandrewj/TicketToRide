package handler;

import java.io.IOException;
import request.LoginRequest;
import response.LoginResponse;
import service.LoginService;
import util.Serializer;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class LoginHandler extends HandlerBase {

    /**
     * Creates a new instance.
     */
    public LoginHandler() {
    }

    /**
     * Handles the request.
     * @param body the request body.
     * @return the server's response.
     * @throws IOException if something goes wrong while processing the request.
     */
    @Override
    public String customHandle(String body) throws IOException
    {
        LoginRequest request = (LoginRequest) Serializer.fromJson(body,LoginRequest.class);
        LoginResponse response = LoginService.login(request);
        return Serializer.toJson(response);
    }

}
