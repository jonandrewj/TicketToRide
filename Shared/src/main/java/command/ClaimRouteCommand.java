package command;

import model.Color;
import model.MapRoute;

/**
 * Created by kendall on 7/20/2017.
 */

public class ClaimRouteCommand extends Command implements ILoggable {
    private MapRoute route;
    private String username;
    private String gameID;
    private Color colorToUse;

    /**
     * crates a claim route command.
     * @param route the route to claim.
     * @param username the user that's claiming the route.
     * @param gameID the id of the game.
     */
    public ClaimRouteCommand(MapRoute route, String username, String gameID, Color colorToUse) {
        super();

        this.route = route;
        this.username = username;
        this.gameID = gameID;
        this.colorToUse = colorToUse;
    }

    /**
     * Executes this command, creating a game in the current context.
     */
    @Override
    public Boolean execute()
    {
        return getContext().claimRouteForGame(route,gameID,username,colorToUse);


    }

    @Override
    public String getGameId() {
        return gameID;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getLogMessage() {
        return "claimed the route from " + route.getCity1().getName() + " to " + route.getCity2().getName();
    }
}
