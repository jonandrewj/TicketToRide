package plugin;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.MongoClientURI;

import java.util.List;
import java.util.Set;

import command.ICommand;
import model.Account;
import model.Game;

/**
 * Created by Andrew Jacobson on 8/3/2017.
 */

public class MongoDB implements iDaoAPI {

    // CLASS VARIABLES -----------------------------------------------------------------------------
    MongoClient mongoClient;
    MongoDatabase database;

    MongoGameDao gameDao;
    MongoUserDao userDao;
    MongoGameCommandDao gameCommandDao;
    MongoCommandQueueDao commandQueueDao;


    public MongoDB(){
        
        // CONNECT TO THE MONGO ATLAS SERVER
        MongoClientURI uri = new MongoClientURI(
            "mongodb://jonandrewj:athens95mongo@tickettoride-shard-00-00-qolad.mongodb.net:27017,"
            + "tickettoride-shard-00-01-qolad.mongodb.net:27017,tickettoride-shard-00-02-qolad"
            + ".mongodb.net:27017/tickettoride?ssl=true&replicaSet=TicketToRide-shard-0&authSource=admin"
        );
        
        // THE DEFAULT CONSTRUCTOR FOR A MONGO CLIENT
        // SETS IP TO LOCALHOST AND PORT TO 27017 -- DEFAULTS FOR MONGODB---------------------------
        mongoClient = new MongoClient(uri);

        // GET THE DEFAULT DATABASE FOR TICKET TO RIDE
        database = mongoClient.getDatabase("tickettoride");

        // CREATE THE 4 SUB-DAOS
        gameDao = new MongoGameDao(database);
        userDao = new MongoUserDao(database);
        gameCommandDao = new MongoGameCommandDao(database);
        commandQueueDao = new MongoCommandQueueDao(database);

    }

    // OVERRIDE METHODS FROM iDaoAPI ---------------------------------------------------------------

    @Override
    public void addGame(Game game) {
        gameDao.updateGame(game);
    }

    @Override
    public void updateGames(List<Game> games) {

        // UPDATE THE GAMES
        for (Game g : games){
            gameDao.updateGame(g);
        }


        // NOW THAT IT'S UPDATED -- MOVE ALL THE COMMANDS FROM THE TEMP QUEUE TO THE REAL QUEUE
        List<ICommand> tempCommands = gameCommandDao.getGameCommands();
        gameCommandDao.clear();
        for (ICommand command : tempCommands){
            commandQueueDao.addCommand(command);
        }
    }

    @Override
    public List<ICommand> getDeltaCommands() {
        List<ICommand> deltaCommands = gameCommandDao.getGameCommands();
        gameCommandDao.clear();
        return deltaCommands;
    }

    @Override
    public void addDeltaCommand(ICommand command) {
        gameCommandDao.addCommand(command);
    }

    @Override
    public Set<Account> getUserAccounts() {
        return userDao.getUserAccounts();
    }

    @Override
    public void addUserAccount(Account account) {
        userDao.addUserAccount(account);
    }

    @Override
    public List<ICommand> getCommandQueue() {
        return commandQueueDao.getCommandQueue();
    }

    @Override
    public void selfDestruct() {
        userDao.clear();
        gameDao.clear();
        gameCommandDao.clear();
        commandQueueDao.clear();
    }

    @Override
    public List<Game> getGames() {
        return gameDao.getGames();
    }
}
