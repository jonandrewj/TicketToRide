package model;

import java.util.List;

import command.ICommand;

/**
 * Created by deriktj on 7/9/2017.
 */

public interface IModelFacade
{

    // PHASE ONE
    Boolean addGame(Game game);
    Boolean addPlayerToGame(String username,String gameID);
    Boolean startGame(String gameID);
    Boolean applyCommand(ICommand command);
    //Boolean removeGame(String gameId);
    Boolean drawTrainCard(String gameID, String username, int index, Color color, Color colorToReplace);

    Boolean drawDestinationCard(String gameID, String username, String city1, String city2, Integer points);

    Boolean discardDestinationCards(String gameID, String username, List<DestinationCard> cardsToDiscard);

    Boolean addChatMessageForGame(String gameID, String message, String username);

    // will fail if the player doesn't have the right cards
    // will discard the cards for the user
    Boolean claimRouteForGame(MapRoute route, String gameID, String username, Color colorToUse);

    Boolean startTurn(String gameId, String username);

    void setDestinationScoreForPlayer(String gameID, String username, int completeScore, int incompleteScore);
    void setLongestRouteScoreForPlayer(String gameID, String username, int score);
    void updateDeckSize(String gameID, int size);

    Boolean changeGameState(String gameId, Game.Gamestate newState);


}
