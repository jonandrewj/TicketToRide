package asynctask;

import android.os.AsyncTask;

import model.facade.PreGameModelFacade;
import serverproxy.ServerFacade;
import model.data.ClientModel;
import model.Game;
import response.ResponseBase;
import fragment.GameSelectFragment;

/**
 * Created by Alyx on 7/9/17.
 */

public class JoinGameTask extends AsyncTask<GameSelectFragment, Integer, Boolean> {
    /** Join result, contains the result of the join attempt */
    private ResponseBase joinResponse;

    /** The authorization token returned from the server upon successful authentication */
    private String authToken;

    /** Access the model */
    private ClientModel clientModel;

    /** Holds the user's username & number of players requested for game */
    private String username;
    private Game game;


    /** The fragment that called this task */
    private GameSelectFragment caller;

    /** Constant for one player */
    private final int ONE_PLAYER = 1;


    public JoinGameTask() {
        joinResponse = null;
        username = "";
        authToken = "";
        clientModel = ClientModel.getInstance();
    }


    protected Boolean doInBackground(GameSelectFragment... urls) {
        // Get info from the UI
        this.caller = urls[0];

        joinResponse =  ServerFacade.joinGame(game.getGameID().toString());
        return !joinResponse.isHasError();
    }

    protected void onProgressUpdate(Integer... progress) {
        // progressBar.setProgress(progress[0]);
    }

    protected void onPostExecute(Boolean success) {

        // Tell the view!

        if(success){
            caller.gameJoinUpdate("Game join successful.", success);
            clientModel.myJoinedGame = game.getGameID();

        } else {
            caller.gameJoinUpdate("Game join FAILED.", success);
        }

    }

    public void setJoinedGame(String gameID) {
        this.game = PreGameModelFacade.getGameByID(gameID);
    }
}