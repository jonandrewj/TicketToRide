package command;

import java.util.List;

import model.DestinationCard;

/**
 * Created by deriktj on 7/24/2017.
 */

public class DiscardDestinationCardCommand extends Command implements ILoggable
{

    private String username;
    private String gameID;
    private List<DestinationCard> cardsToDiscard;

    public DiscardDestinationCardCommand(String username, String gameID, List<DestinationCard> cardsToDiscard) {
        this.username = username;
        this.gameID = gameID;
        this.cardsToDiscard = cardsToDiscard;
    }

    @Override
    public Boolean execute() {
        return getContext().discardDestinationCards(gameID,username,cardsToDiscard);
    }

    @Override
    public String getGameId()
    {
        return gameID;
    }

    @Override
    public String getUsername()
    {
        return username;
    }

    @Override
    public String getLogMessage()
    {
        return this.getUsername() + " discarded " + cardsToDiscard.size() + " destination card" + (cardsToDiscard.size() == 1 ? "" : "s") + ".";
    }
}
