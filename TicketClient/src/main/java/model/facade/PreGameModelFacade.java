package model.facade;

import java.util.List;

import model.Color;
import model.Game;
import model.Player;
import model.data.ClientModel;

/**
 * Created by Andrew Jacobson on 7/17/2017.
 */

public class PreGameModelFacade {
    private static ClientModel model = ClientModel.getInstance();

    public static Game getGameByID(String gameID){
        Game game = null;
        for(Game g : model.getGames()){
            if(g.getGameID().equals(gameID)){
                game = g;
            }
        }
        return game;
    }

    public static Game getGameByIndex(int index){
        return model.getGames().get(index);
    }

    public static boolean isUserInGame(String username, String gameID){
        boolean found = false;
        for (Player p : getGameByID(gameID).getPlayers()){
            if (p.getUsername().equals(username)){
                found = true;
            }
        }
        return found;
    }

    public static void givePlayersOrderAndColor(Game game){
        List<Player> players = game.getPlayers();
        switch (players.size()){
            case 2:
                players.get(1).setOrder(0);
                players.get(0).setOrder(1);
                players.get(0).setColor(Color.GREEN);
                players.get(1).setColor(Color.BLACK);
                break;
            case 3:
                players.get(1).setOrder(0);
                players.get(0).setOrder(1);
                players.get(2).setOrder(2);
                players.get(0).setColor(Color.YELLOW);
                players.get(1).setColor(Color.WHITE);
                players.get(2).setColor(Color.RED);
                break;
            case 4:
                players.get(1).setOrder(0);
                players.get(0).setOrder(3);
                players.get(2).setOrder(2);
                players.get(3).setOrder(1);
                players.get(0).setColor(Color.GREEN);
                players.get(1).setColor(Color.BLACK);
                players.get(2).setColor(Color.RED);
                players.get(3).setColor(Color.BLUE);
                break;
            case 5:
                players.get(1).setOrder(4);
                players.get(0).setOrder(1);
                players.get(2).setOrder(2);
                players.get(3).setOrder(3);
                players.get(4).setOrder(0);
                players.get(0).setColor(Color.GREEN);
                players.get(1).setColor(Color.BLACK);
                players.get(2).setColor(Color.RED);
                players.get(3).setColor(Color.BLUE);
                players.get(4).setColor(Color.YELLOW);
                break;

        }
    }
}
