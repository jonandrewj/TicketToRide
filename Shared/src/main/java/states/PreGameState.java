package states;

import model.Color;
import model.MapRoute;
import model.Player;
import model.TrainCardHand;

/**
 * Created by Joseph on 7/29/2017.
 *
 * Represents a state before the game has actually begun.
 */

public class PreGameState extends TurnState {
    public PreGameState() {
        super("PreGameState");
    }

    @Override
    public boolean acceptDestinationCards(Player target) {
        target.setTurnState(new NotYourTurnState());
        return true;
    }

    @Override
    public boolean startTurn(Player target) {
        return false;
    }

    @Override
    public boolean drawCard(Player target, boolean isFaceUp, Color cardColor) {
        return false;
    }

    @Override
    public boolean drawDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean claimRoute(Player target, MapRoute route, TrainCardHand hand) {
        return false;
    }

    @Override
    public void endGame(Player target) { }
}
