package log;

import java.util.ArrayList;
import java.util.List;

import command.ICommand;

/**
 * Created by Joseph on 7/15/2017.
 *
 * The GameHistory is used to store various messages of multiple kinds.
 *
 * @invariant the gameHistory's internal log is never null.
 * @invariant any access to the game history through a get method will not change anything.
 */
public class GameHistory implements IGameHistory {

    private List<HistoryMessage> logItems;

    /**
     * Creates a new GameHistory object.
     *
     * @pre none.
     *
     * @post none.
     */
    public GameHistory() {
        logItems = new ArrayList<>();
    }

    @Override
    public List<HistoryMessage> getMessages() {
        return logItems;
    }

    @Override
    public List<HistoryMessage> getMessages(int offset, int limit) throws IllegalArgumentException {
        if (offset + limit > getCount()) {
            throw new IllegalArgumentException("Offset and limit are too high.");
        }
        else if (offset >= getCount()) {
            throw new IllegalArgumentException("Offset is too high.");
        }
        else if (offset <= 0) {
            throw new IllegalArgumentException("Offset must be positive.");
        }
        else if (limit < offset) {
            throw new IllegalArgumentException("Limit must be less than offset.");
        }
        limit = limit == ALL_MESSAGES ? logItems.size() : limit;
        return logItems.subList(offset, limit);
    }

    @Override
    public void addMessage(HistoryMessage item) {
        logItems.add(item);
    }

    @Override
    public int getCount() {
        return logItems.size();
    }
}
