package model.algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.SwingWorker;

import model.Game;
import model.Player;
import model.ServerModelFacade;
import model.algorithm.Graph.*;

import model.MapRoute;

/**
 * Created by deriktj on 7/30/2017.
 */

public class LongestPathAsyncTask extends SwingWorker<Set<String>, Integer>
{
    public static final int POINT_VALUE_LONGEST_ROUTE = 15;

    private Set<String> playersWithLongestRoute;
    private Map<String,Graph> graphs;
    private Game game;

    public LongestPathAsyncTask() {
        graphs = new HashMap<>();
        playersWithLongestRoute = new HashSet<>();
    }

    public LongestPathAsyncTask(Game game) {
        this();
        this.game = game;
        List<MapRoute> routes = game.getMapRoutes();
        for(Player player : game.getPlayers()) {
            graphs.put(player.getUsername(),new Graph());
        }

        for(MapRoute route : routes) {
            String username = route.getOwnerID();
            if(username == null) { continue; }
            graphs.get(username).addNode(route.getCity1().getName());
            graphs.get(username).addNode(route.getCity2().getName());
            graphs.get(username).addEdge(route.getCity1().getName(),route.getCity2().getName(),route.getRouteLength());
        }

    }

    @Override
    protected Set<String> doInBackground() throws Exception
    {
        int furthestLengthOfAllPlayers = 0;

        for (Map.Entry<String,Graph> pair : graphs.entrySet())
        {
            Graph bigGraph = pair.getValue();
            List<Graph> subGraphs = bigGraph.splitOnConnectivity();
            for(Graph graph : subGraphs) {
                /** Find furthest route in subgraph */
                FurthestRoute anEndPoint = new FurthestRoute();
                anEndPoint.furthestLength = 0;
                anEndPoint.furthestNode = null;
                graph.findFurthestNode(graph.getSomeNode(), 0, anEndPoint);
                FurthestRoute otherEndPoint = new FurthestRoute();
                otherEndPoint.furthestLength = 0;
                otherEndPoint.furthestNode = null;
                graph.findFurthestNode(anEndPoint.furthestNode, 0, otherEndPoint);
                if(otherEndPoint.furthestLength == furthestLengthOfAllPlayers) {
                    playersWithLongestRoute.add(pair.getKey());
                }
                else if(otherEndPoint.furthestLength > furthestLengthOfAllPlayers) {
                    playersWithLongestRoute.clear();
                    playersWithLongestRoute.add(pair.getKey());
                    furthestLengthOfAllPlayers = otherEndPoint.furthestLength;
                }
            }
        }
        return playersWithLongestRoute;
    }

    @Override
    protected void done() {
        for(String player : playersWithLongestRoute) {
            game.getPlayerByName(player).setScoreFromLongestTrain(POINT_VALUE_LONGEST_ROUTE);
        }
        ServerModelFacade.getInstance().sendLongestRouteScoreData(game.getGameID());
    }

}
