package model;

import java.security.SecureRandom;

/**
 * Created by deriktj on 7/11/2017.
 */

public class Account {
    private String username;
    private String password;
    private AuthToken curAuthToken;

    /**
     * Creates an account instance.
     * @param username the username associated with the account.
     * @param password the password used by this user.
     * @param auth the auth token to assign to the user.
     */
    public Account(String username, String password, AuthToken auth) {
        this.username = username;
        this.password = password;
        this.curAuthToken = auth;
    }

    /**
     * generates an authorization token for a user.
     * @return the newly generated authtoken.
     */
    public static AuthToken generateAuthToken() {
        SecureRandom random = new SecureRandom();
        byte[] bytes = new byte[20];
        random.nextBytes(bytes);
        String tokenString = bytes.toString();
        AuthToken token = new AuthToken(tokenString);
        token.setTimeOut(-1);
        return token;
    }

    /**
     * @return the username associated with this account.
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * @param username the username to assign to this account.
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * @return the password for this account.
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * @param password to assign to this password.
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * @return this accounts current auth token.
     */
    public AuthToken getCurAuthToken()
    {
        return curAuthToken;
    }

    /**
     * @param curAuthToken the new auth token for this account.
     */
    public void setCurAuthToken(AuthToken curAuthToken)
    {
        this.curAuthToken = curAuthToken;
    }
}
