package fragment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Map;

import cs340.byu.tickettoride.R;
import model.Color;
import model.TrainCardHand;

/**
 * Created by deriktj on 7/20/2017.
 */

/**
 * Adapter for cards in the player's hand
 */
public class TrainCardHandAdapter extends RecyclerView.Adapter<TrainCardHandAdapter.CustomViewHolder>
{
    private Color[] handColors;
    private int[] handCounts;
    private int handSize;

    private Context mContext;

    /**
     * Constructor
     * @param hand the TrainCardHand of the player to display
     * @param context context the adapter lives in
     */
    public TrainCardHandAdapter(TrainCardHand hand, Context context) {
        handSize = hand.getHandSize();
        handColors = new Color[handSize];
        handCounts = new int[handSize];

        int i = 0;
        for(Map.Entry<Color,Integer> entry : hand.getHandMap().entrySet()) {
            handColors[i] = entry.getKey();
            handCounts[i] = entry.getValue();
            i++;
        }
        this.mContext = context;
    }

    /**
     * Inflates the view with the train_card_display layout
     * @param parent the parent view
     * @param viewType
     * @return
     */
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.train_card_display, parent,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    /**
     * Binds each card to its onClickListener and sets it's image.
     * @param holder the recycler view list element
     * @param position the position of the holder in the list
     *
     * @post the list element will be setup to be rendered according to the card it corresponds to in the player's hand
     */
    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position)
    {
        holder.colorCount.setText(String.valueOf(handCounts[position]));
        Color color = handColors[position];
        switch(color)
        {
            case BLUE:
                holder.getLayout().setBackgroundResource(R.mipmap.blue_train);
                break;
            case GREEN:
                holder.getLayout().setBackgroundResource(R.mipmap.green_train);
                break;
            case PINK:
                holder.getLayout().setBackgroundResource(R.mipmap.pink_train);
                break;
            case WHITE:
                holder.getLayout().setBackgroundResource(R.mipmap.white_train);
                break;
            case YELLOW:
                holder.getLayout().setBackgroundResource(R.mipmap.yellow_train);
                break;
            case ORANGE:
                holder.getLayout().setBackgroundResource(R.mipmap.orange_train);
                break;
            case BLACK:
                holder.getLayout().setBackgroundResource(R.mipmap.black_train);
                break;
            case RED:
                holder.getLayout().setBackgroundResource(R.mipmap.red_train);
                break;
            case WILD:
                holder.getLayout().setBackgroundResource(R.mipmap.locomotive_train);
                break;
            default:
                holder.getLayout().setBackgroundResource(R.mipmap.back_train);
                break;
        }
    }


    /**
     * Gets the item id
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i)
    {
        return i;
    }

    /**
     * Gets the number of items in the list (always 6)
     * @return the number of items in the list (6)
     */
    @Override
    public int getItemCount()
    {
        return handSize;
    }

    /**
     * The view holder for each card in the list. Used internally.
     */
    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        protected TextView colorCount;
        protected View layout;

        public CustomViewHolder(View view) {
            super(view);
            layout = view;
            this.colorCount = (TextView) view.findViewById(R.id.colorCount);
        }

        public View getLayout() {
            return layout;
        }
    }
}
