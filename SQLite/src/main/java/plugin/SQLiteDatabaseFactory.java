package plugin;

import org.tmatesoft.sqljet.core.SqlJetException;

/**
 * Created by parad on 8/9/2017.
 */

public class SQLiteDatabaseFactory implements iDatabaseFactory {
    /**
     * Attempts to create a connection to the database.
     * @return a new SQLiteDB if no errors occur; otherwise, null.
     */
    @Override
    public iDaoAPI getDatabase() {
        try {
            return new SQLiteDB();
        } catch (SqlJetException e) {
            e.printStackTrace();
            return null;
        }
    }
}
