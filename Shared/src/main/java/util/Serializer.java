package util;

import com.google.gson.Gson;

/**
 * Created by deriktj on 7/9/2017.
 */

public class Serializer {

    private static Gson gson = new Gson();

    /**
     * Instantiates a new serializer.
     */
    private Serializer() {

    }

    /**
     * Converts the given object to a json string.
     * @param o the object to convert.
     * @return a serialized representation of the object.
     */
    public static String toJson(Object o) {
        return gson.toJson(o);
    }

    /**
     * Turns a json string into an object of the specified class.
     * @param jsonObj the string containing the object data.
     * @param c the class of the object to retrieve from the string.
     * @return an object, with the data from the json string.
     */
    public static Object fromJson(String jsonObj, Class c) {
        return gson.fromJson(jsonObj, c);
    }
}
