package command;

import model.Color;

/**
 * Created by deriktj on 7/17/2017.
 */

public class DrawTrainCardCommand extends Command implements ILoggable
{
    private String player;
    private String gameID;
    private int index;
    private static final int DECK_INDEX = 5;
    private Color toReplace;
    private Color color;

    public DrawTrainCardCommand(String player, String gameID, int index) {
        super();
        this.player = player;
        this.gameID = gameID;
        this.index = index;
        toReplace = null;
        color = null;
    }

    @Override
    public Boolean execute()
    {
        return getContext().drawTrainCard(gameID,player,index,color,toReplace);
    }

    public Color getToReplace()
    {
        return toReplace;
    }

    public void setToReplace(Color toReplace)
    {
        this.toReplace = toReplace;
    }

    public Color getColor()
    {
        return color;
    }

    public void setColor(Color color)
    {
        this.color = color;
    }

    public String getPlayer()
    {
        return player;
    }

    public String getGameID()
    {
        return gameID;
    }

    @Override
    public String getGameId() {
        return this.gameID;
    }

    @Override
    public String getUsername() {
        return this.player;
    }

    @Override
    public String getLogMessage() {
        String message = "drew a ";
        message += index == DECK_INDEX ? "card from the deck." : color.toString() + " card.";
        return message;
    }
}
