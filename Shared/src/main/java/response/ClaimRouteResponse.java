package response;

import model.MapRoute;

/**
 * Created by kendall on 7/20/2017.
 */

public class ClaimRouteResponse extends ResponseBase {
    /**
     * Creates a basic Response.
     *
     * @param hasError     if this response is sent as a result of an error.
     * @param errorMessage the response's error message.
     */

    private MapRoute route;
    public ClaimRouteResponse(boolean hasError, String errorMessage) {

        super(hasError, errorMessage);
    }

    public void setRoute(MapRoute route) {
        this.route = route;
    }

    public MapRoute getRoute() {
        return route;
    }
}
