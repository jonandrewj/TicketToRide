package states;

import model.Color;
import model.MapRoute;
import model.Player;
import model.TrainCardHand;

/**
 * Created by Alyx on 8/9/17.
 */

public class TurnState implements ITurnState {
    private String stateIdentifier;

    public TurnState(String id){
        this.stateIdentifier = id;
    }

    @Override
    public boolean acceptDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean startTurn(Player target) {
        return false;
    }

    @Override
    public boolean drawCard(Player target, boolean isFaceUp, Color cardColor) {
        return false;
    }

    @Override
    public boolean drawDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean claimRoute(Player target, MapRoute route, TrainCardHand hand) {
        return false;
    }

    @Override
    public void endGame(Player target) {

    }

    public String getStateIdentifier() {
        return stateIdentifier;
    }

}
