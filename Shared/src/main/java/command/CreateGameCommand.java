package command;

import model.Game;

/**
 * Created by deriktj on 7/9/2017.
 */

public class CreateGameCommand extends Command
{

    private int playerMax;
    private String gameID;

    /**
     * Creates a create game command.
     * @param playerMax the maximum amount of players to allow in the new game.
     */
    public CreateGameCommand(int playerMax, String gameID) {
        super();

        this.playerMax = playerMax;
        this.gameID = gameID;
    }

    /**
     * Executes this command, creating a game in the current context.
     */
    @Override
    public Boolean execute()
    {
        Game newGame = new Game(playerMax, gameID);

        return getContext().addGame(newGame);
    }
}
