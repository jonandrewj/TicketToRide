package response;

import model.AuthToken;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class RegisterResponse extends ResponseBase {
    private AuthToken authToken;

    /**
     * Creates a Register Response.
     * @param hasError if the response contains an error.
     * @param errorMessage the message of the error.
     * @param authToken an auth token for the user.
     */
    public RegisterResponse(boolean hasError, String errorMessage, AuthToken authToken) {
        super(hasError, errorMessage);
        this.authToken = authToken;
    }

    /**
     * @return the auth token given by this response.
     */
    public AuthToken getAuthToken() {
        return authToken;
    }

    /**
     * @param authToken the auth token to be sent.
     */
    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }
}
