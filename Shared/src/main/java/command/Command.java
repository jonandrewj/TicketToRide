package command;

import java.util.Calendar;
import java.util.Date;

import model.IModelFacade;
import util.ITypeRetriever;

/**
 * Created by Andrew Jacobson on 7/9/2017.
 */

public class Command implements ICommand, ITypeRetriever{

    private String commandIdentifier;
    private IModelFacade context;
    private final Date timestamp;

    /**
     * creates a basic command object.
     */
    public Command() {
        this.commandIdentifier = this.getClass().getName();
        this.timestamp = Calendar.getInstance().getTime();
    }

    public Boolean execute(){return false;};

    /**
     * @return the context of the command.
     */
    protected IModelFacade getContext()  {
        if(context == null) {
            //error
        }
        return context;
    }

    /**
     * @param context the context of the command.
     */
    public void setContext(IModelFacade context) {
        this.context = context;
    }

    /**
     * @return the time at which the command was created.
     */
    @Override
    public final Date getTimestamp() {
        return timestamp;
    }

    /**
     * @return the full classname of the command object.
     */
    public String getCommandClassName() {
        return commandIdentifier;
    }

    @Override
    public String getType() {
        return getCommandClassName();
    }
}
