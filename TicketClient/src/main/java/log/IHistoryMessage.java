package log;

import java.util.Date;

/**
 * Created by Joseph on 7/17/2017.
 *
 * A simple message to be used in the GameHistory.
 * @invariant the data in the HistoryMessage is never changed.
 */

interface IHistoryMessage extends Comparable {

    /**
     * @return the message held within this instance
     */
    String getMessage();

    /**
     * @return the timestamp that this history item was created at.
     */
    Date getTimestamp();

    /**
     * Creates a string that is a formatted version of this history message.
     *
     * @pre none.
     *
     * @post nothing in the history message is altered.
     * @post the chat message is formatted with the username, the message, and the timestamp.
     *
     * @return a formatted string based on this history message.
     */
    String format();

    /**
     * @return the source of this message.
     */
    String getOwner();

    /**
     * @return the color this log message should create.
     */
    EventType getEventType();
}
