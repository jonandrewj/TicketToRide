package response;

import model.AuthToken;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class LoginResponse extends ResponseBase {
    private AuthToken authToken;

    /**
     * Creates a login response.
     * @param hasError if this response contains an error.
     * @param errorMessage the error message.
     * @param authToken the authorization token created by the response.
     */
    public LoginResponse(boolean hasError, String errorMessage, AuthToken authToken) {
        super(hasError, errorMessage);
        this.authToken = authToken;
    }

    /**
     * @return the token given by the response.
     */
    public AuthToken getAuthToken() {
        return authToken;
    }

    /**
     * @param authToken a new token for the response.
     */
    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }
}
