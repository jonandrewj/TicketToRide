package service;

import model.Account;
import model.AuthToken;
import model.ServerModel;
import model.ServerModelFacade;
import request.RegisterRequest;
import response.RegisterResponse;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class RegisterService {

    /**
     * Handles the registration request.
     * @param request the request to be handled.
     * @return the response to the request.
     */
    public static RegisterResponse register(RegisterRequest request) {
        boolean noError = false;
        String username = request.getUsername();
        String password = request.getPassword();
        if(password != null && !password.isEmpty()) {
            // Received password!
            if (username != null && !username.isEmpty()) {
                if(ServerModelFacade.getInstance().getAccount(username) == null) {
                    AuthToken myToken = Account.generateAuthToken();
                    ServerModelFacade.getInstance().newAccount(username,password, myToken);
                    return new RegisterResponse(noError,"Registration successful", myToken);
                }
                else {
                    return new RegisterResponse(!noError,"This user is already registered",null);
                }

            }
        }
        return new RegisterResponse(!noError, "Username or password is empty", null);

    }
}
