package model;

/**
 * Created by Andrew Jacobson on 7/17/2017.
 */

public class City {

    private String cityName;

    public City(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
