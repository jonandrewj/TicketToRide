package service;

import model.Account;
import model.AuthToken;
import model.ServerModel;
import model.ServerModelFacade;
import request.LoginRequest;
import response.LoginResponse;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class LoginService {

    /**
     * Handles a login request.
     * @param request the request to be handled.
     * @return a response to the request.
     */
    public static LoginResponse login(LoginRequest request) {
        boolean noError = false;

        String username = request.getUsername();
        String password = request.getPassword();
        if(password != null && !password.isEmpty()) {
            // Received password!
            if (username != null && !username.isEmpty()) {
                Account accountInQuestion = ServerModelFacade.getInstance().getAccount(username);
                if(accountInQuestion != null && accountInQuestion.getPassword().equals(password)) {
                    AuthToken myToken = Account.generateAuthToken();
                    accountInQuestion.setCurAuthToken(myToken);
                    return new LoginResponse(noError, "Login successful", myToken);
                }
            }
        }
        return new LoginResponse(!noError, "Username or password is incorrect", null);


        // USE THE USERNAME AND PASSWORD TO GET A NEW AUTH TOKEN FOR THE USER
        // RETURN ERROR IF PASSWORD IS INCORRECT
        // NO CHANGES NEED TO BE MADE TO THE COMMAND QUEUE
    }
}
