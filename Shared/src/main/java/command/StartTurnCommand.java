package command;

/**
 * Created by parad on 8/1/2017.
 */

public class StartTurnCommand extends Command implements ILoggable {

    private String username;
    private String gameId;

    public StartTurnCommand() {
        super();
    }

    public StartTurnCommand(String gameId, String username) {
        super();
        this.username = username;
        this.gameId = gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    @Override
    public String getGameId() {
        return gameId;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getLogMessage() {
        return "It's " + username + "'s turn!";
    }

    @Override
    public Boolean execute() {
        return this.getContext().startTurn(gameId, username);
    }
}
