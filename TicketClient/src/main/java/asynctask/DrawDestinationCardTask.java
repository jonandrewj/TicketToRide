package asynctask;

import android.os.AsyncTask;

import fragment.CardSelectionFragment;
import model.Game;
import model.Player;
import response.CommandResponse;
import serverproxy.ServerFacade;
import model.data.ClientModel;
import response.ResponseBase;
import fragment.GameSelectFragment;

/**
 * Created by Alyx on 7/9/17.
 */

public class DrawDestinationCardTask extends AsyncTask<CardSelectionFragment, Integer, Boolean> {
    /** Register result, contains the result of the register attempt */
    private CommandResponse drawResponse;

    /** Access the model */
    private ClientModel model;

    /** Access the database */
    private ServerFacade facade;

    /** The fragment that called this task */
    private CardSelectionFragment caller;

    private String username;
    private String gameID;

    public DrawDestinationCardTask(String username, String gameID) {
        facade = new ServerFacade();
        drawResponse = null;
        model = ClientModel.getInstance();

        this.username = username;
        this.gameID = gameID;
    }


    protected Boolean doInBackground(CardSelectionFragment... urls) {

        Game game = ClientModel.getInstance().getGameByID(gameID);
        Player currentPlayer = game.getPlayerByName(username);
        if (!currentPlayer.canDrawDestinationCards()) return false;

        // Get info from the UI
        this.caller = urls[0];

        drawResponse =  facade.drawDestinationCard(username,gameID);

        if(drawResponse != null && !drawResponse.isHasError()){
            // Return success
            return true;

        } else {
            return false;
        }

    }

    protected void onProgressUpdate(Integer... progress) {
        // progressBar.setProgress(progress[0]);
    }

    protected void onPostExecute(Boolean success) {

    }

}
