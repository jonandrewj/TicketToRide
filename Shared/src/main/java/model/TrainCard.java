package model;


/**
 * Created by deriktj on 7/16/2017.
 */

public class TrainCard
{
    private Color color;

    public TrainCard(Color color) {
        this.color = color;
    }

    public Color getColor()
    {
        return color;
    }

}
