package server;

import java.net.URLClassLoader;

import plugin.iDatabaseFactory;

/**
 * Created by Andrew Jacobson on 8/6/2017.
 */

public class AbstractDatabaseFactory {

    public static iDatabaseFactory getDatabaseFactory(String dbType, URLClassLoader cl) {

        try {
            Class database = Class.forName(dbType, true, cl);
            return (iDatabaseFactory) database.newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
        // NULL SHOULD TELL THE SERVER THAT THE COMMAND WAS INVALID
        return null;
    }
}
