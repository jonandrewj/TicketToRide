package command;

import model.Game;

/**
 * Created by parad on 8/1/2017.
 */

public class ChangeGameStateCommand extends Command implements ILoggable {

    private Game.Gamestate newState;
    private String gameID;

    public ChangeGameStateCommand(String gameId, Game.Gamestate newState) {
        this.gameID = gameId;
        this.newState = newState;
    }

    @Override
    public Boolean execute() {
        return getContext().changeGameState(gameID,newState);
    }

    @Override
    public String getGameId() {
        return gameID;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public String getLogMessage() {
        switch (newState) {
            case SETUP:
                return "The game is being set up.";
            case PLAY:
                return "Let the games begin!";
            case LAST_TURN:
                return "It's the last turn! Make it count!";
            case END:
                return "The game is over!";
            default:
                // If we ever reach this point, we must have added a new state.
                return "invalid game state operation in";
        }
    }
}
