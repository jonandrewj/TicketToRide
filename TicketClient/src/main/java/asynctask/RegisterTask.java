package asynctask;

import android.os.AsyncTask;

import serverproxy.ServerFacade;
import model.AuthToken;
import model.User;
import response.RegisterResponse;
import fragment.LoginFragment;

/**
 * Created by Alyx on 7/9/17.
 */

public class RegisterTask extends AsyncTask<LoginFragment, Integer, RegisterResponse> {
    
    /** Register result, contains the result of the register attempt */
    private RegisterResponse registerResponse;

    /** The authorization token returned from the server upon successful authentication */
    private String authToken;

    /** Access the model */
    private User user;

    /** Access the database */
    private ServerFacade facade;

    /** Holds the user's username & password as raw text */
    private String username;
    private String password;

    /** The fragment that called this task */
    private LoginFragment caller;

    public void setRegisterFields(String username, String password){
        this.username = username;
        this.password = password;
    }


//    private boolean loginSuccessful;

    public RegisterTask() {
        facade = new ServerFacade();
        registerResponse = null;
        username = "";
        password = "";

        user = User.getInstance();
    }


    protected RegisterResponse doInBackground(LoginFragment... urls) {
        // Get info from the UI
        this.caller = urls[0];

        // Login thru database
        registerResponse = facade.register(username,password);


        // If no error, set authToken in the model.
        if(registerResponse != null && !registerResponse.isHasError()){

            // Get token
            authToken = registerResponse.getAuthToken().getToken();

            // Fill up the model
            user.setUsername(username);
            user.setAuthToken(new AuthToken(authToken));

            // Return success
        } else {
            // If error, return false;
            System.out.println("Failure: ");
            if(registerResponse != null) {
                System.out.println(registerResponse.getErrorMessage());
            }
        }
        return registerResponse;
    }

    protected void onProgressUpdate(Integer... progress) {
        // progressBar.setProgress(progress[0]);
    }

    protected void onPostExecute(RegisterResponse response) {
        caller.authenticationUpdate(response.getErrorMessage(), !response.isHasError());
    }


}