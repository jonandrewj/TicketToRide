package serverproxy;

import org.junit.Test;

import request.LoginRequest;
import request.RegisterRequest;
import response.LoginResponse;
import response.PollerResponse;
import response.RegisterResponse;
import response.ResponseBase;

import static org.junit.Assert.*;

/**
 * Created by Andrew Jacobson on 7/13/2017.
 */
public class InitGameProxyTest {
    private static String ip = "192.168.1.119";
    private static String auth;

    @Test
    public void register() throws Exception {
        InitGameProxy.setServerIP(ip);
        RegisterResponse response = InitGameProxy.register(new RegisterRequest("andrewj7", "password"));
        assertTrue(!response.isHasError());
        response = InitGameProxy.register(new RegisterRequest("andrewj7", "password"));
        assertTrue(response.isHasError());
        InitGameProxy.setServerIP("192.168.1.992");
        response = InitGameProxy.register(new RegisterRequest("asd6", "password"));
        assertTrue(response.isHasError());

    }

    @Test
    public void login() throws Exception {
        InitGameProxy.setServerIP(ip);
        LoginResponse response = InitGameProxy.login(new LoginRequest("jonandrewj2", "masterpass"));
        assertTrue(response.isHasError());
        InitGameProxy.register(new RegisterRequest("jonandrewj2", "masterpass"));

        response = InitGameProxy.login(new LoginRequest("jonandrewj2", "masterpass"));
        assertTrue(!response.isHasError());
        auth = response.getAuthToken().getToken();
    }

    @Test
    public void createGame() throws Exception {
        InitGameProxy.setServerIP(ip);
        ResponseBase response = InitGameProxy.createGame(3);
        assertTrue(!response.isHasError());
        response = InitGameProxy.createGame(4);
        assertTrue(!response.isHasError());
        response = InitGameProxy.createGame(2);
        assertTrue(!response.isHasError());
        response = InitGameProxy.createGame(5);
        assertTrue(!response.isHasError());
    }

    @Test
    public void poll() throws Exception {
        InitGameProxy.setServerIP(ip);
        InitGameProxy.createGame(3);
        InitGameProxy.createGame(3);
        InitGameProxy.createGame(3);
        PollerResponse response = InitGameProxy.poll(-1);
        //System.out.println(response.getCommandCount());
        for (int i = 0; i < response.getCommandCount(); i++){
            System.out.println(response.getCommands().get(i).toString());
        }
    }

    @Test
    public void joinGame() throws Exception {
        InitGameProxy.setServerIP(ip);
        ResponseBase response = InitGameProxy.joinGame("nothing");
        assertTrue(response.isHasError());

    }





}