package command;

/**
 * Created by Andrew Jacobson on 7/9/2017.
 */

public class StartGameCommand extends Command implements ILoggable
{

    String gameIdToStart;

    /**
     * When executed, starts the specified game.
     * @param gameIdToStart the id of the game that is to be started.
     */
    public StartGameCommand(String gameIdToStart) {
        super();
        this.gameIdToStart = gameIdToStart;
    }

    /**
     * Starts the specified game.
     */
    @Override
    public Boolean execute()
    {
        return getContext().startGame(gameIdToStart);
    }

    @Override
    public String getGameId() {
        return gameIdToStart;
    }

    @Override
    public String getUsername() {
        return null;
    }

    @Override
    public String getLogMessage() {
        return "the game has started!";
    }
}
