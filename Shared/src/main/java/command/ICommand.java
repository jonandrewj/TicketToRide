package command;

import java.util.Date;

import model.IModelFacade;
import util.ITypeRetriever;

/**
 * Created by deriktj on 7/9/2017.
 */

public interface ICommand extends ITypeRetriever
{
    /**
     * Execute the command.
     */
    Boolean execute();

    /**
     * @param context the context of the command.
     */
    void setContext(IModelFacade context);

    /**
     * @return the time at which the command was created.
     */
    Date getTimestamp();
}
