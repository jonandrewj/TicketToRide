package states;

import model.Color;
import model.MapRoute;
import model.Player;
import model.TrainCardHand;

/**
 * Created by Joseph on 7/29/2017.
 *
 * Represents a state where it is not the player's turn.
 */

public class NotYourTurnState extends TurnState {
    public NotYourTurnState() {
        super("NotYourTurnState");
    }

    @Override
    public boolean acceptDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean startTurn(Player target) {
        target.setTurnState(new TurnStartedState());
        return true;
    }

    @Override
    public boolean drawCard(Player target, boolean isFaceUp, Color cardColor) {
        return false;
    }

    @Override
    public boolean drawDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean claimRoute(Player target, MapRoute route, TrainCardHand hand) {
        return false;
    }

    @Override
    public void endGame(Player target) {
        target.setState(new PostGameState());
    }
}
