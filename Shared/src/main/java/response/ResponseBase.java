package response;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class ResponseBase {
    private boolean hasError;
    private String errorMessage;

    /**
     * Creates a basic Response.
     * @param hasError if this response is sent as a result of an error.
     * @param errorMessage the response's error message.
     */
    public ResponseBase(boolean hasError, String errorMessage) {
        this.hasError = hasError;
        this.errorMessage = errorMessage;
    }

    /**
     * @return true, if the response has an error. Otherwise, false.
     */
    public boolean isHasError() {
        return hasError;
    }

    /**
     * @param hasError true if this reponse should carry an error.
     */
    public void setHasError(boolean hasError) {
        this.hasError = hasError;
    }

    /**
     * @return the error message for this response.
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * @param errorMessage the response's error message.
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
