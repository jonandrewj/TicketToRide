package command;

/**
 * Created by deriktj on 7/23/2017.
 */

public class DrawDestinationCardCommand extends Command implements ILoggable
{
    private String username;
    private String gameID;
    private String city1;
    private String city2;
    private Integer points;

    public DrawDestinationCardCommand(String username, String gameID) {
        this.username = username;
        this.gameID = gameID;
        this.city1 = null;
        this.city2 = null;
        this.points = null;
    }

    @Override
    public Boolean execute() {
        return getContext().drawDestinationCard(gameID, username, city1, city2, points);
    }

    @Override
    public String getGameId()
    {
        return gameID;
    }

    @Override
    public String getUsername()
    {
        return username;
    }

    @Override
    public String getLogMessage()
    {
        return this.getUsername() + " drew a destination card.";
    }

    public void setCity1(String city1)
    {
        this.city1 = city1;
    }

    public void setCity2(String city2)
    {
        this.city2 = city2;
    }

    public void setPoints(Integer points)
    {
        this.points = points;
    }
}
