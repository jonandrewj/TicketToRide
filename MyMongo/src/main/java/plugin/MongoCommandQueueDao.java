package plugin;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

import command.Command;
import command.ICommand;
import util.Serializer;

/**
 * Created by Andrew Jacobson on 8/5/2017.
 */

public class MongoCommandQueueDao {

    // COLLECTIONS OF COMMANDS FROM THE TICKET TO RIDE DATABASE
    MongoCollection<Document> commandQueueCollection;

    public MongoCommandQueueDao(MongoDatabase database){
        commandQueueCollection = database.getCollection("commandQueue");
    }

    public void clear(){
        commandQueueCollection.drop();
    }

    public List<ICommand> getCommandQueue(){
        List<ICommand> commandQueue = new ArrayList<>();

        MongoCursor<Document> cursor = commandQueueCollection.find().iterator();
        try {
            while (cursor.hasNext()) {
                String json = cursor.next().toJson();
                Command command = (Command) Serializer.fromJson(json,Command.class);
                ICommand castCommand = (ICommand) Serializer.fromJson(json ,Class.forName(command.getCommandClassName()));

                commandQueue.add(castCommand);
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        finally {
            cursor.close();
        }

        return commandQueue;
    }

    public void addCommand(ICommand command){
        String json = Serializer.toJson(command);
        commandQueueCollection.insertOne(Document.parse(json));
    }
}
