package handler;

import java.io.IOException;
import command.Command;
import command.ICommand;
import command.CreateGameCommand;
import model.ServerModel;
import model.ServerModelFacade;
import response.CommandResponse;
import util.Serializer;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class CommandHandler extends HandlerBase {

    /**
     * Creates a command handler instance.
     */
    public CommandHandler()
    {

    }

    /**
     * Creates a handler for this class.
     * @param body the request body.
     * @return a response to the request.
     * @throws IOException if something goes wrong while processing the request.
     */
    @Override
    public String customHandle(String body) throws IOException
    {
        try {
            Command command = (Command) Serializer.fromJson(body,Command.class);
            System.out.println("Received: " + command.getCommandClassName());
            ICommand castCommand = (ICommand) Serializer.fromJson(body,Class.forName(command.getCommandClassName()));
            Boolean success = ServerModelFacade.getInstance().applyCommand(castCommand);
            CommandResponse response = new CommandResponse(!success,"");
            String responseJson = Serializer.toJson(response);
            return responseJson;
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new IOException("Command not found!");
        }
    }
}
