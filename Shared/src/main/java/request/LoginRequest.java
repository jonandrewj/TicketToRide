package request;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class LoginRequest {
    private String username;
    private String password;

    /**
     * Creates a login request for the user.
     * @param username the username to login.
     * @param password the password for the user.
     */
    public LoginRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * @return the username associated with the request.
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to assign to the request.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return this request's password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password associated with the request.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
