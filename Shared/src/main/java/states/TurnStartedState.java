package states;

import model.Color;
import model.MapRoute;
import model.Player;
import model.TrainCardHand;

/**
 * Created by Joseph on 7/29/2017.
 *
 * Represents a state where a player's turn has started and they have not made any decisions.
 */

public class TurnStartedState extends TurnState {

    private int cardCount = 0;

    public TurnStartedState() {
        super("TurnStartedState");
    }

    @Override
    public boolean acceptDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean startTurn(Player target) {
        return false;
    }

    @Override
    public boolean drawCard(Player target, boolean isFaceUp, Color cardColor) {
        if (isFaceUp && cardColor == Color.WILD) {
            target.setTurnState(new NotYourTurnState());
        }
        else {
            target.setTurnState(new TrainCardDrawnState());
        }
        return true;
    }

    @Override
    public boolean drawDestinationCards(Player target) {
        cardCount++;
        if (cardCount == 3) {
            target.setTurnState(new DestinationCardDrawnState());
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public boolean claimRoute(Player target, MapRoute route, TrainCardHand hand) {
        target.setTurnState(new NotYourTurnState());
        return true;

    }

    @Override
    public void endGame(Player target) { }
}
