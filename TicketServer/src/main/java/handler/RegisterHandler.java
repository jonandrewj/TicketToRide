package handler;

import java.io.IOException;
import request.RegisterRequest;
import response.RegisterResponse;
import service.RegisterService;
import util.Serializer;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class RegisterHandler extends HandlerBase {

    /**
     * creates a new instance.
     */
    public RegisterHandler() {
    }

    /**
     * Handles the request sent to the server.
     * @param body the request body.
     * @return the response body.
     * @throws IOException if the server runs into an issue while processing the request.
     */
    @Override
    public String customHandle(String body) throws IOException
    {
        RegisterRequest request = (RegisterRequest) Serializer.fromJson(body,RegisterRequest.class);
        RegisterResponse response = RegisterService.register(request);
        return Serializer.toJson(response);
    }
}
