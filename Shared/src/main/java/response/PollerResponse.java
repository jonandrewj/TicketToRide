package response;

import java.util.ArrayList;
import java.util.List;

import command.*;
import request.PollerRequest;
import util.Serializer;

/**
 * Created by deriktj on 7/11/2017.
 */

public class PollerResponse extends ResponseBase
{
    private int commandCount;
    private List<String> commands;
    private int lastIndex;

    public PollerResponse(){
        // If no response, assume no error.
        super(false, "");

        commandCount = 0;
        commands = new ArrayList<>();
    }

    /**
     * Creates a new response for the poller request.
     * @param hasError if the response contains an error.
     * @param errorMessage the error message.
     */
    public PollerResponse(boolean hasError, String errorMessage)
    {
        super(hasError, errorMessage);
        commandCount = 0;
        commands = new ArrayList<>();
    }

    /**
     * Inserts a command to be sent to the client.
     * @param command the command to be added.
     */
    public void addCommand(Command command) {
        commandCount++;
        commands.add(Serializer.toJson(command));
    }

    /**
     * @return the amount of commands in the response.
     */
    public int getCommandCount() {
        return commandCount;
    }

    /**
     * @param commandCount the amount of commands to carry.
     */
    public void setCommandCount(int commandCount) {
        this.commandCount = commandCount;
    }

    /**
     * @param commands a list of commands to send in the poller response.
     */
    public void setCommands(List<String> commands){
        this.commands = commands;
    }

    /**
     * @return this poller response's commands.
     */
    public List<String>getCommands(){
        return this.commands;
    }

    public int getLastIndex()
    {
        return lastIndex;
    }

    public void setLastIndex(int lastIndex)
    {
        this.lastIndex = lastIndex;
    }
}
