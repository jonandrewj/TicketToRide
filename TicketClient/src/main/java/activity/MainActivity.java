package activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;

import fragment.LoginFragment;
import cs340.byu.tickettoride.R;

import android.support.v4.app.FragmentTransaction;

/**
 * The main activity for the app.
 */
public class MainActivity extends AppCompatActivity {

    private EditText username;
    private EditText password;
    private EditText port;
    private EditText host;
    private Button loginButton;
    private Boolean loginSuccess = false;
    private FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);
        Fragment fragment = new LoginFragment();
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_login, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

    }

    /**
     * Moves the app to the Game View.
     */
    public void toGameView(){
        Intent gameIntent = new Intent(null, GamePlayActivity.class);
        startActivity(gameIntent);
    }



    @Override
    public void onBackPressed() {
    }

    @Override
    public void onStop() {
        // Just for future reference, I guess?
        super.onStop();
    }

}


