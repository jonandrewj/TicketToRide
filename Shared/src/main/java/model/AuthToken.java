package model;

/**
 * Created by Alyx on 7/10/17.
 */

public class AuthToken {
    private int timeOut;
    private String token;

    /**
     * Builds a basic authtoken with a default value and timeout.
     */
    public AuthToken(){
        timeOut = 0;
        token = "";
    }

    /**
     * Creates an authtoken with the given value.
     * @param token the value of the authorization token.
     */
    public AuthToken(String token){
        this.token = token;
        timeOut = 0;
    }

    /**
     * @return the timeout assigned to this auth token.
     */
    public int getTimeOut() {
        return timeOut;
    }

    /**
     * @param timeOut the time until timeout for this token.
     */
    public void setTimeOut(int timeOut) {
        this.timeOut = timeOut;
    }

    /**
     * @return the value of this authorization token
     */
    public String getToken() {
        return token;
    }

    /**
     * @param token the value to give this auth token.
     */
    public void setToken(String token) {
        this.token = token;
    }
}
