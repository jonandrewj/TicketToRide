package asynctask;

import android.os.AsyncTask;

import fragment.HistoryAndChatFragment;
import response.CommandResponse;
import serverproxy.ServerFacade;

/**
 * Created by Joseph on 7/22/2017.
 */

public class ChatTask extends AsyncTask<HistoryAndChatFragment, Integer, Boolean> {

    private HistoryAndChatFragment caller;
    private String message;
    private String gameId;


    public ChatTask(String message, String gameId) {
        this.message = message;
        this.gameId = gameId;
    }

    @Override
    protected Boolean doInBackground(HistoryAndChatFragment... params) {
        this.caller = params[0];

        CommandResponse response = ServerFacade.sendChatMessage(gameId, message);

        return !response.isHasError();
    }
}
