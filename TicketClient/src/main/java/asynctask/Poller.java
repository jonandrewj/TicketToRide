package asynctask;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Alyx on 7/10/17.
 */

public class Poller {

    private final long TIMER_INITIAL_DELAY = 0; // Wait 0 milliseconds until timer starts.
    private final long TIMER_PERIOD = 1000; // Repeat every 1000 milliseconds (1 second)

    private Timer timer = new Timer();
    private TimerTask task = new PollTimer();



    /**
     * This polls the server to see if there are any commands.
     */
    public void startPoller(){

        timer.scheduleAtFixedRate(task,TIMER_INITIAL_DELAY,TIMER_PERIOD);

    }

    /**
     * Stops the poller.
     */
    public void stopPoller(){
        timer.cancel();
    }
}
