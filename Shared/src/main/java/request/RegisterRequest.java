package request;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class RegisterRequest {
    private String username;
    private String password;

    /**
     * Creates a request to register a new user.
     * @param username the username of the new user.
     * @param password the password to assign to the user.
     */
    public RegisterRequest(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * @return the username associated with the request.
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to send with the request.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password of the user.
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to assign to this request.
     */
    public void setPassword(String password) {
        this.password = password;
    }
}
