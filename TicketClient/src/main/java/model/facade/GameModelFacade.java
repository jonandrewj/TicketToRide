package model.facade;

import java.util.ArrayList;
import model.Game;
import model.MapRoute;
import model.Player;
import model.User;
import model.data.ClientModel;

/**
 * Created by Andrew Jacobson on 7/17/2017.
 */

/**
 * @Invariant the reference to the ClientModel is not null
 */
public class GameModelFacade {
    private static ClientModel model = ClientModel.getInstance();

    /**
     * @param gameID
     * @pre a non-null string is passed in
     *
     * @post a list of names of the game's players, the list is not in any specific order
     * @post an empty list if the game is not found
     *
     * @return a list of strings with Player names on them
     */
    public static ArrayList<String> getPlayerNamesForGame(String gameID){
        Game game = PreGameModelFacade.getGameByID(gameID);
        ArrayList<String> playerNames = new ArrayList<>();
        if (game != null) {
            for (Player p : game.getPlayers()) {
                playerNames.add(p.getUsername());
            }
        }
        return playerNames;
    }

    /**
     * @pre a non-null string is passed in
     *
     * @post the game's routes list is an initialized, empty list
     * @post no changes if the game is not found
     *
     * @param gameID
     */
    public static void initializeGame(String gameID){
        Game game = PreGameModelFacade.getGameByID(gameID);
        if (game != null) {
            //game.routes = new ArrayList<>();
        }
    }

    /**
     * @pre none
     *
     * @post the logged in user is returned as a User object
     * @return a User object that represents the user that is logged in to this client
     */
    public static User getUser() {
        return model.getUser();
    }

    /**
     * @param username
     * @param gameID
     * @pre both parameters are non-null
     * @pre the username is actually associated with the gameID
     *
     * @post if found, the player object for the given username is returned
     *
     * @return a Player Object or null
     */
    public static Player getPlayerInGame(String username,String gameID) {
        return PreGameModelFacade.getGameByID(gameID).getPlayerByName(username);
    }

    public static ArrayList<MapRoute> getRoutesForPlayerInGame(String gameID, String player){
        Game game = PreGameModelFacade.getGameByID(gameID);
        ArrayList<MapRoute> playerRoutes = new ArrayList<>();
        for (MapRoute r : game.getMapRoutes()){
            if (r.getOwnerID().equals(player)){
                playerRoutes.add(r);
            }
        }
        return playerRoutes;
    }
}
