package plugin;

import java.io.IOException;

/**
 * Created by Andrew Jacobson on 8/5/2017.
 */

// I ATEMPTED TO START THE SERVER FROM A THREAD BUT IT IS CAUSING PROBLEMS FOR THE SERVER
public class MongoServer implements Runnable {

    public MongoServer(){}

    @Override
    public void run() {
        Runtime rt = Runtime.getRuntime();
        try {
            rt.exec("mongod");
        } catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
