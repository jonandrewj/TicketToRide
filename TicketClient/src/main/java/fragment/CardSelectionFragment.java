package fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import activity.GameOverActivity;
import asynctask.DiscardDestinationCardsTask;
import asynctask.DrawDestinationCardTask;
import cs340.byu.tickettoride.R;
import fragment.adapter.DestinationCardHandAdapter;
import fragment.adapter.TrainCardDeckAdapter;
import fragment.adapter.TrainCardHandAdapter;
import model.DestinationCard;
import model.DestinationCardDeck;
import model.DestinationCardHand;
import model.Game;
import model.Player;
import model.TrainCardDeck;
import model.TrainCardHand;
import model.data.ClientModel;
import model.facade.GameModelFacade;
import model.facade.PreGameModelFacade;

/**
 * A simple {@link Fragment} subclass. Provides a presenter for the Card Selection view.
 */
public class CardSelectionFragment extends Fragment implements Observer {


    private RecyclerView mPlayerTrainCardHandView;
    private RecyclerView mPlayerDestinationCardHandView;
    private RecyclerView mDeckView;
    private TextView mTrainDeckSize;
    private TextView mDestinationDeckSize;
    private Button mDrawDestinationCardsButton;


    public CardSelectionFragment() {
        ClientModel.getInstance().addObserver(this);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Inflate the layout for this fragment
        View cardView =  inflater.inflate(R.layout.fragment_card_selection, container, false);

        // DO WHATEVER MODIFICATIONS YOU NEED HERE! ENJOY

        mPlayerTrainCardHandView = (RecyclerView) cardView.findViewById(R.id.playerTrainCardHandRecyclerView);
        mPlayerDestinationCardHandView = (RecyclerView) cardView.findViewById(R.id.playerDestinationCardHandRecyclerView);
        mDeckView = (RecyclerView) cardView.findViewById(R.id.deckListRecyclerView);
        mDrawDestinationCardsButton = (Button) cardView.findViewById(R.id.drawDestinationCardsButton);
        mTrainDeckSize = (TextView) cardView.findViewById(R.id.trainCardDeckSize);
        mDestinationDeckSize = (TextView) cardView.findViewById(R.id.destinationCardDeckSize);


        Game myGame = PreGameModelFacade.getGameByID(ClientModel.getInstance().myJoinedGame);
        Player myPlayer = myGame.getPlayerByName(GameModelFacade.getUser().getUsername());
        TrainCardDeck deck = myGame.getTrainDeck();
        TrainCardHand hand = myGame.getTrainCardHandForPlayer(myPlayer.getUsername());
        DestinationCardHand destinationCardHand = myGame.getDestinationCardHandForPlayer(myPlayer.getUsername());
        DestinationCardDeck destinationCardDeck = myGame.getDestinationCardDeck();

        /** Button stuff */

        buildButton(destinationCardHand);

        /** Player train hand stuff */
        LinearLayoutManager handLayoutManager = new LinearLayoutManager(getContext());
        handLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mPlayerTrainCardHandView.setLayoutManager(handLayoutManager);

        TrainCardHandAdapter handAdapter = new TrainCardHandAdapter(hand,getContext());
        mPlayerTrainCardHandView.setAdapter(handAdapter);


        /** Train deck stuff */
        LinearLayoutManager deckLayoutManger = new LinearLayoutManager(getContext());
        deckLayoutManger.setOrientation(LinearLayoutManager.HORIZONTAL);
        mDeckView.setLayoutManager(deckLayoutManger);

        TrainCardDeckAdapter deckAdapter = new TrainCardDeckAdapter(deck,getContext(), this);
        mDeckView.setAdapter(deckAdapter);

        mTrainDeckSize.setText("Train Deck: " + String.valueOf(deck.getDeckSize()));

        /** Destination hand stuff */

        LinearLayoutManager destinationHandLayoutManager = new LinearLayoutManager(getContext());
        destinationHandLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mPlayerDestinationCardHandView.setLayoutManager(destinationHandLayoutManager);

        DestinationCardHandAdapter destinationCardHandAdapter = new DestinationCardHandAdapter(destinationCardHand,getContext());
        mPlayerDestinationCardHandView.setAdapter(destinationCardHandAdapter);

        mDestinationDeckSize.setText("Dest. Deck: " + String.valueOf(destinationCardDeck.getDeckSize()));
        return cardView;
    }

    private void buildButton(DestinationCardHand destinationCardHand)
    {
        if(!hasDiscardableCards(destinationCardHand)) {
            mDrawDestinationCardsButton.setText("Draw Dest. Cards");
            mDrawDestinationCardsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    drawDestinationCards();

                }
            });
        }
        else {
            mDrawDestinationCardsButton.setText("Accept");
            mDrawDestinationCardsButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    discardDestinationCards();

                }
            });
        }

    }

    @Override
    public void update(Observable o, Object arg){
        FragmentManager manager = getFragmentManager();
        if (manager == null) return;
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.detach(this);
        transaction.attach(this);
        transaction.commit();

        if(arg != null){
            Intent gameIntent = new Intent(this.getActivity(), GameOverActivity.class);
            startActivity(gameIntent);
        }
    }

    protected void drawDestinationCards() {
        new DrawDestinationCardTask(ClientModel.getInstance().getUser().getUsername(),ClientModel.getInstance().myJoinedGame).execute(this);
        new DrawDestinationCardTask(ClientModel.getInstance().getUser().getUsername(),ClientModel.getInstance().myJoinedGame).execute(this);
        new DrawDestinationCardTask(ClientModel.getInstance().getUser().getUsername(),ClientModel.getInstance().myJoinedGame).execute(this);
    }

    protected void discardDestinationCards() {
        Game game = ClientModel.getInstance().getGameByID(ClientModel.getInstance().myJoinedGame);
        String username = ClientModel.getInstance().getUser().getUsername();
        DestinationCardHand hand = game.getDestinationCardHandForPlayer(username);
        List<DestinationCard> cardsToDiscard = new ArrayList<>();
        for(DestinationCard card : hand.getHand()) {
            if(card.getSelectedToDiscard()) {
                cardsToDiscard.add(card);
            }
        }


        new DiscardDestinationCardsTask(username,game.getGameID(),cardsToDiscard).execute(this);

    }

    private Boolean hasDiscardableCards(DestinationCardHand hand) {
        for(DestinationCard card : hand.getHand()) {
            if(card.canBeDiscarded()) {
                return true;
            }
        }
        return false;
    }

    public void printSuccessToast(boolean success){
        if(success){
            Toast.makeText(this.getContext(), "Good job! Destination cards saved.", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this.getContext(), "You can't discard that many destination cards!", Toast.LENGTH_SHORT).show();
        }
    }
}
