package serverproxy;

import java.util.List;

import model.Color;
import model.DestinationCard;
import model.MapRoute;
import request.LoginRequest;
import request.RegisterRequest;
import response.ClaimRouteResponse;
import response.CommandResponse;
import response.LoginResponse;
import response.PollerResponse;
import response.RegisterResponse;

/**
 * Created by Andrew Jacobson on 7/2/2017.
 */

public class ServerFacade
{
    /**
     * @param serverIP the IP of the server.
     */
    public static void setIP(String serverIP)
    {
        InitGameProxy.setServerIP(serverIP);
    }

    /**
     * Retrieves a register response for the given user.
     * @param username the username to register.
     * @param password the user's password.
     * @return a response from the server.
     */
    public static RegisterResponse register(String username, String password)
    {
        return InitGameProxy.register(new RegisterRequest(username, password));
    }

    /**
     * Retrieves a login response from the server.
     * @param username the name of the user to login.
     * @param password the user's password.
     * @return a login response from the server.
     */
    public static LoginResponse login(String username, String password)
    {
        return InitGameProxy.login(new LoginRequest(username, password));
    }

    /**
     * Joins a player to a specified game.
     * @param gameID the id of the game to join the player to.
     * @return a command response with regards to the game.
     */
    public static CommandResponse joinGame(String gameID) {

        return InitGameProxy.joinGame(gameID);
    }

    /**
     * Sends a request to the game to create a game.
     * @param maxPlayers the maximum amount of players to add to the game.
     * @return a response about the game status.
     */
    public static CommandResponse createGame(int maxPlayers) {

        return InitGameProxy.createGame(maxPlayers);
    }

    public static CommandResponse drawTrainCard(String username, String gameID, int index) {
        return InitGameProxy.drawTrainCard(username,gameID,index);
    }

    public CommandResponse drawDestinationCard(String username, String gameID)
    {
        return InitGameProxy.drawDestinationCard(username,gameID);
    }

    /**
     * Sends a chat message to the server.
     * @param gameID the id of the game the message belongs to.
     * @param message the chat message.
     * @return a response about the chat message.
     */
    public static CommandResponse sendChatMessage(String gameID, String message) {
        return InitGameProxy.sendChat(message, gameID);
    }


    /**
     * Polls the server.
     * @return a response from the server.
     */
    public static PollerResponse poll(int lastIndex) {
        return InitGameProxy.poll(lastIndex);
    }

    public static CommandResponse startGame(String gameID) {
        return InitGameProxy.startGame(gameID);
    }

    public static ClaimRouteResponse claimRoute(MapRoute route, String username, String gameID, Color colorToUse)
    {
        //kendall look at the example of the create game stuff

        return InitGameProxy.claimRoute(route, username, gameID,colorToUse);
    }


    public CommandResponse discardDestinationCard(String username, String gameID, List<DestinationCard> cardsToDiscard)
    {
        return InitGameProxy.discardDestinationCard(username,gameID,cardsToDiscard);
    }
}