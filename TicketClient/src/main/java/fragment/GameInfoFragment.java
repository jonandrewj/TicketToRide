package fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import activity.GameOverActivity;
import fragment.adapter.PlayerListAdapter;
import model.Game;
import model.data.ClientModel;
import cs340.byu.tickettoride.R;
import model.facade.PreGameModelFacade;
import activity.GamePlayActivity;

/**
 * Created by kendall on 7/7/2017.
 *
 * Provides a presenter for the GameInfo view.
 */

public class GameInfoFragment extends Fragment implements Observer {

    // View
    private View preGameView;
    
    // Text that displays how many players are remaining
    private TextView mPlayersRemaining;

    // For the recyclerView
    private ListView mPlayerList;
    private PlayerListAdapter playerListAdapter;
    private List<String> currentPlayers;

    public GameInfoFragment(){
        playerListAdapter = null;
        currentPlayers = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        preGameView = inflater.inflate(R.layout.fragment_pre_game_view, container, false);

        mPlayerList = (ListView) preGameView.findViewById(R.id.playerList);
        mPlayersRemaining = (TextView) preGameView.findViewById(R.id.playersWaitingFor);

        Game myGame = PreGameModelFacade.getGameByID(ClientModel.getInstance().myJoinedGame);
        int playersRemaining = myGame.getPlayerMax() - myGame.getPlayerCount();
        mPlayersRemaining.setText("Waiting for " + playersRemaining + " more players");

        if (!ClientModel.getInstance().myJoinedGame.equals("")){
            if (PreGameModelFacade.getGameByID(
                    ClientModel.getInstance().myJoinedGame).isGameStarted()){
                // My game is already going --
                startGameEvent();
            }
        }

        playerListAdapter = new PlayerListAdapter(this.getContext());
        for (int i = 0; i < myGame.getPlayerCount(); i++){
            playerListAdapter.addPlayer(myGame.getPlayers().get(i).getUsername());
        }
        mPlayerList.setAdapter(playerListAdapter);

        return preGameView;
    }

    @Override
    public void onStart() {
        super.onStart();
        ClientModel.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ClientModel.getInstance().deleteObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.detach(this);
        transaction.attach(this);
        transaction.commit();

        if(arg != null){
            Intent gameIntent = new Intent(this.getActivity(), GameOverActivity.class);
            startActivity(gameIntent);
        }
    }

    /**
     * Called when a game is to be started.
     */
    public void startGameEvent(){
        if (!ClientModel.getInstance().isInGame) {
            ClientModel.getInstance().isInGame = true;
            Intent gameIntent = new Intent(getActivity(), GamePlayActivity.class);
            startActivity(gameIntent);
        }
    }
}
