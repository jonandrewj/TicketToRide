package model;

/**
 * Created by Alyx on 7/9/17.
 */


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static model.Color.BLACK;
import static model.Color.BLUE;
import static model.Color.WILD;
import static model.Color.GREEN;
import static model.Color.ORANGE;
import static model.Color.PINK;
import static model.Color.RED;
import static model.Color.WHITE;
import static model.Color.YELLOW;

public class Game implements IGame{

    public enum Gamestate {
        SETUP,
        PLAY,
        LAST_TURN,
        END,
    }


    // DATA FIELDS ---------------------------------------------------------------------------------

    /** Number of players possible for this game (Always between 2 and 5) */
    private int playerMax;

    /** Unique identifier for the game */
    private String gameID;

    /** Indicator of game status. Options: started (true) or not started (false) */
    private boolean isStarted;

    /**
     * Indicates if the turn rotation has begun.
     */
    private boolean turnsStarted = false;

    /** Players in game */
    private List<Player> players;

    private List<TrainCardHand> trainCardHands;

    private List<DestinationCardHand> destinationCardHands;

    private TrainCardDeck trainDeck;

    private DestinationCardDeck destinationCardDeck;
    //public ArrayList<MapRoute> routes;
    /** used to populate cities*/
    ArrayList<MapCity> mapCities = new ArrayList<>();

    /** used to populate routes*/
    ArrayList<MapRoute> mapRoutes = new ArrayList<>();

    /** used to populate routes*/
    Map<String, MapCity> cityMap = new HashMap<>();

    private Gamestate gamestate;

    // CONSTRUCTORS --------------------------------------------------------------------------------
    public Game(){
        gamestate = Gamestate.SETUP;
        trainCardHands = new ArrayList<>();
        destinationCardDeck = new DestinationCardDeck();
        destinationCardHands = new ArrayList<>();
        gameID = UUID.randomUUID().toString();

        // Default max = 5;
        this.playerMax = 5;
        // Game does not start upon creation...
        isStarted = false;

        //populate datastructures for routes;

        initializeCityList();
        initializeCityMap();
        populateRoutes();

        players = new ArrayList<>();
    }

    public Game(int playerMax){
        gamestate = Gamestate.SETUP;
        trainCardHands = new ArrayList<>();
        destinationCardDeck = new DestinationCardDeck();
        destinationCardHands = new ArrayList<>();
        gameID = "";
        // Inputted max # of players (User inputted)
        this.playerMax = playerMax;

        // Game does not start upon creation...
        isStarted = false;

        players = new ArrayList<>();


        initializeCityList();
        initializeCityMap();
        populateRoutes();
    }

    public Game(int playerMax, String gameID){
        gamestate = Gamestate.SETUP;
        trainCardHands = new ArrayList<>();
        destinationCardDeck = new DestinationCardDeck();
        destinationCardHands = new ArrayList<>();
        this.gameID = gameID;

        // Inputted max # of players (User inputted)
        this.playerMax = playerMax;

        // Game does not start upon creation...
        isStarted = false;

        players = new ArrayList<>();

        initializeCityList();
        initializeCityMap();
        populateRoutes();
    }

    public void buildTrainCardDeck(int wilds, int others) {
        trainDeck = new TrainCardDeck(wilds,others);
    }

    // GETTERS AND SETTERS -------------------------------------------------------------------------

    public Gamestate getGamestate()
    {
        return gamestate;
    }


    public void setGamestate(Gamestate gamestate)
    {
        this.gamestate = gamestate;
    }

    /**
     * Gets the total amount of players active in the game.
     * @return the number of players that have joined.
     */
    public int getPlayerCount() {
        return this.players.size();
    }

    /**
     * Gets the maximum of players that could join.
     * @return the set max of players.
     */
    public int getPlayerMax() {
        return playerMax;
    }

    /**
     * Sets how many players can join the game.
     * @param playerMax The amount of players to allow.
     */
    public void setPlayerMax(int playerMax) {
        this.playerMax = playerMax;
    }

    /**
     * Gets the id of the game.
     * @return The game id.
     */
    public String getGameID() {
        return gameID;
    }

    /**
     * Sets the game ID.
     * @param gameID the new game id.
     */
    public void setGameID(String gameID) {
        this.gameID = gameID;
    }

    /**
     * Determines if the game has started.
     * @return true if the game has begun; otherwise, false.
     */
    public boolean isGameStarted() {
        return isStarted;
    }

    /**
     * Sets the game start state.
     * @param gameStarted Whether or not to start the game.
     */
    public void setGameStarted(boolean gameStarted) {
        this.isStarted = gameStarted;
    }

    public List<Player> getPlayers() {
        return players;
    }

    // OVERRIDE METHODS ----------------------------------------------------------------------------


    /**
     * Determines if two games are equal.
     *
     * Note: It's been decided that equal gameId's mean equal games.
     *
     * @param o The object to compare to.
     * @return true if the games are equal, otherwise, false.
     */
    @Override
    public boolean equals(Object o){
        // Null case
        if(o == null){
            return false;
        }

        // The same object case
        if(o == this){
            return true;
        }

        // Different class case
        if(o.getClass() != this.getClass()){
            return false;
        }

        // Cast object
        Game that = (Game) o;

        // Compare gameID's only, and make decision
        if(this.getGameID().equals(that.getGameID())){
            return true;
        }

        return false;
    }

    @Override
    public int hashCode(){
        return gameID.hashCode() * 50 + getPlayerCount() * playerMax;
    }

    // SIMPLE ACCESSORS ----------------------------------------------------------------------------


    /**
     * Adds a player to the game.
     * @param username name of the player.
     */
    public void addPlayer(String username) {
        players.add(new Player(username));
        trainCardHands.add(new TrainCardHand(username));
        destinationCardHands.add(new DestinationCardHand(username));
    }

    /**
     * Returns a specifies player object if the username is correct
     * @param username
     * @return
     */
    public Player getPlayerByName(String username) {
        for(Player player : players) {
            if (player.getUsername().equals(username)) {
                return player;
            }
        }
        return null;
    }

    /**
     * Removes the player by the given username.
     * @param username The name of the player to remove. If the player isn't found, it isn't removed.
     */
    public void removePlayer(String username) {
        for (Player player : players) {
            if(player.getUsername().equals(username)){
                players.remove(player);
                return;
            }
        }
    }

    public boolean claimRoute(MapRoute routeToClaim, String username, Color colorToUse) {
        MapRoute route = getRoute(routeToClaim);
        Player player = getPlayerByName(username);
        TrainCardHand hand = getTrainCardHandForPlayer(username);

        route.setOwnerID(username);
        route.setRouteClaimed(true);

        int normalCardsToTake = Math.min(hand.getCardCount(colorToUse),route.getRouteLength());
        int wildCardsToTake = Math.max(route.getRouteLength() - normalCardsToTake, 0);

        hand.removeCardFromHand(colorToUse,normalCardsToTake);
        hand.removeCardFromHand(Color.WILD,wildCardsToTake);

        trainDeck.addToDiscard(colorToUse,normalCardsToTake);
        trainDeck.addToDiscard(Color.WILD,wildCardsToTake);

        player.useTrainPieces(route.getRouteLength());
        player.addScoreFromRoutes(route.getRouteWorth());

        return true;
    }

    private MapRoute getRoute(MapRoute routeToClaim)
    {
        for(MapRoute route : mapRoutes) {
            if(route.equals(routeToClaim)) {
                return route;
            }
        }
        return null;
    }

    public boolean isJoinable(){
        return getPlayerCount() < getPlayerMax();
    }

    public void populateRoutes()
    {

        mapRoutes.add(new MapRoute(cityMap.get("Vancouver"), cityMap.get("Calgary"), 3, WILD, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("Calgary"), cityMap.get("Winnipeg"), 6, WHITE, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("Portland"), cityMap.get("Salt Lake City"), 6, BLUE, 0 , 0));
        mapRoutes.add(new MapRoute(cityMap.get("Vancouver"), cityMap.get("Seattle"), 1, WILD, -15 , 0));
        mapRoutes.add(new MapRoute(cityMap.get("Vancouver"), cityMap.get("Seattle"), 1, WILD, 15 , 0));
        mapRoutes.add(new MapRoute(cityMap.get("Portland"), cityMap.get("Seattle"), 1, WILD, -15 , 0));
        mapRoutes.add(new MapRoute(cityMap.get("Portland"), cityMap.get("Seattle"), 1, WILD, 15 , 0));
        mapRoutes.add(new MapRoute(cityMap.get("Portland"), cityMap.get("San Francisco"), 6, GREEN, -15 , 0));
        mapRoutes.add(new MapRoute(cityMap.get("Portland"), cityMap.get("San Francisco"), 6, PINK, 15 , 0));
        mapRoutes.add(new MapRoute(cityMap.get("Los Angeles"), cityMap.get("El Paso"), 6, BLACK, 0 , 50));
        mapRoutes.add(new MapRoute(cityMap.get("Los Angeles"), cityMap.get("Phoenix"), 3, WILD, 0 , 40));
        mapRoutes.add(new MapRoute(cityMap.get("Los Angeles"), cityMap.get("Las Vegas"), 2, WILD, 0 ,30));
        mapRoutes.add(new MapRoute(cityMap.get("San Francisco"), cityMap.get("Salt Lake City"), 5, ORANGE, -10, 20));
        mapRoutes.add(new MapRoute(cityMap.get("San Francisco"), cityMap.get("Salt Lake City"), 5, WHITE, -10, 50));
        mapRoutes.add(new MapRoute(cityMap.get("Las Vegas"), cityMap.get("Salt Lake City"), 3, ORANGE, -10, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Phoenix"), cityMap.get("Santa Fe"), 3, WILD, -10, 30));
        mapRoutes.add(new MapRoute(cityMap.get("San Francisco"), cityMap.get("Los Angeles"), 3, PINK, -30, 40));
        mapRoutes.add(new MapRoute(cityMap.get("San Francisco"), cityMap.get("Los Angeles"), 3, YELLOW, -10, 20));
        mapRoutes.add(new MapRoute(cityMap.get("Phoenix"), cityMap.get("Denver"), 5, WHITE, 0, 40));
        mapRoutes.add(new MapRoute(cityMap.get("Santa Fe"), cityMap.get("Oklahoma City"), 3, BLUE, 10, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Denver"), cityMap.get("Omaha"), 4, PINK, 0, 0));

        mapRoutes.add(new MapRoute(cityMap.get("El Paso"), cityMap.get("Houston"), 6, GREEN, 0, 60));
        mapRoutes.add(new MapRoute(cityMap.get("Houston"), cityMap.get("New Orleans"), 2, WILD, 0, 60));

        mapRoutes.add(new MapRoute(cityMap.get("New Orleans"), cityMap.get("Miami"), 6, RED, -10, 60));
        mapRoutes.add(new MapRoute(cityMap.get("Seattle"), cityMap.get("Calgary"), 4, WILD, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("Seattle"), cityMap.get("Helena"), 6, YELLOW, 0, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Helena"), cityMap.get("Duluth"), 6, ORANGE, -10, 10));

        // rout distance update to here

        mapRoutes.add(new MapRoute(cityMap.get("Winnipeg"), cityMap.get("Sault St Marie"), 6, WILD, -10, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Sault St Marie"), cityMap.get("Montreal"), 5, BLACK, -10, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Montreal"), cityMap.get("Boston"), 2, WILD, -10, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Montreal"), cityMap.get("Boston"), 2, WILD, 20, -10));
        mapRoutes.add(new MapRoute(cityMap.get("Boston"), cityMap.get("New York"), 2, RED, 20, 20));
        mapRoutes.add(new MapRoute(cityMap.get("Boston"), cityMap.get("New York"), 2, YELLOW, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("Montreal"), cityMap.get("New York"), 3, BLUE, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("Montreal"), cityMap.get("Toronto"), 3, WILD, 10, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Sault St Marie"), cityMap.get("Toronto"), 2, WILD, 10, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Winnipeg"), cityMap.get("Duluth"), 4, BLACK, 10, 10));
        //good to here

        mapRoutes.add(new MapRoute(cityMap.get("Helena"), cityMap.get("Omaha"), 5, RED, 10, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Helena"), cityMap.get("Salt Lake City"), 3, PINK, 10, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Helena"), cityMap.get("Denver"), 4, GREEN, 10, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Duluth"), cityMap.get("Omaha"), 2, WILD, -5, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Duluth"), cityMap.get("Omaha"), 2, WILD, 20, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Denver"), cityMap.get("Kansas City"), 4, BLACK, 10, 20));
        mapRoutes.add(new MapRoute(cityMap.get("Denver"), cityMap.get("Kansas City"), 4, ORANGE, 10, 50));
        mapRoutes.add(new MapRoute(cityMap.get("Denver"), cityMap.get("Oklahoma City"), 4, RED, 10, 30));


        mapRoutes.add(new MapRoute(cityMap.get("Santa Fe"), cityMap.get("Oklahoma City"), 3, BLUE, 10, 30));
        mapRoutes.add(new MapRoute(cityMap.get("New York"), cityMap.get("Washington"), 2, ORANGE, 0, 30));
        mapRoutes.add(new MapRoute(cityMap.get("New York"), cityMap.get("Washington"), 2, BLACK, 25, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Washington"), cityMap.get("Raleigh"), 2, WILD, -10, 20));
        mapRoutes.add(new MapRoute(cityMap.get("Washington"), cityMap.get("Raleigh"), 2, WILD, 15, 40));
        mapRoutes.add(new MapRoute(cityMap.get("Washington"), cityMap.get("Pittsburgh"), 2, WILD, 10, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Raleigh"), cityMap.get("Charleston"), 2, WILD, 10, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Raleigh"), cityMap.get("Atlanta"), 2, WILD, -3, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Raleigh"), cityMap.get("Atlanta"), 2, WILD, 15, 50));
        mapRoutes.add(new MapRoute(cityMap.get("Raleigh"), cityMap.get("Nashville"), 3, BLACK, 10, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Raleigh"), cityMap.get("Pittsburgh"), 2, WILD, 10, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Charleston"), cityMap.get("Miami"), 4, PINK, 10, 40));
        mapRoutes.add(new MapRoute(cityMap.get("New Orleans"), cityMap.get("Little Rock"), 3, GREEN, 0, 40));
        mapRoutes.add(new MapRoute(cityMap.get("Nashville"), cityMap.get("Atlanta"), 1, WILD, 10, 50));
        mapRoutes.add(new MapRoute(cityMap.get("Nashville"), cityMap.get("Little Rock"), 3, WHITE, 10, 50));
        mapRoutes.add(new MapRoute(cityMap.get("Oklahoma City"), cityMap.get("Little Rock"), 2, WILD, 10, 50));

        mapRoutes.add(new MapRoute(cityMap.get("Nashville"), cityMap.get("Saint Louis"), 2, WILD, 0, 30));
        mapRoutes.add(new MapRoute(cityMap.get("Saint Louis"), cityMap.get("Chicago"), 2, WHITE, 15, 40));
        mapRoutes.add(new MapRoute(cityMap.get("Saint Louis"), cityMap.get("Chicago"), 2, GREEN, -15, 30));
        // good to here
        mapRoutes.add(new MapRoute(cityMap.get("Dallas"), cityMap.get("Oklahoma City"), 2, WILD, -15, 40));
        mapRoutes.add(new MapRoute(cityMap.get("Dallas"), cityMap.get("Oklahoma City"), 2, WILD, 15, 40));
        mapRoutes.add(new MapRoute(cityMap.get("Dallas"), cityMap.get("Houston"), 1, WILD, -15, 50));
        mapRoutes.add(new MapRoute(cityMap.get("Dallas"), cityMap.get("Houston"), 1, WILD, 15, 50));

        mapRoutes.add(new MapRoute(cityMap.get("Pittsburgh"), cityMap.get("Chicago"), 3, BLACK, -15, 15));
        mapRoutes.add(new MapRoute(cityMap.get("Pittsburgh"), cityMap.get("Chicago"), 3, ORANGE, -15, 45));
        //mapRoutes.add(new MapRoute(cityMap.get("Dallas"), cityMap.get("Oklahoma City"), 2, WILD, -15, 40));
        //mapRoutes.add(new MapRoute(cityMap.get("Dallas"), cityMap.get("Oklahoma City"), 2, WILD, 15, 40));

        //good to here;
        mapRoutes.add(new MapRoute(cityMap.get("Dallas"), cityMap.get("Little Rock"), 2, WILD, 0, 30));


        mapRoutes.add(new MapRoute(cityMap.get("Dallas"), cityMap.get("El Paso"), 4, RED, 0, 50));
        mapRoutes.add(new MapRoute(cityMap.get("El Paso"), cityMap.get("Oklahoma City"), 2, YELLOW, 0, 40));



        mapRoutes.add(new MapRoute(cityMap.get("El Paso"), cityMap.get("Santa Fe"), 2, WILD, 0, 50));
        mapRoutes.add(new MapRoute(cityMap.get("Denver"), cityMap.get("Santa Fe"), 2, WILD, 0, 40));

        //good here
        mapRoutes.add(new MapRoute(cityMap.get("Salt Lake City"), cityMap.get("Denver"), 3, RED, 0, 5));
        mapRoutes.add(new MapRoute(cityMap.get("Salt Lake City"), cityMap.get("Denver"), 3, YELLOW, 0, 35));

        mapRoutes.add(new MapRoute(cityMap.get("Oklahoma City"), cityMap.get("Kansas City"), 2, WILD, -15, 40));
        mapRoutes.add(new MapRoute(cityMap.get("Oklahoma City"), cityMap.get("Kansas City"), 2, WILD, 15, 40));
        mapRoutes.add(new MapRoute(cityMap.get("Calgary"), cityMap.get("Helena"), 4, WILD, 0, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Winnipeg"), cityMap.get("Helena"), 4, BLUE, 0, 10));
        mapRoutes.add(new MapRoute(cityMap.get("Duluth"), cityMap.get("Sault St Marie"), 3, WILD, 15, 0));
        //mapRoutes.add(new MapRoute(cityMap.get("Oklahoma City"), cityMap.get("Kansas City"), 2, WILD, 15, 40));

        //good here
        mapRoutes.add(new MapRoute(cityMap.get("Atlanta"), cityMap.get("Charleston"), 2, WILD, 0, 50));
        mapRoutes.add(new MapRoute(cityMap.get("Atlanta"), cityMap.get("Miami"), 5, BLUE, 15, 40));
        mapRoutes.add(new MapRoute(cityMap.get("New Orleans"), cityMap.get("Atlanta"), 4, YELLOW, -10, 40));
        mapRoutes.add(new MapRoute(cityMap.get("New Orleans"), cityMap.get("Atlanta"), 4, ORANGE, 10, 55));
        mapRoutes.add(new MapRoute(cityMap.get("Little Rock"), cityMap.get("Saint Louis"), 2, WILD, 0, 55));
        mapRoutes.add(new MapRoute(cityMap.get("Kansas City"), cityMap.get("Saint Louis"), 2, PINK, 0, 65));

        mapRoutes.add(new MapRoute(cityMap.get("Kansas City"), cityMap.get("Saint Louis"), 2, BLUE, 0, 35));
        mapRoutes.add(new MapRoute(cityMap.get("Saint Louis"), cityMap.get("Pittsburgh"), 5, GREEN, 0, 35));
        mapRoutes.add(new MapRoute(cityMap.get("Toronto"), cityMap.get("Pittsburgh"), 2, WILD, 0, 15));

        mapRoutes.add(new MapRoute(cityMap.get("Duluth"), cityMap.get("Toronto"), 6, PINK, 0, 5));
        mapRoutes.add(new MapRoute(cityMap.get("Toronto"), cityMap.get("Chicago"), 4, WHITE, 0, 15));
        mapRoutes.add(new MapRoute(cityMap.get("Duluth"), cityMap.get("Chicago"), 3, RED, 0, 15));
        mapRoutes.add(new MapRoute(cityMap.get("Omaha"), cityMap.get("Chicago"), 4, BLUE, 0, 15));
        mapRoutes.add(new MapRoute(cityMap.get("Omaha"), cityMap.get("Kansas City"), 1, WILD, -15, 15));
        mapRoutes.add(new MapRoute(cityMap.get("Omaha"), cityMap.get("Kansas City"), 1, WILD, 15, 15));
        mapRoutes.add(new MapRoute(cityMap.get("New York"), cityMap.get("Pittsburgh"), 2, WHITE, 0, 15));
        mapRoutes.add(new MapRoute(cityMap.get("New York"), cityMap.get("Pittsburgh"), 2, GREEN, 0, 45));
        mapRoutes.add(new MapRoute(cityMap.get("Nashville"), cityMap.get("Pittsburgh"), 4, YELLOW, 0, 20));

    }

    public void initializeCityList(){

        mapCities.add(new MapCity("Miami", 1471, 849));
        mapCities.add(new MapCity("Atlanta", 1302, 688));
        mapCities.add(new MapCity("Boston", 1699, 267));
        mapCities.add(new MapCity("Calgary", 380, 90));
        mapCities.add(new MapCity("Charleston", 1470, 682));
        mapCities.add(new MapCity("Vancouver", 168, 95));
        mapCities.add(new MapCity("Seattle", 157, 165));
        mapCities.add(new MapCity("Portland", 123, 234));
        mapCities.add(new MapCity("San Francisco", "SF", 85, 458));
        mapCities.add(new MapCity("Los Angeles", "LA", 166, 583));
        mapCities.add(new MapCity("Phoenix", 374, 605));
        mapCities.add(new MapCity("El Paso", "EP", 611, 727));
        mapCities.add(new MapCity("Dallas", 888, 716));
        mapCities.add(new MapCity("Houston", 948, 789));
        mapCities.add(new MapCity("New Orleans", "NO", 1075, 774));
        mapCities.add(new MapCity("Winnipeg", 772, 136));
        mapCities.add(new MapCity("Sault St Marie", "SM", 1171, 231));
        mapCities.add(new MapCity("Montreal", "MT", 1512, 142));
        mapCities.add(new MapCity("Toronto", 1395, 297));
        mapCities.add(new MapCity("Helena", 539, 300));
        mapCities.add(new MapCity("Pittsburgh", 1386, 466));
        mapCities.add(new MapCity("Duluth", 900, 300));
        mapCities.add(new MapCity("Las Vegas", "LV", 311, 527));
        mapCities.add(new MapCity("Santa Fe", 600, 550));
        mapCities.add(new MapCity("Salt Lake City", "SL", 411, 407));
        mapCities.add(new MapCity("Denver", 655, 439));
        mapCities.add(new MapCity("Oklahoma City", "Oc", 841, 606));
        mapCities.add(new MapCity("Little Rock", "LR", 950, 609));
        mapCities.add(new MapCity("Nashville", "TN", 1200, 667));
        mapCities.add(new MapCity("Raleigh", 1420, 612));
        mapCities.add(new MapCity("Saint Louis", "SL", 1175, 571));
        mapCities.add(new MapCity("Kansas City", "KC", 933, 486));
        mapCities.add(new MapCity("Washington", 1577, 527));
        mapCities.add(new MapCity("New York", "NT", 1595, 403));
        mapCities.add(new MapCity("Chicago", "CH", 1195, 469));
        mapCities.add(new MapCity("Omaha", "OM", 900, 397));
    }

    public void initializeCityMap(){
        for (MapCity city : mapCities){
            cityMap.put(city.getName(), city);
        }
    }

    public ArrayList<MapRoute> getMapRoutes() {
        return mapRoutes;
    }

    public TrainCardDeck getTrainDeck()
    {
        return trainDeck;
    }

    public void setTrainDeck(TrainCardDeck trainDeck)
    {
        this.trainDeck = trainDeck;
    }

    public TrainCardHand getTrainCardHandForPlayer(String username)
    {
        for(TrainCardHand hand : trainCardHands) {
            if (hand.getUsername().equals(username)) {
                return hand;
            }
        }
        return null;
    }

    public DestinationCardHand getDestinationCardHandForPlayer(String username)
    {
        for(DestinationCardHand hand : destinationCardHands) {
            if (hand.getUsername().equals(username)) {
                return hand;
            }
        }
        return null;
    }

    public DestinationCardDeck getDestinationCardDeck()
    {
        return destinationCardDeck;
    }

    public List<DestinationCardHand> getDestinationCardHands()
    {
        return destinationCardHands;
    }
    
    /**
     * Retrieves the player with the specified turn ordering.
     * @param order the zero-based index of the player's turn order.
     * @return the player whose turn number is the given integer.
     */
    public Player findPlayerByTurnOrder(int order) {
        if (order >= getPlayerMax()) throw new IndexOutOfBoundsException("There are less players than this turn.");
        for (Player p : getPlayers()) {
            if (p.getOrder() == order) {
                return p;
            }
        }
        return null;
    }

    public boolean haveTurnsStarted() {
        return turnsStarted;
    }

    public void setTurnsStarted(boolean turnsStarted) {
        this.turnsStarted = turnsStarted;
    }
}
