package activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Paint;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import asynctask.ClaimRouteTask;
import model.Color;
import model.Game;
import model.MapRoute;
import model.TrainCardHand;
import model.data.ClientModel;
import model.data.GameMap;
import model.facade.PreGameModelFacade;
import serverproxy.ServerFacade;

/**
 * Created by kendall on 7/15/2017.
 * @invariant  number of routes will be constant.
 * @invariant there will always be one canvas
 *
 */


public class RouteSelectedAlertDialogue {
    AlertDialog.Builder routeDisplayer;
    GamePlaySingleton gamePlaySingleton = GamePlaySingleton.Inst();
    List<MapRoute> routesToDisplay;
    private ServerFacade facade;
    private Context context;
    AlertDialog dialog;


    /**
     *
     * @param context, this is the context from teh gameplayActivity
     */
    public RouteSelectedAlertDialogue(Context context)
    {
        this.context = context;
        routeDisplayer = new AlertDialog.Builder(context);
        facade = new ServerFacade();

    }

    /**
     *
    * @param cityName is the city that was selected by the use. All routes connected to that city are to be displayed
   * @pre: needs list of current available routes
    *@post: displays via alertDialog available routes from selected city.
    *@post:user can select route from display and it will give route to player and remove route from list.
     */
    public List<MapRoute> displayAvailableRoutes(String cityName)
    {
        //List<MapRoute> updatedRoutes = GameMap.getMap().mapRoutes;
        List<String> routeInformation = new ArrayList<>();

        List<MapRoute> availableRoutes = PreGameModelFacade.getGameByID(ClientModel.getInstance().getMyJoinedGame()).getMapRoutes();

        Game thisGame = PreGameModelFacade.getGameByID(ClientModel.getInstance().myJoinedGame);

        routesToDisplay = new ArrayList<>();
        for (MapRoute route : availableRoutes) {

             if (route.getCity1().getName().equals(cityName) || route.getCity2().getName().equals(cityName) ) {
                 if (!route.getRouteClaimed()) {
                     if (parallelRouteRule(route, thisGame.getPlayerCount(), availableRoutes)) {
                         routesToDisplay.add(route);
                         routeInformation.add("MapRoute: " + route.getCity1().getName() +
                                 " to " + route.getCity2().getName() + ", Color: " +
                                 getColor(route) + ", Lenght: " + route.getRouteLength());
                     }
                 }

             }
        }
        String[] routeToShow = new String[routesToDisplay.size()];
        for (int i = 0; i < routesToDisplay.size(); i++) {
            routeToShow[i] = routeInformation.get(i);
        }
        makeBuilder(routeToShow);
        return availableRoutes;
    }

    private boolean parallelRouteRule(MapRoute route, int numGamePlayers, List<MapRoute> gameRoutes){
        if (numGamePlayers >=4){
            return true;
        }
        boolean donotshow = false;
        for (MapRoute r : gameRoutes){
            if (r.getCity1().getName().equals(route.getCity1().getName()) && r.getCity2().getName().equals(route.getCity2().getName())){
                if (r.getRouteClaimed()){
                    donotshow = true;
                }
            }
        }
        return !donotshow;
    }

    /**
    *@param routeInfo, contains all the route info needed to be displyed for all the routes from the selected city.
    * post: displays alert dialog box with available routes to select
     */
    void makeBuilder(final String[] routeInfo)
    {

        routeDisplayer.setTitle("Choose a MapRoute");
        // add a radio button list

        int checkedItem = 0; // cow
        gamePlaySingleton.setRouteToClaimInt(0);
        routeDisplayer.setSingleChoiceItems(routeInfo, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user checked an item
                gamePlaySingleton.setRouteToClaimInt(which);
            }
        });

        routeDisplayer.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog2, int which) {
                // user clicked OK

                MapRoute tempMaproute = routesToDisplay.get(gamePlaySingleton.getRouteToClaimInt());

                //converting the int to a color
                String user = ClientModel.getInstance().getUser().getUsername();
                String gameID = ClientModel.getInstance().getMyJoinedGame();

                if (tempMaproute.getRouteColor().equals(Color.WILD))
                {
                    dialog.dismiss();
                    GamePlayActivity gamePlayActivity = (GamePlayActivity) context;
                    gamePlayActivity.selectGrayColor(tempMaproute, user, gameID);


//                    final GrayRouteAlertDiagram grayRouteAlertDiagram =  new GrayRouteAlertDiagram(context);
//                    AlertDialog.Builder builder = grayRouteAlertDiagram.displayColorsToChoose(tempMaproute);
//
//                    dialog = builder.create();
//                    dialog.show();
//                    gamePlaySingleton.getView().invalidate();




                }
                else {
                   if( parallelRouteProtector(user, tempMaproute)) {
                       claimRouteHelper(tempMaproute, gameID, user);
                   }
                }




            }
        });
        routeDisplayer.setNegativeButton("Cancel", null);
        routeDisplayer.setCancelable(false);
// create and show the alert dialog
      dialog = routeDisplayer.create();
        dialog.show();
    }

    /**
     *
     * @param tempMaproute, the route that was selected
     * @param gameID the gameID of the game to claim route on
     * @return returns the string for the android Color given to the route
     */

   void claimRouteHelper(MapRoute tempMaproute, String gameID, String user)
   {
       TrainCardHand hand = ClientModel.getInstance().getGameByID(gameID).getTrainCardHandForPlayer(user);
       int specificColorCards;

       if (hand.getHandMap().get(tempMaproute.getRouteColor()) == null)
       {
           specificColorCards = 0;
       }
       else
       {
           specificColorCards = hand.getHandMap().get(tempMaproute.getRouteColor());
       }
       int wildCards;
       if (hand.getHandMap().get(Color.WILD) == null)
       {
           wildCards = 0;
       }
       else
       {
           wildCards  = hand.getHandMap().get(Color.WILD);
       }

       int combinedCards = specificColorCards + wildCards;

       try {
           if (combinedCards >= tempMaproute.getRouteLength()) {

               ClaimRouteTask claimRouteTask = new ClaimRouteTask(tempMaproute, user, gameID, gamePlaySingleton.getGameplayContext(),tempMaproute.getRouteColor());
               claimRouteTask.execute();
           } else {
               Toast.makeText(context, "You don't Have enough cards for that route and color", Toast.LENGTH_SHORT).show();
           }
       } catch (NullPointerException e) {
           Toast.makeText(context, "You don't Have enough cards for that route and color", Toast.LENGTH_SHORT).show();

       }
   }

    String getColor(MapRoute route)
    {
        String color = "ORANGE";
        model.Color routeColor = route.getRouteColor();
        if (routeColor.equals(model.Color.RED))
        {
            color = "RED";
        }
        if (routeColor.equals(model.Color.BLACK))
        {
            color = "BLACK";
        }
        if (routeColor.equals(model.Color.BLUE))
        {
            color = "BLUE";
        }

        if (routeColor.equals(Color.WILD))
        {
            color = "GRAY";
        }
        if (routeColor.equals(model.Color.GREEN))
        {
            color = "GREEN";
        }
        if (routeColor.equals(model.Color.PINK))
        {
            color = "MAGENTA";
        }
        if (routeColor.equals(model.Color.WHITE))
        {
            color = "WHITE";
        }
        if (routeColor.equals(model.Color.YELLOW))
        {
            color = "YELLOW";
        }

        return color;
    }






/**
*@param route, this is claimed route that needs to show ownership
*post: updates the canvas to display teh claimed route
 */
    void drawOwnerShip(MapRoute route)
    {
        //draw line of wonership over route.

        Paint paint = new Paint();
        //playerColor

        int tempColor = enumSwitch(PreGameModelFacade.getGameByID(ClientModel.getInstance().myJoinedGame).getPlayerByName(ClientModel.getInstance().getUser().getUsername()).getColor());

        paint.setColor(tempColor);
        paint.setStrokeWidth(20);


        int xDiff = route.getXPositionModifier();
        int yDiff = route.getYPositionModifier();

        float x1 = gamePlaySingleton.getScreenWidth() * route.getCity1().getX() / 1800 + xDiff;
        float y1 = gamePlaySingleton.getScreenHeight() * route.getCity1().getY() / 1000 + yDiff;
        float x2 = gamePlaySingleton.getScreenWidth() * route.getCity2().getX() / 1800 + xDiff;
        float y2 = gamePlaySingleton.getScreenHeight() * route.getCity2().getY() / 1000 + yDiff;

        gamePlaySingleton.getCanvas().drawLine(
                x1,
                y1,
                x2,
                y2,
                paint);
        if(gamePlaySingleton.getView()!= null) {
            gamePlaySingleton.getView().invalidate();
        }

    }


    public int enumSwitch(Enum color)
    {


        int toReturn = android.graphics.Color.BLUE;
        if (color.equals(model.Color.PINK)) {
            toReturn = android.graphics.Color.MAGENTA;

        } else if (color.equals(model.Color.WHITE)) {
            toReturn = android.graphics.Color.WHITE;

        } else if (color.equals(model.Color.BLUE)) {
            toReturn = android.graphics.Color.BLUE;

        } else if (color.equals(model.Color.YELLOW)) {
            toReturn = android.graphics.Color.YELLOW;

        } else if (color.equals(model.Color.ORANGE)) {
            toReturn = android.graphics.Color.rgb(255, 169, 0);

        } else if (color.equals(model.Color.BLACK)) {
            toReturn = android.graphics.Color.BLACK;

        } else if (color.equals(model.Color.RED)) {
            toReturn = android.graphics.Color.RED;

        } else if (color.equals(model.Color.GREEN)) {
            toReturn = android.graphics.Color.GREEN;

        }
        return toReturn;
    }

    public Boolean parallelRouteProtector(String username, MapRoute mapRoute)
    {
        List<MapRoute> availableRoutes = PreGameModelFacade.getGameByID(ClientModel.getInstance().getMyJoinedGame()).getMapRoutes();

        for (MapRoute route : availableRoutes)
        {
            if (route.getOwnerID() != null)
            {
                if(route.getOwnerID().equals(ClientModel.getInstance().getUser().getUsername()))
                {
                    if(mapRoute.getCity1().getName().equals(route.getCity1().getName())&& mapRoute.getCity2().getName().equals(route.getCity2().getName()))
                    {
                        Toast.makeText(context, "You Cant Claim Parallel Routes", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
