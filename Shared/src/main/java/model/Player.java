package model;

import states.DestinationCardDrawnState;
import states.ITurnState;
import states.NotYourTurnState;
import states.PostGameState;
import states.PreGameState;
import states.TrainCardDrawnState;
import states.TurnStartedState;
import states.TurnState;

import static model.Color.WILD;

/**
 * Created by Alyx on 7/10/17.
 */

public class Player {

    private TurnState turnState;
    private int trainPiecesLeft;
    private int scoreFromRoutes;
    private int scoreFromLongestTrain;
    private int scoreFromCompleteDestinationCards;
    private int scoreFromIncompleteDestinationCards;
    private int numberOfTrainCards;
    private int numberOfDestinationCards;
    private String username;
    private int order;
    private Color color;
    private boolean isTurn;
    private boolean lastTurnTaken = false;

    /**
     * Instantiates a player with the given user name.
     * @param username the username of the player to create.
     */
    public Player(String username){
        this.username = username;
        trainPiecesLeft = 20;
        scoreFromRoutes = 0;
        scoreFromLongestTrain = 0;
        scoreFromCompleteDestinationCards = 0;
        scoreFromIncompleteDestinationCards = 0;
        numberOfTrainCards = 4;
        numberOfDestinationCards = 3;
        isTurn = false;
        // Initialize the player to the pregame state.
        turnState = new PreGameState();
    }

    /**
     * @return this player's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username The name to assign the player.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    public boolean useTrainPieces(int number){
        if (trainPiecesLeft - number >= 0){
            trainPiecesLeft -= number;
            return true;
        }
        return false;
    }

    public int getTrainPiecesLeft() {
        return trainPiecesLeft;
    }

    public void setTrainPiecesLeft(int trainPiecesLeft) {
        this.trainPiecesLeft = trainPiecesLeft;
    }

    public int getScore() {
        return getScoreFromRoutes() + getScoreFromCompleteDestinationCards() + getScoreFromIncompleteDestinationCards() + getScoreFromLongestTrain();
    }

    public int getNumberOfTrainCards() {
        return numberOfTrainCards;
    }

    public void setNumberOfTrainCards(int numberOfTrainCards) {
        this.numberOfTrainCards = numberOfTrainCards;
    }

    public int getNumberOfDestinationCards() {
        return numberOfDestinationCards;
    }

    public void setNumberOfDestinationCards(int numberOfDestinationCards) {
        this.numberOfDestinationCards = numberOfDestinationCards;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public boolean isTurn() {
        return isTurn;
    }

    public void setTurn(boolean turn) {
        isTurn = turn;
    }

    public Boolean doesPlayerHaveEnoughTrains(MapRoute mapRoute)
    {
        return this.getTrainPiecesLeft() > mapRoute.getRouteLength();
    }

    public Boolean doesPlayerHaveEnoughTrainCards(MapRoute mapRoute, TrainCardHand hand)
    {
        Color routeColor;
        if (mapRoute.getRouteColor().equals(WILD))
        {
            routeColor = mapRoute.getGrayRouteTrainColor();
        }
        else {
            routeColor = mapRoute.getRouteColor();
        }

        if (hand.getHandMap().get(routeColor) >= mapRoute.getRouteLength())
        {
            return true;
        }
        return false;
    }

    /**
     * @return if it's currently this player's turn.
     */
    public boolean isPlayerTurn() {
        return !(turnState instanceof NotYourTurnState ||
                turnState instanceof PostGameState ||
                turnState instanceof PreGameState ||
                turnState.getStateIdentifier().equals("NotYourTurnState") ||
                turnState.getStateIdentifier().equals("PostGameState") ||
                turnState.getStateIdentifier().equals("PreGameState")
        );
    }

    /**
     * Called when the player wishes to discard their destination cards.
     * @return true if the selected cards can be discarded.
     */
    public boolean canAcceptDestinationCards(int cardsToDiscardCount) {
        if(turnState instanceof PreGameState || (turnState.getStateIdentifier().equals("PreGameState"))) {
            return cardsToDiscardCount <= 1;
        }
        else if(turnState instanceof  DestinationCardDrawnState || (turnState.getStateIdentifier().equals("DestinationCardDrawnState"))) {
            return cardsToDiscardCount <= 2;
        }
        return false;
    }

    /**
     * changes the player to reflect that they have accepted destination cards.
     * @return true if the action could be performed.
     */
    public boolean acceptDestinationCards() {
        return this.turnState.acceptDestinationCards(this);
    }

    /**
     * Tells the player if their turn can begin.
     * @return true if... true.
     */
    public boolean canStartTurn() {
        return this.turnState instanceof NotYourTurnState;
    }

    /**
     * Starts this player's turn.
     * @return true if the player's turn successfully started.
     */
    public boolean startTurn() {
        return this.turnState.startTurn(this);
    }

    /**
     * Checks if the player can draw the given card.
     * @param isFaceUp if the card is face up or not.
     * @param cardColor the color of the card.
     * @return true if the player is able to draw the card.
     */
    public boolean canDrawCard(Boolean isFaceUp, Color cardColor) {
        if (this.turnState instanceof TurnStartedState || turnState.getStateIdentifier().equals("TurnStartedState")) {
            return true;
        }
        else if (this.turnState instanceof  TrainCardDrawnState || turnState.getStateIdentifier().equals("TrainCardDrawnState")) {
            return !isFaceUp || cardColor != Color.WILD;
        }
        else return false;
    }

    /**
     * Draws a card for the player.
     * @param isFaceUp if the card is face up
     * @param cardColor the color of the card.
     * @return true if the action was performed.
     */
    public boolean drawCard(Boolean isFaceUp, Color cardColor) {
        return this.turnState.drawCard(this, isFaceUp, cardColor);
    }

    /**
     * @return if the player can draw destination cards.
     */
    public boolean canDrawDestinationCards() {
        if(turnState.getStateIdentifier().equals("TurnStartedState")){
            return true;
        }
        return this.turnState instanceof TurnStartedState;
    }

    /**
     * @return if the player drew destination cards.
     */
    public boolean drawDestinationCards() {
        return this.turnState.drawDestinationCards(this);
    }

    /**
     * @return true if the player can claim the given route.
     */
    public boolean canClaimRoute(MapRoute mapRoute, TrainCardHand hand) {
        if  (!(this.turnState instanceof TurnStartedState) && !(turnState.getStateIdentifier().equals("TurnStartedState"))) {
            return false;
        }

        if (this.getTrainPiecesLeft() < mapRoute.getRouteLength())
        {
            return false;
        }

        Color routeColor;
        if (mapRoute.getRouteColor().equals(WILD) || mapRoute.getRouteColor().equals(WILD))
        {
            routeColor = mapRoute.getGrayRouteTrainColor();
        }
        else {
            routeColor = mapRoute.getRouteColor();
        }

        int numberOfCards = hand.getCardCount(routeColor);

        if (numberOfCards >= mapRoute.getRouteLength())
        {
            return true;
        }
        else if (mapRoute.getRouteColor().equals(WILD) || mapRoute.getRouteColor().equals(WILD))
        {
            routeColor = mapRoute.getGrayRouteTrainColor();
        }
        else {
            routeColor = mapRoute.getRouteColor();
        }

        int colorCards = hand.getCardCount(routeColor);
        int wildCards =  hand.getCardCount(Color.WILD);
        int combinedCards = colorCards + wildCards;

        return combinedCards >= mapRoute.getRouteLength();
    }

    public boolean claimRoute(MapRoute mapRoute, TrainCardHand hand) {
        return this.turnState.claimRoute(this, mapRoute, hand);
    }

    /**
     * don't use this unless you are extending the ITurnState. DO NOT USE IT.
     * @param turnState
     */
    public void setTurnState(TurnState turnState) {
        this.turnState = turnState;
    }

    public void endGame() { this.turnState.endGame(this); }

    public void setState(TurnState state) {
        this.turnState = state;
    }

    public int getScoreFromRoutes()
    {
        return scoreFromRoutes;
    }

    public void setScoreFromRoutes(int scoreFromRoutes)
    {
        this.scoreFromRoutes = scoreFromRoutes;
    }

    public void addScoreFromRoutes(int scoreToAdd) {
        scoreFromRoutes = scoreFromRoutes + scoreToAdd;
    }

    public int getScoreFromLongestTrain()
    {
        return scoreFromLongestTrain;
    }

    public void setScoreFromLongestTrain(int scoreFromLongestTrain)
    {
        this.scoreFromLongestTrain = scoreFromLongestTrain;
    }

    public int getScoreFromCompleteDestinationCards()
    {
        return scoreFromCompleteDestinationCards;
    }

    public void setScoreFromCompleteDestinationCards(int scoreFromCompleteDestinationCards)
    {
        this.scoreFromCompleteDestinationCards = scoreFromCompleteDestinationCards;
    }

    public int getScoreFromIncompleteDestinationCards()
    {
        return scoreFromIncompleteDestinationCards;
    }

    public void setScoreFromIncompleteDestinationCards(int scoreFromIncompleteDestinationCards) {
        this.scoreFromIncompleteDestinationCards = scoreFromIncompleteDestinationCards;
    }
    /**
     * @return true if the player has decided on their initial cards.
     */
    public boolean initialCardsDiscarded() {
        if(this.turnState.getStateIdentifier().equals("PreGameState")){
            return false;
        } else {
            return true;
        }
//        return !(this.turnState instanceof PreGameState);
    }

    public boolean wasLastTurnTaken() {
        return lastTurnTaken;
    }

    public void setLastTurnTaken(boolean lastTurnTaken) {
        this.lastTurnTaken = lastTurnTaken;
    }
}
