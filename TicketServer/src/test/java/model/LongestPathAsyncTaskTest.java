package model;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import model.algorithm.LongestPathAsyncTask;

import static model.Color.*;
/**
 * Created by deriktj on 7/31/2017.
 */
public class LongestPathAsyncTaskTest
{

    /** used to populate cities*/
    ArrayList<MapCity> mapCities = new ArrayList<>();

    /** used to populate routes*/
    ArrayList<MapRoute> mapRoutes = new ArrayList<>();

    /** used to populate routes*/
    Map<String, MapCity> cityMap = new HashMap<>();

    @Test
    public void doInBackground() throws Exception
    {
        initializeCityList();
        initializeCityMap();
        populateRoutes();

        //getRoute("A","B").setOwndrID("derik");
        //getRoute("B","C").setOwndrID("derik");
        //getRoute("C","D").setOwndrID("derik");

        //getRoute("E","F").setOwndrID("butt");
        //getRoute("F","G").setOwndrID("butt");
        //getRoute("G","H").setOwndrID("butt");

        //LongestPathAsyncTask task = new LongestPathAsyncTask(mapRoutes);

        //task.execute();
        //Set<String> result = task.get();
        System.out.println("task ");
        //assert();
    }

    public void populateRoutes()
    {

        mapRoutes.add(new MapRoute(cityMap.get("A"), cityMap.get("B"), 3, WILD, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("B"), cityMap.get("C"), 3, WHITE, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("C"), cityMap.get("D"), 4, BLUE, 0 , 0));

        mapRoutes.add(new MapRoute(cityMap.get("E"), cityMap.get("F"), 3, WILD, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("F"), cityMap.get("G"), 4, WHITE, 0, 0));
        mapRoutes.add(new MapRoute(cityMap.get("G"), cityMap.get("H"), 3, BLUE, 0 , 0));
    }

    public void initializeCityList(){

        mapCities.add(new MapCity("A", 1471, 849));
        mapCities.add(new MapCity("B", 1302, 688));
        mapCities.add(new MapCity("C", 1699, 267));
        mapCities.add(new MapCity("D", 380, 90));
        mapCities.add(new MapCity("E", 1470, 682));
        mapCities.add(new MapCity("F", 168, 95));
        mapCities.add(new MapCity("G", 157, 165));
        mapCities.add(new MapCity("H", 123, 234));
    }

    public void initializeCityMap(){
        for (MapCity city : mapCities){
            cityMap.put(city.getName(), city);
        }
    }

    public MapRoute getRoute(String city1, String city2) {
        for(MapRoute route : mapRoutes) {
            if(route.getCity1().getName().equals(city1) && route.getCity2().getName().equals(city2)) {
                return route;
            }
        }
        return null;
    }
}