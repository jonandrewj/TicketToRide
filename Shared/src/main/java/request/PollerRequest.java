package request;

/**
 * Created by deriktj on 7/11/2017.
 */

public class PollerRequest
{
    private String username;
    private int index;

    public PollerRequest(String username,int index){
        this.username = username;
        this.index = index;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }
}
