package plugin;

/**
 * Created by Andrew Jacobson on 8/6/2017.
 */

public class MongoDatabaseFactory implements iDatabaseFactory {

    public MongoDatabaseFactory(){}

    @Override
    public iDaoAPI getDatabase() {
        return new MongoDB();
    }
}
