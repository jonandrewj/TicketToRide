package serverproxy;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

import model.data.ClientModel;

/**
 * Created by Andrew Jacobson on 7/2/2017.
 */

public class ClientCommunicator {
    // SINGLETON PATTERN ---------------------------------------------------------------------------
    public static ClientCommunicator communicator;
    private Gson gson;
    private ClientCommunicator(){
        gson = new Gson();
    }

    /**
     * @return the client communicator instance.
     */
    public static ClientCommunicator get(){
        if (communicator == null){
            communicator = new ClientCommunicator();
        }
        return communicator;
    }

    /**
     * Sends a request to the server.
     * @param requestMessage the request body.
     * @param handler the path to send the request to.
     * @param ip the server host.
     * @return the response body.
     * @throws Exception if there is an error from the server.
     */
    public String sendRequest(Object requestMessage, String handler, String ip) throws Exception {
        HttpURLConnection conn = null;
        try {
            conn = openConnection(handler, ip);

            OutputStream request = conn.getOutputStream();
            write(gson.toJson(requestMessage), request);
            request.close();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK){
                String response = read(conn.getInputStream());
                return response;
            }
            else {
                throw new Exception("Bad Response From Server - HTTP NOT OK");
            }

        } catch (IOException e){
            System.out.println(e.getMessage());
            throw e;
        }
    }

    private HttpURLConnection openConnection(String handler, String ip) throws Exception {
        HttpURLConnection result = null;
        try {
            URL url = new URL("http://" + ip + ":8080/" + handler);
            result = (HttpURLConnection)url.openConnection();
            result.setRequestMethod("POST");
            result.setConnectTimeout(850);
            result.setDoOutput(true);
            result.setDoInput(true);
            result.addRequestProperty("auth", ClientModel.getInstance().getUser().getAuthToken().getToken());

            // Add so JSON can pass thru
            result.addRequestProperty("Accept", "application/json");

            result.connect();

        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
            throw new Exception("I'm sorry! It seems the ip address isn't correct");
        }catch (MalformedURLException e) {
            System.out.println(e.getMessage());
            throw new Exception("Oops! I think the connection is incorrect");
        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new Exception("Oh man, we messed up some writing over here. Give it another try");
        } catch (Exception e){
            System.out.println(e.getMessage());
            throw new Exception("Rats! I don't even know what's wrong");
        }
        return result;
    }

    private static String read(InputStream stream) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader reader = new InputStreamReader(stream);
        char[] buf = new char[1024];
        int length;
        while ((length = reader.read(buf)) > 0) {
            sb.append(buf, 0, length);
        }
        return sb.toString();
    }

    private static void write(String str, OutputStream os) throws IOException {
        OutputStreamWriter sw = new OutputStreamWriter(os);
        sw.write(str);
        sw.flush();
    }


}
