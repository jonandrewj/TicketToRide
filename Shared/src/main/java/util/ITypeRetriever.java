package util;

/**
 * Created by parad on 8/9/2017.
 */

public interface ITypeRetriever {
    String getType();
}
