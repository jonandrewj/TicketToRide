package model;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by deriktj on 7/9/2017.
 */

public class GameList
{
    private List<Game> gameList;

    /**
     * Instantiates the Game List.
     */
    public GameList() {
        gameList = new ArrayList<>();
    }

    /**
     * Sets the game list
     * @param gameList
     */
    public void setGameList(List<Game> gameList){
        this.gameList = gameList;
    }

    /**
     * Adds a game to this list.
     * @param g the game to add to the list.
     */
    public void add(Game g) {
        gameList.add(g);
    }

    /**
     * Removes the specified game to the list.
     * @param g the game to remove.
     */
    public void remove(Game g) {
        gameList.remove(g);
    }

    /**
     * Gets the game at the given index.
     * @param i
     */
    public Game get(int i) {
        return gameList.get(i);
    }

    /**
     * Using a string, finds and returns a game.
     * @param id the id of the game to find.
     * @return the game with the given UUID.
     */
    public Game getGameByID(String id) {
        for(int i = 0; i < gameList.size(); i++) {
            if(gameList.get(i).getGameID().equals(id)) {
                return gameList.get(i);
            }
        }
        return null;
    }

    /**
     * @return the amount of games in this list.
     */
    public int getSize() {
        return gameList.size();
    }

    public List<Game> getGames(){
        return gameList;
    }
}
