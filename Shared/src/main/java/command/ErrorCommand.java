package command;

/**
 * Created by Andrew Jacobson on 7/9/2017.
 */

public class ErrorCommand extends Command
{
    String errorMessage;

    /**
     * Creates an error command.
     * @param errorMessage the error message.
     */
    public ErrorCommand(String errorMessage) {
        super();
        this.errorMessage = errorMessage;
    }

    /**
     * Prints out an error message.
     */
    @Override
    public Boolean execute()
    {
        System.out.println("Error:  " + errorMessage);
        return true;
    }
}
