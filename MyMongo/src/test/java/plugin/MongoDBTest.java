package plugin;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import command.ICommand;
import command.JoinGameCommand;
import model.Account;
import model.AuthToken;
import model.Game;

import static org.junit.Assert.*;

/**
 * Created by Andrew Jacobson on 8/5/2017.
 */
public class MongoDBTest {

    MongoDB mongoDB;
    List<Game> games;
    Set<Account> accounts;
    List<ICommand> commandQueue;
    List<ICommand> gameCommands;

    @Before
    public void setUp() throws Exception {
        mongoDB = new MongoDB();
        games = new ArrayList<>();
        accounts = new HashSet<>();
        commandQueue = new ArrayList<>();
        gameCommands = new ArrayList<>();
    }

    @After
    public void tearDown() throws Exception {
        mongoDB.mongoClient.close();
        //games.clear();
        //accounts.clear();
        //commandQueue.clear();
        //gameCommands.clear();
    }

    @Test
    public void addGame() throws Exception {
        selfDestruct();

        Game game = new Game();
        games.add(game);
        mongoDB.addGame(game);
        assertTrue(mongoDB.getGames().size() == 1);

        game = new Game();
        games.add(game);
        mongoDB.addGame(game);
        assertTrue(mongoDB.getGames().size() == 2);

    }

    /*@Test
    public void updateGame() throws Exception {
        addGame();
        Game game = games.get(0);
        game.setPlayerMax(4);

        ICommand command = new JoinGameCommand(games.get(0).getGameID(), "Andrew");
        mongoDB.addDeltaCommand(command);

        //mongoDB.updateGame(game);

        assertTrue(mongoDB.getDeltaCommands().size() == 0);
        assertTrue(mongoDB.getCommandQueue().size() == 1);

        assertTrue(mongoDB.getGames().size() == 2);
        List<Game> localgames = mongoDB.getGames();
        if (localgames.get(0).getGameID().equals(game.getGameID())){
            assertTrue(localgames.get(0).getPlayerMax() == 4);
        }
    }*/

    @Test
    public void addGameCommand() throws Exception {
        addGame();
        ICommand command = new JoinGameCommand(games.get(0).getGameID(), "Andrew");
        mongoDB.addDeltaCommand(command);
        assertTrue(mongoDB.getDeltaCommands().size() == 1);
    }

    @Test
    public void getUserAccounts() throws Exception {
        assertTrue(mongoDB.getUserAccounts().size() == 0);
        addUserAccount();
        assertTrue(mongoDB.getUserAccounts().size() == 1);
    }

    @Test
    public void addUserAccount() throws Exception {
        mongoDB.addUserAccount(new Account("andrew", "password", new AuthToken()));
    }

    @Test
    public void getCommandQueue() throws Exception {

    }

    @Test
    public void selfDestruct() throws Exception {
        mongoDB.selfDestruct();
        assertTrue(mongoDB.getDeltaCommands().size() == 0);
        assertTrue(mongoDB.getUserAccounts().size() == 0);
        assertTrue(mongoDB.getGames().size() == 0);
        assertTrue(mongoDB.getCommandQueue().size() == 0);
    }

    @Test
    public void getGames() throws Exception {

    }

}