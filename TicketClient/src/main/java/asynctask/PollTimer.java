package asynctask;

import java.util.TimerTask;

/**
 * Created by Alyx on 7/13/17.
 */

public class PollTimer extends TimerTask {
    /**
     * Runs the poller.
     */
    @Override
    public void run() {
            // Just start async task & proceed
            new PollerTask().execute();
    }
}
