package command;

import model.IModelFacade;

/**
 * Created by Joseph on 7/17/2017.
 *
 *
 */
public class GameChatCommand extends Command implements ILoggable {
    private String owner;
    private String message;
    private String gameId;

    /**
     * Creates a new instance of a gamechat command.
     * @param owner the name of the player creating the chat message.
     * @param message the message that the player is sending.
     * @param gameId the game id to apply the command to. (is this needed?)
     */
    public GameChatCommand(String owner, String message, String gameId) {
        super();
        this.owner = owner;
        this.message = message;
        this.gameId = gameId;
    }

    /**
     * Adds a chat item. Doesn't really do... anything. I have to return true.
     * @return true if the command was correctly executed; otherwise, false.
     */
    @Override
    public Boolean execute() {
        this.getContext().addChatMessageForGame(this.gameId, this.message, owner);
        return true;
    }

    /**
     * @return the message contained in the chat message.
     */
    public String getChatMessage() {
        return message;
    }

    @Override
    public String getGameId() {
        return gameId;
    }

    @Override
    public String getUsername() {
        return owner;
    }

    @Override
    public String getLogMessage() {
        return "said: " + message;
    }
}
