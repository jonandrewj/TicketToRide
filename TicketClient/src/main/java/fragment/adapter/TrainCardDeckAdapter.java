package fragment.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import asynctask.DrawTrainCardTask;
import cs340.byu.tickettoride.R;
import fragment.CardSelectionFragment;
import model.Color;
import model.TrainCardDeck;
import model.data.ClientModel;

/**
 * Created by deriktj on 7/22/2017.
 */

/**
 * Populates the train deck recycler view by taking the deck and displaying the top 5 cards (and the top card of the deck)
 *
 */
public class TrainCardDeckAdapter extends RecyclerView.Adapter<TrainCardDeckAdapter.CustomViewHolder>
{
    private TrainCardDeck deck;
    private Color[] deckColors;
    private Context mContext;
    private CardSelectionFragment parentFragment;

    /**
     * Interface for the onClickListener. Used later for anonymous classes
     *
     */
    public interface OnItemClickListener {
        void onItemClick(int index);
    }

    /**
     * Constructor
     * @param deck the train card deck object to display
     * @param context context the adapter lives in
     * @param csf parent view of the fragment
     *
     */
    public TrainCardDeckAdapter(TrainCardDeck deck, Context context, CardSelectionFragment csf) {
        this.deck = deck;
        deckColors = new Color[6];
        deckColors[0] = null;
        for(int i = 4; i >= 0; i--) {
            deckColors[5 - i] = deck.getCardAtIndex(i).getColor();
        }
        this.mContext = context;
        this.parentFragment = csf;
    }

    /**
     * inflates the card_deck_display layout
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.deck_card_display, parent,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    private void drawCard(int index) {
        DrawTrainCardTask asyncTask = new DrawTrainCardTask(ClientModel.getInstance().getUser().getUsername(),ClientModel.getInstance().myJoinedGame,index);
        asyncTask.execute(parentFragment);
    }

    /**
     * Binds each card to its onClickListener and sets it's image.
     * @param holder the recycler view list element
     * @param position the position of the holder in the list
     *
     * @post the list element will be setup to be rendered according to the card it corresponds to in the deck
     */
    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position)
    {
        Color color = deckColors[position];
        holder.bind(position,new TrainCardDeckAdapter.OnItemClickListener() {
            @Override public void onItemClick(int index) {
                drawCard(5 - position);
            }
        });
        if(position == 0) {
            holder.getLayout().setBackgroundResource(R.mipmap.back_train);
            return;
        }
        switch(color)
        {
            case BLUE:
                holder.getLayout().setBackgroundResource(R.mipmap.blue_train);
                break;
            case GREEN:
                holder.getLayout().setBackgroundResource(R.mipmap.green_train);
                break;
            case PINK:
                holder.getLayout().setBackgroundResource(R.mipmap.pink_train);
                break;
            case WHITE:
                holder.getLayout().setBackgroundResource(R.mipmap.white_train);
                break;
            case YELLOW:
                holder.getLayout().setBackgroundResource(R.mipmap.yellow_train);
                break;
            case ORANGE:
                holder.getLayout().setBackgroundResource(R.mipmap.orange_train);
                break;
            case BLACK:
                holder.getLayout().setBackgroundResource(R.mipmap.black_train);
                break;
            case RED:
                holder.getLayout().setBackgroundResource(R.mipmap.red_train);
                break;
            case WILD:
                holder.getLayout().setBackgroundResource(R.mipmap.locomotive_train);
                break;
            default:
                //;
                break;
        }
    }


    /**
     * Gets the item id
     * @param i
     * @return
     */
    @Override
    public long getItemId(int i)
    {
        return i;
    }

    /**
     * Gets the number of items in the list (always 6)
     * @return the number of items in the list (6)
     */
    @Override
    public int getItemCount()
    {
        return 6;
    }

    /**
     * The view holder for each card in the list. Used internally
     */
    public static class CustomViewHolder extends RecyclerView.ViewHolder {
                protected View layout;

        public CustomViewHolder(View view) {
            super(view);
            layout = view;
        }

        public View getLayout() {
            return layout;
        }

        public void bind(final Integer index, final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(index);
                }
            });
        }

    }
}
