package fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;

import fragment.adapter.GameListAdapter;
import model.data.ClientModel;
import model.Game;
import asynctask.CreateGameTask;
import asynctask.JoinGameTask;
import cs340.byu.tickettoride.R;
import model.facade.PreGameModelFacade;
import activity.GamePlayActivity;

/**
 * Created by kendall on 7/6/2017.
 *
 * Presenter for the GameSelect view.
 */
public class GameSelectFragment extends Fragment implements Observer {

    // View
    private View gameSelectionView;
    
    // Widgets
    private Button createGameButton;
    private ListView viewedGameList;
    private TextView numberOfPlayersDisplayText;
    private EditText numberOfPlayersField;

    // Input field
    private String numPlayers;

    // For recycler view
    private Map<String, List<String>> mExpandableItemsMap;
    private GameListAdapter gameListAdapter;

    private Boolean ableToJoinGame = false;

    private final int MAX_GAMES = 5;

    /** Fragment context */
    private Context myContext;

    public GameSelectFragment()
    {
        // Initialize context
        myContext = this.getContext();

        // Initialize fields for recyclerView
        mExpandableItemsMap = new TreeMap<>();
        gameListAdapter = null;

        // Default to 5 players.
        numPlayers = "5";
    }

    @Override
    public void onStart() {
        super.onStart();
        ClientModel.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ClientModel.getInstance().deleteObserver(this);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Get context
        myContext = this.getContext();

        // Inflate the fragment
        gameSelectionView = inflater.inflate(R.layout.fragment_game_selection, container, false);

        // Initialize widgets
        initWidgets();

        if (ClientModel.getInstance().getGames().size() == 0)
        {
            // I force user to only start a new game
            // by displaying only the game create widgets
            config_hideGameList();
            showCreateButton();
        }
        else if (ClientModel.getInstance().getGames().size() >= MAX_GAMES)
        {
            //user cannot create new game, they have to choose a game
            config_hideCreateGame();
            showGameList();
        }
        else {
            showGameList();
            showCreateButton();

        }
        // CHECK IF SELECTED GAME IS STARTED
        if (!ClientModel.getInstance().myJoinedGame.equals("")){
            if (PreGameModelFacade.getGameByID(
                    ClientModel.getInstance().myJoinedGame).isGameStarted()){
                // My game is already going --
                startGameEvent();
            }
        }

        return gameSelectionView;
    }

    /**
     * Shows the user the list of games.
     */
    public void showGameList(){
        viewedGameList = (ListView) gameSelectionView.findViewById(R.id.gameList);
        gameListAdapter = new GameListAdapter(this.getContext());

        for (int i = 0; i < ClientModel.getInstance().getGames().size(); i++){
            Game game = PreGameModelFacade.getGameByIndex(i);
            gameListAdapter.addGame(game.getGameID(), game.getPlayerCount(), game.getPlayerMax());
        }
        viewedGameList.setAdapter(gameListAdapter);
        // GameList Listener

        viewedGameList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String gameID = ((GameListAdapter.GameData)parent.getItemAtPosition(position)).gameID;

                joinGame(gameID);
            }
        });
    }

    /**
     * Allows the user to view the game creation button.
     */
    public void showCreateButton(){
        createGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createGame();
                //openGame.setSelectedGameIndex(openGame.getGames().size());

            }
        });
        numberOfPlayersField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                numPlayers = s.toString();
            }
        });
    }

    private void initWidgets(){
        createGameButton  = (Button) gameSelectionView.findViewById(R.id.newGameButton);
        viewedGameList = (ListView) gameSelectionView.findViewById(R.id.gameList);
        numberOfPlayersDisplayText = (TextView) gameSelectionView.findViewById(R.id.numberOfPlayers);
        numberOfPlayersField = (EditText) gameSelectionView.findViewById(R.id.numberOfGamesFromUserInput);
    }

    private void config_hideGameList(){
        viewedGameList.setVisibility(View.GONE);
    }

    private void config_displayGameList(){
        viewedGameList.setVisibility(View.VISIBLE);
    }

    private void config_hideCreateGame() {
        createGameButton.setVisibility(View.GONE);
        numberOfPlayersDisplayText.setVisibility(View.GONE);
        numberOfPlayersField.setVisibility(View.GONE);
    }

    private void config_displayCreateGame(){
        createGameButton.setVisibility(View.VISIBLE);
        numberOfPlayersDisplayText.setVisibility(View.VISIBLE);
        numberOfPlayersField.setVisibility(View.VISIBLE);
    }

    /**
     * Finishes the given activity and moves to the pre game view.
     */
    public void moveToPreGameView() {
        //Finsihes this activity and moves to game selection activity
        FragmentManager fragmentManager2 = getFragmentManager();
        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
        GameInfoFragment fragment2 = new GameInfoFragment();
        //fragmentTransaction2.addToBackStack("xyz");
        //fragmentTransaction2.replace(android.R.id.content,fragment2, null);
        fragmentTransaction2.replace(((ViewGroup) (getView().getParent())).getId(), fragment2);
        fragmentTransaction2.addToBackStack(null);
        // fragmentTransaction2.add(android.R.id.content, fragment2);
        fragmentTransaction2.commit();

    }

    @Override
    public void update(Observable o, Object arg) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.detach(this);
        transaction.attach(this);
        transaction.commit();

        if (userInCurrentGame()) {
            moveToPreGameView();
            //System.out.print("Joined! Time for the next screen.");
            o.deleteObserver(this); // Don't update this.
        }
    }

    private boolean userInCurrentGame(){
        if (ClientModel.getInstance().myJoinedGame.equals("")){
            return false;
        }
        String user = ClientModel.getInstance().getUser().getUsername();
        for (int i = 0; i < ClientModel.getInstance().getGames().size(); i++){
            for (int j = 0; j < PreGameModelFacade.getGameByIndex(i).getPlayerCount(); j++){
                if (user.equals(PreGameModelFacade.getGameByIndex(i).getPlayers().get(j).getUsername())){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Creates a game with the given player as the sole player.
     */
    public void createGame(){
        int playerNumber = 5;
        try{
           playerNumber = Integer.parseInt(numPlayers);
            if (playerNumber < 2 ||playerNumber > 5)
            {
                Toast toast = Toast.makeText(this.getContext(), "Invalid Input, Automatically set to 5 players", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
        catch (Exception e)
        {
            Toast toast = Toast.makeText(this.getContext(), "Invalid Input, Automatically set to 5 players", Toast.LENGTH_SHORT);
            toast.show();
            playerNumber = 5;
        }
        CreateGameTask creator = new CreateGameTask();
        creator.createGame(playerNumber);
        creator.execute(this);
    }

    /**
     * Joins the game, propagating all the way to the server.
     */
    public void joinGame(String gameID){
        // Check if user is already in this game
        String user = ClientModel.getInstance().getUser().getUsername();
        if (PreGameModelFacade.isUserInGame(user, gameID)){
            // Rejoin --
            if (PreGameModelFacade.getGameByID(gameID).isGameStarted()){
                // My game is already going --
                ClientModel.getInstance().setJoinedGame(gameID);
                startGameEvent();
            }
            else {
                ClientModel.getInstance().setJoinedGame(gameID);
            }
        }
        else {
            // Create task
            JoinGameTask joiner = new JoinGameTask();

            // Init task
            joiner.setJoinedGame(gameID);

            // Execute task
            joiner.execute(this);
        }
    }

    /**
     * Prints the entered message to the screen. If "success" is true,
     * it will assume the joining (or creation, which is a different flavor
     * of joining) was successful and will move to game selection screen.
     * @param message Message that will be printed to screen
     * @param success Join status (either success || !success)
     */
    public void gameJoinUpdate(String message, boolean success) {
        // Print toast
        Toast toast = Toast.makeText(this.getContext(), message, Toast.LENGTH_SHORT);
        toast.show();

    }

    /**
     * Shows a toast notifying the user about a game creation update.
     * @param message the message to show in the toast.
     * @param success if the game creation was successful
     */
    public void gameCreateUpdate(String message, boolean success){
        // Print toast
        Toast toast = Toast.makeText(this.getContext(), message, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Starts a game.
     */
    public void startGameEvent(){
        if (!ClientModel.getInstance().isInGame) {
            ClientModel.getInstance().isInGame = true;
            Intent gameIntent = new Intent(getActivity(), GamePlayActivity.class);
            startActivity(gameIntent);
        }
    }

}
