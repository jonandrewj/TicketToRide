package model;

/**
 * Created by Alyx on 7/10/17.
 */

public class User {
    private String username;
    private AuthToken authToken;

    private static User _instance;

    /**
     * Returns the instance of the user class.
     * @return
     */
    public static User getInstance(){
        if(_instance == null){
            _instance = new User();
        }
        return _instance;
    }
    private User(){
        username = "";
        authToken = new AuthToken();
    }

    /**
     * Retrieves the user's name.
     * @return the username assigned to this user.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username to the specified string.
     * @param username The name the user should have.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the user's auth token.
     * @return the auth token assigned to the user.
     */
    public AuthToken getAuthToken() {
        return authToken;
    }

    /**
     * Sets the user's auth token.
     * @param authToken the Auth token to assign to the user.
     */
    public void setAuthToken(AuthToken authToken) {
        this.authToken = authToken;
    }
}
