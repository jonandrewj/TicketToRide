package model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by deriktj on 7/18/2017.
 */

public class TrainCardHand
{

    private Map<Color,Integer> hand;

    private String username;

    public TrainCardHand(String username) {
        hand = new HashMap<>();
        this.username = username;

    }

    public int getHandSize() {
        return hand.size();
    }

    public String getUsername()
    {
        return username;
    }

    public void addCardToHand(Color color, int count) {
        hand.put(color,(hand.get(color) == null ? 0 : hand.get(color)) + count);
    }

    public void removeCardFromHand(Color color, int count) {
        hand.put(color,(hand.get(color) == null ? 0 : hand.get(color)) - count);
        if(hand.get(color) <= 0) {
            hand.remove(color);

        }
    }

    public int getCardCount(Color color){
        if(hand.get(color) != null){
            return hand.get(color);
        }
        else {
            return 0;
        }

    }
    public Map<Color, Integer> getHandMap() {
        return hand;
    }
}
