package fragment.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import cs340.byu.tickettoride.R;
import model.Color;
import model.DestinationCard;
import model.DestinationCardHand;
import model.TrainCardDeck;

/**
 * Created by deriktj on 7/22/2017.
 */

public class DestinationCardHandAdapter extends RecyclerView.Adapter<DestinationCardHandAdapter.CustomViewHolder>
{
    private DestinationCardHand hand;
    private Context mContext;

    public interface OnItemClickListener {
        void onItemClick(View v,int index);
    }


    public DestinationCardHandAdapter(DestinationCardHand hand, Context context) {
        this.hand = hand;
        this.mContext = context;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.destination_card_display, parent,false);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, final int position)
    {
        holder.bind(position,new DestinationCardHandAdapter.OnItemClickListener() {
            @Override public void onItemClick(View v,int index) {
                DestinationCard card = hand.getHand().get(index);
                if(card.canBeDiscarded()) {
                    card.setSelectedToDiscard(!card.getSelectedToDiscard());
                    updateDestinationCard(v,card);
                }
            }
        });


        String city1 = hand.getHand().get(position).getCity1();
        String city2 = hand.getHand().get(position).getCity2();
        String points = String.valueOf(hand.getHand().get(position).getPoints());
        Boolean isDiscardable = hand.getHand().get(position).canBeDiscarded();
        Boolean isSelectedToDiscard = hand.getHand().get(position).getSelectedToDiscard();
        holder.city1.setText(city1);
        holder.city2.setText(city2);
        holder.points.setText(points);
        if(isDiscardable) {
            if(isSelectedToDiscard) {
                holder.background.setBackgroundColor(android.graphics.Color.RED);
            }
            else {
                holder.background.setBackgroundColor(android.graphics.Color.GREEN);
            }
        }
    }

    private void updateDestinationCard(View v, DestinationCard card)
    {
        Boolean isDiscardable = card.canBeDiscarded();
        Boolean isSelectedToDiscard = card.getSelectedToDiscard();
        if(isDiscardable) {
            if(isSelectedToDiscard) {
                v.findViewById(R.id.destinationCardFillable).setBackgroundColor(android.graphics.Color.RED);
            }
            else {
                v.findViewById(R.id.destinationCardFillable).setBackgroundColor(android.graphics.Color.GREEN);
            }
        }
    }


    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public int getItemCount()
    {
        return hand.getHand().size();
    }

    public static class CustomViewHolder extends RecyclerView.ViewHolder {
        protected View layout;
        protected TextView city1;
        protected TextView city2;
        protected TextView points;
        protected LinearLayout background;

        public CustomViewHolder(View view) {
            super(view);
            layout = view;
            city1 = (TextView) view.findViewById(R.id.destinationCity1);
            city2 = (TextView) view.findViewById(R.id.destinationCity2);
            points = (TextView) view.findViewById(R.id.destinationPoints);
            background = (LinearLayout) view.findViewById(R.id.destinationCardFillable);
        }

        public View getLayout() {
            return layout;
        }

        public void bind(final Integer index, final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(v,index);
                }
            });
        }

    }
}
