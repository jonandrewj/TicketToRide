package command;

/**
 * Created by Joseph on 7/19/2017.
 *
 * Provides functionality to grab a GameId and/or a username from a command.
 */
public interface ILoggable extends ICommand {
    /**
     * @return the id of the game associated with this instance.
     */
    String getGameId();

    /**
     * @return the username of the player that caused this command.
     */
    String getUsername();

    /**
     * @return the message from this command.
     */
    String getLogMessage();
}
