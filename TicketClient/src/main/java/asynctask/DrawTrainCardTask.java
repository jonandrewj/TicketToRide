package asynctask;

import android.os.AsyncTask;

import fragment.CardSelectionFragment;
import model.Color;
import model.Game;
import model.Player;
import model.TrainCard;
import response.CommandResponse;
import serverproxy.ServerFacade;
import model.data.ClientModel;
import response.ResponseBase;
import fragment.GameSelectFragment;

import static model.TrainCardDeck.DECK_INDEX;

/**
 * Created by Alyx on 7/9/17.
 */

public class DrawTrainCardTask extends AsyncTask<CardSelectionFragment, Integer, Boolean> {
    /** Register result, contains the result of the register attempt */
    private CommandResponse drawResponse;

    /** Access the model */
    private ClientModel model;

    /** The fragment that called this task */
    private CardSelectionFragment caller;

    private String username;
    private String gameID;
    private int index;

    public DrawTrainCardTask(String username, String gameID, int index) {
        drawResponse = null;
        model = ClientModel.getInstance();

        this.username = username;
        this.gameID = gameID;
        this.index = index;
    }


    protected Boolean doInBackground(CardSelectionFragment... urls) {

        Game game = ClientModel.getInstance().getGameByID(gameID);
        Player currentPlayer = game.getPlayerByName(username);
        TrainCard card = game.getTrainDeck().getCardAtIndex(index);
        if (!currentPlayer.canDrawCard(index != DECK_INDEX, card == null ? null : card.getColor())){
            return false;
        }

        // Get info from the UI
        this.caller = urls[0];
        drawResponse =  ServerFacade.drawTrainCard(username,gameID,index);
        // Return success
        return drawResponse != null && !drawResponse.isHasError();

    }

    protected void onProgressUpdate(Integer... progress) {
        // progressBar.setProgress(progress[0]);
    }

    protected void onPostExecute(Boolean success) {

    }

}
