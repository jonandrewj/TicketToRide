package asynctask;

import android.content.Context;
import android.os.AsyncTask;

import activity.GamePlayActivity;
import model.Color;
import model.Game;
import model.MapRoute;
import model.Player;
import model.TrainCardHand;
import model.data.ClientModel;
import response.ClaimRouteResponse;
import serverproxy.ServerFacade;

/**
 * Created by kendall on 7/19/2017.
 */

public class ClaimRouteTask extends AsyncTask<GamePlayActivity, Integer, Boolean> {

    private MapRoute route;
    private String username;
    private String gameID;
    private Color colorToUse;
    private ClaimRouteResponse claimRouteResponse;

    /** The Activity that called this task */
    private Context caller;

    /** The authorization token returned from the server upon successful authentication */
    public ClaimRouteTask(MapRoute route, String username, String gameID, Context context, Color colorToUse)
    {
        this.caller = context;
        this.route = route;
        this.username = username;
        this.gameID = gameID;
        this.colorToUse = colorToUse;
    }

    @Override
    protected Boolean doInBackground(GamePlayActivity... params) {
        Game game = ClientModel.getInstance().getGameByID(gameID);
        Player currentPlayer = game.getPlayerByName(username);
        TrainCardHand tch = game.getTrainCardHandForPlayer(username);
        if (!currentPlayer.canClaimRoute(route, tch)){
            return false;
        }
        claimRouteResponse =  ServerFacade.claimRoute(route, username, gameID,colorToUse);
        return claimRouteResponse != null && !claimRouteResponse.isHasError();
    }

    protected void onPostExecute(Boolean success)
    {
        GamePlayActivity gamePlayActivity = (GamePlayActivity) caller;
        gamePlayActivity.onClaimRouteResponse(success);
    }
}
