package model;

import command.ICommand;
import plugin.iDaoAPI;

/**
 * Created by Alyx on 8/9/17.
 */

public class ServerPersistence {

    private static ServerModelFacade model;
    private static iDaoAPI api;

    private static int tempQueueSize = 0;

    private static int DELTA = 5;

    public static void getData() {
        api = ServerModelFacade.getInstance().getDatabaseAPI();
        model = ServerModelFacade.getInstance();
        model.setAccounts(api.getUserAccounts());
        model.setGames(api.getGames());
        model.setCommandList(api.getCommandQueue());

        for(ICommand command : api.getDeltaCommands()) {
            model.applyCommand(command);
        }
    }

    public static void setDelta(int delta)
    {
        DELTA = delta;
    }
    public static void commandApplied(ICommand command) {
        tempQueueSize++;
        api.addDeltaCommand(command);
        if(tempQueueSize >= DELTA) {
            tempQueueSize = 0;
            api.updateGames(model.getGameList().getGames());
        }
    }

    public static void addUserAccount(Account account) {
        api.addUserAccount(account);
    }


}
