package handler;



import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public abstract class HandlerBase implements HttpHandler {

    protected HandlerBase(){

    }
    //THIS FUNCTION NEEDS TO BE IMPLEMENTED BY EVERY HANDLER ---------------------------------------

    /**
     * Defines the basic behavior of each handler. This must be implemented by each handler.
     * @param body the request body.
     * @return the response.
     * @throws IOException if something goes wrong on the server end.
     */
    public abstract String customHandle(String body) throws IOException;

    /**
     * Handles the request given to the server.
     * @param exchange the request.
     * @throws IOException if something goes wrong on the server.
     */
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        boolean success = false;
        try {
            if (exchange.getRequestMethod().toLowerCase().equals("post")) {

                // THIS IS THE UNIQUE PART ABOUT EACH HANDLER --------------------------------------
                InputStream bodyStream = exchange.getRequestBody();
                String body = readInput(bodyStream);
                //String auth = exchange.getRequestHeaders();
                String response = customHandle(body);

                exchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);

                OutputStream responseStream = exchange.getResponseBody();
                OutputStreamWriter sw = new OutputStreamWriter(responseStream);
                sw.write(response);
                sw.flush();
                responseStream.close();

                success = true;
            }

            if (!success) {
                exchange.sendResponseHeaders(HttpURLConnection.HTTP_BAD_REQUEST, 0);
                exchange.getResponseBody().close();
            }
        } catch (IOException e) {
            exchange.sendResponseHeaders(HttpURLConnection.HTTP_SERVER_ERROR, 0);
            exchange.getResponseBody().close();
            System.out.println(e.getMessage());
        }
    }

    /**
     * Gets the request body.
     * @param stream the stream from the request.
     * @return a stringified version of the request body.
     * @throws IOException
     */
    private String readInput(InputStream stream) throws IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader reader = new InputStreamReader(stream);
        char[] buf = new char[1024];
        int length;
        while ((length = reader.read(buf)) > 0) {
            sb.append(buf, 0, length);
        }
        return sb.toString();
    }
}
