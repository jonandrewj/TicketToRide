package fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.List;
import cs340.byu.tickettoride.R;
import model.Color;
import model.DestinationCardHand;
import model.Game;
import model.Player;
import model.TrainCardHand;
import model.data.ClientModel;
import model.facade.PreGameModelFacade;

/**
 * Created by Andrew Jacobson on 7/19/2017.
 */

public class PlayerInfoAdapter extends BaseAdapter {

    private Context context;
    public List<Player> players;
    private Game game;

    public PlayerInfoAdapter(Context context, Game game){
        this.context = context;
        this.players = game.getPlayers();
        this.game = game;
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gameView = inflater.inflate(R.layout.list_game, parent, false);

        TextView gameText = (TextView)gameView.findViewById(R.id.gameID);
        TextView descriptionText = (TextView)gameView.findViewById(R.id.gameDescription);
        ImageView icon = (ImageView)gameView.findViewById(R.id.gameIcon);

        Player player = players.get(position);


        int numTrainCards = 0;
        TrainCardHand hand = game.getTrainCardHandForPlayer(player.getUsername());
        for (Color c : hand.getHandMap().keySet()){
            numTrainCards += hand.getHandMap().get(c);
        }
        DestinationCardHand destinationCardHand = game.getDestinationCardHandForPlayer(player.getUsername());
        gameText.setText(player.getUsername());

        String isTurnStatement = "";
        if (player.getUsername().equals(ClientModel.getInstance().getUser().getUsername())) {
            isTurnStatement = game.getPlayerByName(ClientModel.getInstance().getUser().getUsername()).isPlayerTurn()
                    ? " YOUR TURN" : "";
        }

        descriptionText.setText("  Pieces: " + player.getTrainPiecesLeft() +
                                "      Destinations: " + destinationCardHand.getHand().size() +
                                "      TrainCards: " +  numTrainCards +
                                "      Score: " + player.getScore() +
                                "      Order: " + player.getOrder() +
                                isTurnStatement);
        // TODO: make the score real

        switch(player.getColor()){
            case BLACK:
                icon.setImageResource(R.mipmap.black);
                break;
            case BLUE:
                icon.setImageResource(R.mipmap.blue);
                break;
            case RED:
                icon.setImageResource(R.mipmap.red);
                break;
            case WHITE:
                icon.setImageResource(R.mipmap.white);
                break;
            case YELLOW:
                icon.setImageResource(R.mipmap.yellow);
                break;
            case GREEN:
                icon.setImageResource(R.mipmap.green);
                break;
            default:
                icon.setImageResource(R.mipmap.person);
        }

        return gameView;
    }
}
