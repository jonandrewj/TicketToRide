package handler;

import java.io.IOException;

import request.PollerRequest;
import response.PollerResponse;
import service.PollerService;
import util.Serializer;

/**
 * Created by deriktj on 7/11/2017.
 */

public class PollerHandler extends HandlerBase
{
    /**
     * Handles a poller request.
     * @param body the request body.
     * @return the response from the server.
     * @throws IOException if something goes wrong while processing the request.
     */
    @Override
    public String customHandle(String body) throws IOException
    {
        PollerResponse response = new PollerResponse(true, "Could not complete");
        try {
            PollerRequest request = (PollerRequest) Serializer.fromJson(body, PollerRequest.class);
            response = PollerService.poll(request);
        } catch (Exception e){
            e.printStackTrace();
        }
        return Serializer.toJson(response);

    }
}
