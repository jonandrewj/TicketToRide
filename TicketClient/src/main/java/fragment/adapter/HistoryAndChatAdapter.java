package fragment.adapter;

import android.app.usage.UsageEvents;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cs340.byu.tickettoride.R;
import log.EventType;
import log.HistoryMessage;

/**
 * Created by Andrew Jacobson on 7/19/2017.
 */

public class HistoryAndChatAdapter extends BaseAdapter {

    private Context context;
    private List<HistoryMessage> history;

    private final int CHAT_COLOR = android.graphics.Color.rgb(200, 255, 200); // A light, pastel green
    private final int HISTORY_COLOR = android.graphics.Color.rgb(211, 226, 255); // A pastel blue
    private final int EVENT_COLOR = android.graphics.Color.rgb(226, 150, 245); //A pastel purple
    Map<EventType, Integer> logColors;
    public HistoryAndChatAdapter(Context context, List<HistoryMessage> history) {
        this.context = context;
        this.history = history;
        logColors = new HashMap<>();
        logColors.put(EventType.CHAT, this.CHAT_COLOR);
        logColors.put(EventType.PLAYER_ACTION, HISTORY_COLOR);
        logColors.put(EventType.EVENT, EVENT_COLOR);
    }

    @Override
    public int getCount() {
        return history.size();
    }

    @Override
    public Object getItem(int position) {
        return history.get(history.size() - 1 - position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gameView = inflater.inflate(R.layout.list_game, parent, false);
        TextView sender = (TextView)gameView.findViewById(R.id.gameID);
        TextView messageText = (TextView)gameView.findViewById(R.id.gameDescription);
        ImageView icon = (ImageView)gameView.findViewById(R.id.gameIcon);

        HistoryMessage message = history.get(position);

        sender.setText(message.getOwner());
        messageText.setText(message.format());
        int color = logColors.get(message.getEventType());
        gameView.setBackgroundColor(color);

        return gameView;
    }
}
