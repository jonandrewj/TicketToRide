package fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cs340.byu.tickettoride.R;
import model.Player;

/**
 * Created by Alyx on 8/1/17.
 */

public class ScoreboardAdapter extends BaseAdapter {

    private Context context;

    private List<Player> players;

    private String winner;

    public ScoreboardAdapter(Context context){
        this.context = context;
        this.players = new ArrayList<>();
        this.winner = "";
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        if(position < players.size()){
            return players.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View scoreboardView = inflater.inflate(R.layout.score_list_item, parent, false);

        // Get widgets
        TextView playerName = (TextView) scoreboardView.findViewById(R.id.playerName);
        TextView claimedRouteScore = (TextView) scoreboardView.findViewById(R.id.pointsFromClaimedRoutes);
        TextView completedDestinationScore = (TextView) scoreboardView.findViewById(R.id.reachedDestinationPoints);
        TextView incompleteDestinationScore = (TextView) scoreboardView.findViewById(R.id.unreachedDestinationNegPoints);
        TextView longestRoadScore = (TextView) scoreboardView.findViewById(R.id.longestRoutePoints);
        TextView totalScore = (TextView) scoreboardView.findViewById(R.id.totalPoints);

        // Get stats @ position
        Player player = players.get(position);

        // Set textviews
        playerName.setText(player.getUsername());
        claimedRouteScore.setText("  Claimed Routes: " + 
                player.getScoreFromRoutes() + " points");
        completedDestinationScore.setText("  Completed Destination Cards: " 
                +  player.getScoreFromCompleteDestinationCards() + " points");
        incompleteDestinationScore.setText("  Incomplete Destination Cards: "
                + player.getScoreFromIncompleteDestinationCards() + " points");

        longestRoadScore.setText("  Longest road: " + player.getScoreFromLongestTrain() + " points");

        totalScore.setText("  Total score: " + player.getScore() + " points");
        return scoreboardView;
    }

    public void setScoreboardData(List<Player> players) {
        this.players = players;
    }

}
