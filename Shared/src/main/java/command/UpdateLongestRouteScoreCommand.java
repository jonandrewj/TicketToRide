package command;

/**
 * Created by deriktj on 8/1/2017.
 */

public class UpdateLongestRouteScoreCommand extends Command implements ILoggable
{
    private String gameID;
    private String username;
    private int score;

    public UpdateLongestRouteScoreCommand(String gameID, String username, int score) {
        this.gameID = gameID;
        this.username = username;
        this.score = score;
    }

    @Override
    public Boolean execute() {
        getContext().setLongestRouteScoreForPlayer(gameID,username,score);
        return true;
    }

    @Override
    public String getGameId()
    {
        return gameID;
    }

    @Override
    public String getUsername()
    {
        return username;
    }

    @Override
    public String getLogMessage()
    {
        return username + " was awarded " + score + " points from having the longest route!";
    }
}
