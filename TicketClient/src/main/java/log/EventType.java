package log;

/**
 * Created by Joseph on 7/22/2017.
 *
 * Helps determine the type of history message.
 */
public enum EventType {
    /**
     * The event is a chat message.
     */
    CHAT,
    /**
     * The event is a game play event (specific to a user).
     */
    PLAYER_ACTION,
    /**
     * The event is a result of the game moving forward.
     */
    EVENT,
}
