package fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import activity.GameOverActivity;
import cs340.byu.tickettoride.R;
import fragment.adapter.PlayerInfoAdapter;
import model.Color;
import model.Game;
import model.Player;
import model.TrainCardHand;
import model.data.ClientModel;
import model.facade.GameModelFacade;
import model.facade.PreGameModelFacade;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlayerInfoFragment extends Fragment implements Observer{


    public PlayerInfoFragment() {

        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        ClientModel.getInstance().addObserver(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View infoView =  inflater.inflate(R.layout.fragment_player_info, container, false);


        // NUMBER OF CARDS FOR THE PLAYER
        final String gameID = ClientModel.getInstance().myJoinedGame;
        Game game = PreGameModelFacade.getGameByID(gameID);
        TrainCardHand hand = game.getTrainCardHandForPlayer(ClientModel.getInstance().getUser().getUsername());
        Map<Color, Integer> colorCount = hand.getHandMap();
        TextView black = (TextView) infoView.findViewById(R.id.blackCard);
        black.setText(String.valueOf((colorCount.get(Color.BLACK) == null ? 0 : colorCount.get(Color.BLACK))));
        TextView white = (TextView) infoView.findViewById(R.id.whiteCard);
        white.setText(String.valueOf(colorCount.get(Color.WHITE) == null ? 0 : colorCount.get(Color.WHITE)));
        TextView yellow = (TextView) infoView.findViewById(R.id.yellowCard);
        yellow.setText(String.valueOf(colorCount.get(Color.YELLOW) == null ? 0 : colorCount.get(Color.YELLOW)));
        TextView green = (TextView) infoView.findViewById(R.id.greenCard);
        green.setText(String.valueOf(colorCount.get(Color.GREEN) == null ? 0 : colorCount.get(Color.GREEN)));
        TextView blue = (TextView) infoView.findViewById(R.id.blueCard);
        blue.setText(String.valueOf(colorCount.get(Color.BLUE) == null ? 0 : colorCount.get(Color.BLUE)));
        TextView red = (TextView) infoView.findViewById(R.id.redCard);
        red.setText(String.valueOf(colorCount.get(Color.RED) == null ? 0 : colorCount.get(Color.RED)));
        TextView rainbow = (TextView) infoView.findViewById(R.id.rainbowCard);
        rainbow.setText(String.valueOf(colorCount.get(Color.WILD) == null ? 0 : colorCount.get(Color.WILD)));
        TextView purple = (TextView) infoView.findViewById(R.id.purpleCard);
        purple.setText(String.valueOf(colorCount.get(Color.PINK) == null ? 0 : colorCount.get(Color.PINK)));
        TextView orange = (TextView) infoView.findViewById(R.id.orangeCard);
        orange.setText(String.valueOf(colorCount.get(Color.ORANGE) == null ? 0 : colorCount.get(Color.ORANGE)));


        ListView playerInfoList = (ListView)infoView.findViewById(R.id.playerInfoList);
        PlayerInfoAdapter adapter;
        adapter = new PlayerInfoAdapter(this.getContext(), PreGameModelFacade.getGameByID(gameID));
        playerInfoList.setAdapter(adapter);

        return infoView;
    }

    @Override
    public void onStop() {
        super.onStop();
        ClientModel.getInstance().deleteObserver(this);
    }

    @Override
    public void update(Observable o, Object arg){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.detach(this);
        transaction.attach(this);
        transaction.commit();

        if(arg != null){
            Intent gameIntent = new Intent(this.getActivity(), GameOverActivity.class);
            startActivity(gameIntent);
        }
    }

}
