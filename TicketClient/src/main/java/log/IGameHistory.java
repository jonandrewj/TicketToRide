package log;

import java.util.List;

/**
 * Created by Joseph on 7/15/2017.
 *
 * An interface for a logging system, useful for storing histories, chats, and so forth.
 *
 * @invariant any access to the game history through a get method will not change anything.
 */

interface IGameHistory {
    /**
     * Retrieves all messages contained in the log.
     *
     * @pre log is not empty.
     *
     * @post the log retains the same order as before the call.
     *
     * @return a list of all log messages.
     */
    List<HistoryMessage> getMessages();

    /**
     * A constant to get every message from the getMessages method.
     */
    int ALL_MESSAGES = 0;

    /**
     * Retrieves all messages in the log starting from the offset (inclusive) up until the limit
     * (exclusive).
     *
     * @pre offset must be positive or zero.
     * @pre offset must be less than the amount of log messages in the log.
     * @pre offset + limit must be equal to or less than the size of the log.
     * @pre limit must be 0 or positive, and greater than or equal to offset.
     *
     * @param offset the index to start at in the log.
     * @param limit the number of messages to grab from the log. Using 0 will grab all messages after
     *              the offset.
     * @return the specified number of log messages, starting at the offset.
     * @throws IllegalArgumentException if none of the @pre conditions are met.
     */
    List<HistoryMessage> getMessages(int offset, int limit) throws IllegalArgumentException;

    /**
     * Adds a message to the log.
     *
     * @pre the log message cannot be null.
     *
     * @post the log will be sorted after the message is inserted, according to timestamp.
     * @post log.size == log.old.size + 1
     * @post all log messages inserted before the new message remain unchanged.
     *
     * @param item the command to be added to the history.
     */
    void addMessage(HistoryMessage item);

    /**
     * Gets the total amount of log messages registered to the log.
     *
     * @pre none.
     *
     * @return the total amount of log messages in this log.
     */
    int getCount();
}
