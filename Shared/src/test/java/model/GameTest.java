package model;

import static org.junit.Assert.*;

/**
 * Created by parad on 7/11/2017.
 */
public class GameTest {

    @org.junit.Test
    public void getPlayerCount() throws Exception {
        Game game = new Game(5);
        game.addPlayer("potassium");
        game.addPlayer("tom riddle");
        game.addPlayer("gorubi");
        int count = game.getPlayerCount();
        assertEquals(3, count);
        game.removePlayer("gorubi");
        count = game.getPlayerCount();
        assertEquals(2, count);
    }

    @org.junit.Test
    public void getPlayerMax() throws Exception {
        Game game = new Game();
        assertEquals(5, game.getPlayerMax());
        game.setPlayerMax(3);
        assertEquals(3, game.getPlayerMax());
    }

    @org.junit.Test
    public void setPlayerMax() throws Exception {
        Game game = new Game();
        game.setPlayerMax(3);
        assertEquals(3, game.getPlayerMax());
    }

    @org.junit.Test
    public void equals() throws Exception {
        Game game1 = new Game();
        game1.addPlayer("bill");
        game1.addPlayer("jane");

        Game game2 = new Game();
        game2.addPlayer("bill");
        game2.addPlayer("jane");

        boolean g1eg2 = game1.equals(game2);
        assertEquals(true, g1eg2);
    }

    @org.junit.Test
    public void addPlayer() throws Exception {
        Game game = new Game();
        int count = game.getPlayerCount();
        assertEquals(0, count);
        game.addPlayer("red");
        count = game.getPlayerCount();
        assertEquals(1, count);
    }

}
