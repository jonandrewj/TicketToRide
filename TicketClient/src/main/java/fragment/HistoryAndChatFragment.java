package fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.Observer;
import java.util.Observable;

import activity.GameOverActivity;
import asynctask.ChatTask;
import cs340.byu.tickettoride.R;
import fragment.adapter.HistoryAndChatAdapter;
import log.GameHistory;
import model.data.ClientModel;

/**
 * A simple {@link Fragment} subclass. Provides a presenter to the HistoryAndChat view.
 */
public class HistoryAndChatFragment extends Fragment implements Observer {

    private ListView chatList;
    private HistoryAndChatAdapter adapter;

    /**
     * Creates a new History and Chat fragment for the thing
     */
    public HistoryAndChatFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View chatView =  inflater.inflate(R.layout.fragment_history_and_chat, container, false);

        chatList = (ListView)chatView.findViewById(R.id.historyChatList);

        final String gameId = ClientModel.getInstance().getMyJoinedGame();
        GameHistory game = ClientModel.getInstance().getGameHistory(gameId);
        adapter = new HistoryAndChatAdapter(this.getContext(), game.getMessages());
        chatList.setAdapter(adapter);
        chatList.setSelection(game.getCount());
        final Button sendMessage = (Button)chatView.findViewById(R.id.btnSendChatMessage);

        sendMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                sendMessage(gameId);
            }
        });

        return chatView;
    }

    private void sendMessage(String gameId) {
        EditText chatBox = (EditText)getActivity().findViewById(R.id.chatMessageText);
        String message = chatBox.getText().toString();
        chatBox.getText().clear();

        ChatTask task = new ChatTask(message, gameId);
        task.execute(this);
    }

    /**
     * Updates the log to display any new messages that have arrived.
     */
    @Override
    public void update(Observable o, Object arg){
        FragmentManager manager = getFragmentManager();
        if (manager == null) {
            return;
        }
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.detach(this);
        transaction.attach(this);
        final String gameId = ClientModel.getInstance().getMyJoinedGame();
        GameHistory game = ClientModel.getInstance().getGameHistory(gameId);
        chatList.setSelection(game.getCount());
        transaction.commit();

        if(arg != null){
            Intent gameIntent = new Intent(this.getActivity(), GameOverActivity.class);
            startActivity(gameIntent);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ClientModel.getInstance().addObserver(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        ClientModel.getInstance().deleteObserver(this);
    }
}
