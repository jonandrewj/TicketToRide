package plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.jar.Pack200;

import command.Command;
import command.ICommand;
import model.Account;
import model.Game;
import util.ITypeRetriever;

import com.google.gson.Gson;

import org.tmatesoft.sqljet.core.*;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;
import org.tmatesoft.sqljet.core.table.ISqlJetTable;
import org.tmatesoft.sqljet.core.table.SqlJetDb;

/**
 * Created by Alyx on 8/3/17.
 *
 * A class that represents the database.
 */
public class SQLiteDB implements iDaoAPI {

    final private String GAME_TABLE = "games";
    final private String USER_TABLE = "users";
    final private String DELTA_TABLE = "delta";
    final private String COMMANDS_TABLE = "commands";

    final private String GAME_TABLE_PID = "ID";
    final private String GAME_TABLE_GID = "GID";
    final private String GAME_TABLE_GAME_STATE = "gameState";

    final private String USER_TABLE_UID = "UID";
    final private String USER_TABLE_USERNAME = "username";
    final private String USER_TABLE_PASSWORD = "password";

    final private String COMMANDS_TABLE_CID = "CID";
    final private String COMMANDS_TABLE_COMMAND = "command";

    final private String DELTA_TABLE_DID = "DID";
    final private String DELTA_TABLE_GID = "GID";
    final private String DELTA_TABLE_COMMAND = "command";

    // The query used to create the games table.
    final private String GENERATE_GAME_TABLE_QUERY =
        "CREATE TABLE IF NOT EXISTS " + GAME_TABLE + " (" +
            GAME_TABLE_PID + " INTEGER PRIMARY KEY NOT NULL," +
            GAME_TABLE_GID + " TEXT NOT NULL," +
            GAME_TABLE_GAME_STATE + " TEXT NOT NULL" +
        ");";
    // The query used to create the user table.
    final private String GENERATE_USER_TABLE_QUERY =
        "CREATE TABLE IF NOT EXISTS " + USER_TABLE + " (" +
            USER_TABLE_UID + " INTEGER PRIMARY KEY NOT NULL," +
            USER_TABLE_USERNAME + " TEXT NOT NULL," +
            USER_TABLE_PASSWORD + " TEXT NOT NULL" +
        ");";
    // The query used to create the commands table.
    final private String GENERATE_COMMAND_TABLE_QUERY =
        "CREATE TABLE IF NOT EXISTS " + COMMANDS_TABLE + " (" +
            COMMANDS_TABLE_CID + " INTEGER PRIMARY KEY NOT NULL," +
            COMMANDS_TABLE_COMMAND + " TEXT NOT NULL" +
        ");";
    // The query used to create the delta table.
    final private String GENERATE_DELTA_TABLE_QUERY =
        "CREATE TABLE IF NOT EXISTS " + DELTA_TABLE + " (" +
            DELTA_TABLE_DID + " INTEGER PRIMARY KEY NOT NULL," +
            //DELTA_TABLE_GID + " TEXT," +
            DELTA_TABLE_COMMAND + " TEXT NOT NULL" +
        ");";

    private SqlJetDb database = null;

    private int delta_size = 0;

    /**
     * Attempts to create the database.
     */
    SQLiteDB() throws SqlJetException {
        this(false);
    }

    /**
     * Alternative constructor for testing purposes.
     * @param isTesting if this has been initialized for testing.
     * @throws SqlJetException for anything that could go wrong.
     */
    public SQLiteDB(boolean isTesting) throws SqlJetException {
        File dbFile = isTesting ? new File("ttr_db_test.db") : new File("ticketToRide.db");
        try {
            database = SqlJetDb.open(dbFile, true);
            database.createTable(GENERATE_GAME_TABLE_QUERY);
            database.createTable(GENERATE_USER_TABLE_QUERY);
            database.createTable(GENERATE_COMMAND_TABLE_QUERY);
            database.createTable(GENERATE_DELTA_TABLE_QUERY);
        } catch (SqlJetException e) {
            e.printStackTrace();
        } finally {
            database.commit();
        }
    }

    @Override
    public void addGame(Game game) {
        Gson gson = new Gson();
        String serialized = gson.toJson(game);
        try {
            database.beginTransaction(SqlJetTransactionMode.WRITE);
            ISqlJetTable gameTable = database.getTable(GAME_TABLE);
            gameTable.insert(game.getGameID(), serialized);
            database.commit();
        } catch (SqlJetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateGames(List<Game> games) {
        try {
            database.beginTransaction(SqlJetTransactionMode.WRITE);
            Gson gson = new Gson();

            // Update each game in the list
            database.dropTable(GAME_TABLE);
            database.createTable(GENERATE_GAME_TABLE_QUERY);
            ISqlJetTable gameTable = database.getTable(GAME_TABLE);
            for (Game g : games) {
                gameTable.insert(g.getGameID(), gson.toJson(g));
            }

            // Copy the delta table into the command queue
            List<ICommand> commandJsons = getDeltaCommands();
            ISqlJetTable commandQueue = database.getTable(COMMANDS_TABLE);
            for (ICommand commandJson: commandJsons) {
                commandQueue.insert(gson.toJson(commandJson));
            }

            // Flush the delta table
            database.dropTable(DELTA_TABLE);
            database.createTable(GENERATE_DELTA_TABLE_QUERY);
            database.commit();
        } catch (SqlJetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<ICommand> getDeltaCommands() {
        try {
            List<ICommand> commands = getFromTable(DELTA_TABLE, DELTA_TABLE_COMMAND, ICommand.class, Command.class);
            eraseDelta();
            return commands;
        } catch (IllegalAccessException | SqlJetException | ClassNotFoundException | InstantiationException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void addDeltaCommand(ICommand command) {
        try {
            // This should actually be going to the delta table. When that reaches ten elements,
            // move everything to the main table, and then obliterate delta. Recreate it.
            database.beginTransaction(SqlJetTransactionMode.WRITE);
            ISqlJetTable table = database.getTable(DELTA_TABLE);
            Gson gson = new Gson();
            String serialized =  gson.toJson(command);
            table.insert(serialized);
            database.commit();
            delta_size++;
        } catch (SqlJetException e) {
            e.printStackTrace();
        }
    }

    private <T extends ITypeRetriever, V extends T> List<T> getFromTable(String tableName, String columnName, Class<T> inter, Class<V> base) throws SqlJetException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        Gson gson = new Gson();
        List<T> results = new ArrayList<>();
        database.beginTransaction(SqlJetTransactionMode.READ_ONLY);
        ISqlJetTable table = database.getTable(tableName);
        ISqlJetCursor cursor = table.open();
        try {
            if (!cursor.eof()) {
                do {
                    byte[] bytes = cursor.getBlobAsArray(columnName);
                    String json = new String(bytes);
                    V instance = gson.fromJson(json, base);
                    Class trueClass = Class.forName(instance.getType());
                    T interfaced = (T) gson.fromJson(json, trueClass);
                    results.add(interfaced);
                } while (cursor.next());
            }
            cursor.close();
        }
        catch (SqlJetException sje) {
            sje.printStackTrace();
            throw sje;
        }
        database.commit();
        return results;
    }

    @Override
    public Set<Account> getUserAccounts() {
        HashSet<Account> finalResult = new HashSet<>();
        try{
            database.beginTransaction(SqlJetTransactionMode.READ_ONLY);
            ISqlJetTable table = database.getTable(USER_TABLE);
            ISqlJetCursor cursor = table.open();
            if (!cursor.eof()) {
                do {
                    Account account = new Account(
                            cursor.getString(USER_TABLE_USERNAME),
                            cursor.getString(USER_TABLE_PASSWORD),
                            null // Auth Tokens aren't really necessary for us
                    );
                    finalResult.add(account);
                } while (cursor.next());
            }
            cursor.close();
            database.commit();
        } catch (SqlJetException e) {
            e.printStackTrace();
            return null; // Something went wrong, we probably shouldn't return anything worthwhile
        }
        return finalResult;
    }

    @Override
    public void addUserAccount(Account account) {
        try {
            database.beginTransaction(SqlJetTransactionMode.WRITE);
            ISqlJetTable table = database.getTable(USER_TABLE);
            table.insert(account.getUsername(), account.getPassword());
            database.commit();
        } catch (SqlJetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<ICommand> getCommandQueue() {
        try {
            return getFromTable(COMMANDS_TABLE, COMMANDS_TABLE_COMMAND, ICommand.class, Command.class);
        } catch (SqlJetException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void selfDestruct() {
        try {
            database.beginTransaction(SqlJetTransactionMode.WRITE);
            // Regenerate the games table.
            database.dropTable(GAME_TABLE);
            database.createTable(GENERATE_GAME_TABLE_QUERY);

            // Regenerate the user table.
            database.dropTable(USER_TABLE);
            database.createTable(GENERATE_USER_TABLE_QUERY);

            // Regenerate the commands table.
            database.dropTable(COMMANDS_TABLE);
            database.createTable(GENERATE_COMMAND_TABLE_QUERY);

            // Regenerate the delta table.
            database.dropTable(DELTA_TABLE);
            database.createTable(GENERATE_DELTA_TABLE_QUERY);

            database.commit();
        } catch (SqlJetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Game> getGames() {
        try {
            database.beginTransaction(SqlJetTransactionMode.READ_ONLY);

            ISqlJetTable gameTable = database.getTable(GAME_TABLE);
            ISqlJetCursor cursor = gameTable.open();
            Gson gson = new Gson();
            List<Game> games = new ArrayList<>();
            if (!cursor.eof()) do {
                byte[] bytes = cursor.getBlobAsArray(GAME_TABLE_GAME_STATE);
                String json = new String(bytes);
                Game game = gson.fromJson(json, Game.class);
                games.add(game);
            } while (cursor.next());

            database.commit();

            return games;
        } catch (SqlJetException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void eraseDelta() throws SqlJetException {
        database.beginTransaction(SqlJetTransactionMode.WRITE);
        database.dropTable(DELTA_TABLE);
        database.createTable(GENERATE_DELTA_TABLE_QUERY);
        database.commit();
    }
}
