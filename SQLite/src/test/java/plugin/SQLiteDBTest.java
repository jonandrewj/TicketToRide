package plugin;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import command.GameChatCommand;
import command.ICommand;
import model.Account;
import model.Game;

import static org.junit.Assert.*;

/**
 * Created by parad on 8/8/2017.
 *
 * Various tests for the SQLiteDB.
 */
public class SQLiteDBTest {

    private SQLiteDB database;

    @Before
    public void setUp() throws Exception {
        database = new SQLiteDB(true);
        database.selfDestruct(); // Reset the database before each execution

        // Give the database a game.
        Game game = new Game();
        game.addPlayer("first");
        game.addPlayer("second");
        game.addPlayer("third");
        database.addGame(game);

        // Give the database some accounts
        database.addUserAccount(new Account("first", "1", null));
        database.addUserAccount(new Account("second", "2", null));
        database.addUserAccount(new Account("third", "3", null));

        // Give the database a command to hold
        GameChatCommand gcc = new GameChatCommand("reee", "imma be in ur database", "12345");
        database.addDeltaCommand(gcc);
    }

    @After
    public void tearDown() throws Exception {}

    @Test
    public void addGame() throws Exception {
        Game game = new Game();
        game.addPlayer("run");
        game.addPlayer("death");
        game.addPlayer("sss");
        database.addGame(game);

        assertEquals(2, database.getGames().size());
    }

    @Test
    public void updateAndCheckDeltaCommands() throws Exception {
        assertNotEquals(0, database.getDeltaCommands().size());
        database.updateGames(new ArrayList<Game>());
        assertEquals(0, database.getDeltaCommands().size());
    }

    @Test
    public void updateAndCheckGameCount() throws Exception {
        assertEquals(1, database.getGames().size());
        List<Game> newGameList = new ArrayList<>();
        newGameList.add(new Game(5, "first"));
        database.updateGames(newGameList);
        assertEquals(1, database.getGames().size());
        newGameList.add(new Game(4, "second"));
        database.updateGames(newGameList);
        assertEquals(2, database.getGames().size());
    }

    @Test
    public void getDeltaCommands() throws Exception {
        assertEquals(1, database.getDeltaCommands().size());
    }

    @Test
    public void addDeltaCommand() throws Exception {
        System.out.println("Test: Adding game command.");
        for (Integer i = 0; i < 11; i++) {
            GameChatCommand gcc = new GameChatCommand("first", i.toString(), "eh");
            database.addDeltaCommand(gcc);
        }
        List<ICommand> commands = database.getDeltaCommands();
        assertEquals(12, commands.size());
    }

    @Test
    public void getUserAccounts() throws Exception {
        System.out.println("Test: getting user accounts");
        assertEquals(3, database.getUserAccounts().size());
    }

    @Test
    public void addUserAccount() throws Exception {
        System.out.println("Test: adding user account");
        Account newAccount = new Account("Fourth", "4", null);
        database.addUserAccount(newAccount);
        assertEquals(4, database.getUserAccounts().size());
    }

    @Test
    public void getCommandQueue() throws Exception {
        System.out.println("Test: getting command queue");
        for (Integer i = 0; i < 11; i++) {
            GameChatCommand gcc = new GameChatCommand("first", i.toString(), "eh");
            database.addDeltaCommand(gcc);
        }
        database.updateGames(new ArrayList<Game>());
        assertEquals(12, database.getCommandQueue().size());
    }

    @Test
    public void selfDestruct() throws Exception {
        System.out.println("Test: self-destructing");
        database.selfDestruct();
        assertEquals(0, database.getGames().size());
        assertEquals(0, database.getUserAccounts().size());
        assertEquals(0, database.getCommandQueue().size());
        assertEquals(0, database.getDeltaCommands().size());
    }

    @Test
    public void getGames() throws Exception {
        System.out.println("Test: getting games");
        assertEquals(1, database.getGames().size());
    }

}