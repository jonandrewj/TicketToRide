package states;

import model.Color;
import model.MapRoute;
import model.Player;
import model.TrainCardHand;

/**
 * Created by Joseph on 7/29/2017.
 *
 * Represents a state where a player chose to draw a non-wild train card during their turn.
 */

public class TrainCardDrawnState extends TurnState {
    public TrainCardDrawnState() {
        super("TrainCardDrawnState");
    }

    @Override
    public boolean acceptDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean startTurn(Player target) {
        return false;
    }

    @Override
    public boolean drawCard(Player target, boolean isFaceUp, Color cardColor) {
        if (isFaceUp && cardColor == Color.WILD)
        {
            return false;
        }
        target.setTurnState(new NotYourTurnState());
        return true;
    }

    @Override
    public boolean drawDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean claimRoute(Player target, MapRoute route, TrainCardHand hand) {
        return false;
    }

    @Override
    public void endGame(Player target) { }
}
