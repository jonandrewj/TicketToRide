package asynctask;

import android.os.AsyncTask;

import serverproxy.ServerFacade;
import model.data.ClientModel;
import response.ResponseBase;
import fragment.GameSelectFragment;

/**
 * Created by Alyx on 7/9/17.
 */

public class CreateGameTask extends AsyncTask<GameSelectFragment, Integer, Boolean> {
    /** Register result, contains the result of the register attempt */
    private ResponseBase creationResponse;

    /** Access the model */
    private ClientModel model;

    /** Access the database */
    private ServerFacade facade;

    /** Holds the user's username & number of players requested for game */
    private int numPlayers;

    /** Constants **/
    private final int MAX_PLAYERS = 5;
    private final int MIN_PLAYERS = 2;

    /** The fragment that called this task */
    private GameSelectFragment caller;


    public CreateGameTask() {
        facade = new ServerFacade();
        creationResponse = null;

        // Default to 5
        numPlayers = 5;

        model = ClientModel.getInstance();
    }


    protected Boolean doInBackground(GameSelectFragment... urls) {
        // Get info from the UI
        this.caller = urls[0];

        creationResponse =  facade.createGame(numPlayers);

        if(creationResponse != null && !creationResponse.isHasError()){

            // Return success
            return true;

        } else {
            return false;
        }

    }

    protected void onProgressUpdate(Integer... progress) {
        // progressBar.setProgress(progress[0]);
    }

    protected void onPostExecute(Boolean success) {

        // Tell the view!

        if(success){
            caller.gameCreateUpdate("Game creation sent successfully.", success);

//            System.out.println("Game creation successful.");

        } else {
            caller.gameCreateUpdate("Game creation failed to send.", success);
//            System.out.println("Game creation FAILED.");
        }

    }

    public void createGame(int numPlayers) {
        this.numPlayers = numPlayers;
        if(numPlayers > MAX_PLAYERS || numPlayers < MIN_PLAYERS){
            this.numPlayers = 5;
        }
    }

}
