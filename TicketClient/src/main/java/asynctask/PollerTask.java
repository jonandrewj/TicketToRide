package asynctask;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.List;

import command.Command;
import command.ICommand;
import serverproxy.ServerFacade;
import model.data.ClientModel;
import response.PollerResponse;
import util.Serializer;

/**
 * Created by Alyx on 7/10/17.
 */

public class PollerTask extends AsyncTask<Fragment, Integer, Boolean> {
    /** Register result, contains the result of the register attempt */
    private PollerResponse pollResponse;

    /** Access the database */
    private ServerFacade facade;

    /** Holds the user's username & password as raw text */
    private ClientModel model;

    /** The fragment that called this task */
    private Fragment caller;

    private List<ICommand> commands;

//    private boolean loginSuccessful;

    /**
     * Creates a new poller instance.
     */
    public PollerTask() {
        facade = new ServerFacade();
        pollResponse = null;
        model = ClientModel.getInstance();
        commands = new ArrayList<>();
    }

    protected Boolean doInBackground(Fragment... urls) {
        // Get info from the UI
//        this.caller = urls[0];
        try{
            pollResponse = facade.poll(model.getLastIndex());
            if(pollResponse != null) {
                if (!pollResponse.isHasError()) {
                    model.setLastIndex(pollResponse.getLastIndex());

                    // Execute the commands
                    List<String> commandsJson = pollResponse.getCommands();
                    //List<ICommand> commands = new ArrayList<>();
                    for(String commandJson : commandsJson){
                        //System.out.println("commandJson:  " + commandJson);
                        Command command = (Command) Serializer.fromJson(commandJson,Command.class);
                        ICommand castCommand = (ICommand) Serializer.fromJson(commandJson,Class.forName(command.getCommandClassName()));
                        commands.add(castCommand);
                    }
                    Lists.reverse(commands);
                    //executeCommands(commands);

                    // Return success
                    return true;
                } else {
                    // If error, return false;
                    System.out.println("Failure: ");
                    System.out.println(pollResponse.getErrorMessage());
                    return false;
                }
            }
            else {
                //System.out.println("null");
                return false;
            }
        } catch (Exception e){
            e.printStackTrace();
            System.out.print("nothing");
        }
        return false;
    }

    protected void onProgressUpdate(Integer... progress) {
        // progressBar.setProgress(progress[0]);
    }

    protected void onPostExecute(Boolean success) {
        executeCommands(commands);
    }

    private void executeCommands(List<ICommand> commands){
        for(ICommand command : commands){
            System.out.println("Applying: " + command);
            model.applyCommand(command);
        }
    }

}
