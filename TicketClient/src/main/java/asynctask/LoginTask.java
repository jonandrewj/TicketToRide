package asynctask;

import android.os.AsyncTask;

import model.AuthToken;
import model.User;
import serverproxy.ServerFacade;
import response.LoginResponse;
import fragment.LoginFragment;

/**
 * Created by Alyx on 7/9/17.
 */

public class LoginTask extends AsyncTask<LoginFragment, Integer, LoginResponse> {
    /** Register result, contains the result of the register attempt */
    private LoginResponse loginResponse;

    /** The authorization token returned from the server upon successful authentication */
     String authToken;

    /** Access the model */
    private User user;

    /** Access the database */
    private ServerFacade facade;

    /** Holds the user's username & password as raw text */
    private String username;
    private String password;

    /** The fragment that called this task */
    private LoginFragment caller;

    public void setLoginFields(String username, String password){
        this.username = username;
        this.password = password;
    }


//    private boolean loginSuccessful;

    public LoginTask() {
        facade = new ServerFacade();
        loginResponse = null;
        username = "";
        password = "";
        authToken = "";
        user = User.getInstance();
    }


    protected LoginResponse doInBackground(LoginFragment... urls) {
        // Get info from the UI
        this.caller = urls[0];

        // Login thru database
        loginResponse = facade.login(username,password);


        // If no error, set authToken in the model.
        if(loginResponse != null && !loginResponse.isHasError()){

            // Get token
            authToken = loginResponse.getAuthToken().getToken();

            // Fill up the model
            user.setUsername(username);
            user.setAuthToken(new AuthToken(authToken));

        } else {
            // If error, return false;
            System.out.println("Failure: ");
            if(loginResponse != null) {
                System.out.println(loginResponse.getErrorMessage());
            }

        }
        return loginResponse;
    }

    protected void onProgressUpdate(Integer... progress) {
        // progressBar.setProgress(progress[0]);
    }

    protected void onPostExecute(LoginResponse response) {
        caller.authenticationUpdate(response.getErrorMessage(), !response.isHasError());

    }


}