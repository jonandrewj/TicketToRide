package log;

import android.app.usage.UsageEvents;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Joseph on 7/17/2017.
 *
 * @invariant No fields, except color, are ever modified once the object is created.
 */

public class HistoryMessage implements IHistoryMessage {

    private final String owner;
    private final String message;
    private final Date timestamp;
    private final EventType eventType;

    /**
     * Creates a HistoryMessage that can be placed into a GameHistory.
     * @param message the message contained in this instance.
     * @param owner the name of the player who caused this message to be generated.
     */
    public HistoryMessage(String message, String owner, Date timestamp, EventType eventType) {
        this.message = message;
        this.owner = owner;
        this.timestamp = timestamp;
        this.eventType = eventType;
    }

    /**
     * Compares the timestamp of a history message.
     *
     * @pre o is an instance of IHistoryMessage.
     *
     * @post o is unchanged after execution.
     *
     * @param o the object to compare to.
     * @return a result according to the order the items were made.
     */
    @Override
    public final int compareTo(Object o) {
        HistoryMessage other = (HistoryMessage)o;
        return this.timestamp.compareTo(other.timestamp);
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    final public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String format() {
        String formattedTime = new SimpleDateFormat("h:mm a, M/d/yy").format(timestamp);
        return String.format("%s (at %s)", message, formattedTime);
    }

    @Override
    final public String getOwner() {
        return this.owner;
    }

    @Override
    public EventType getEventType() {
        return this.eventType;
    }

}
