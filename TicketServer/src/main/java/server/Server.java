package server;

import com.sun.net.httpserver.HttpServer;
import com.sun.net.httpserver.HttpServer;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import handler.CommandHandler;
import handler.LoginHandler;
import handler.PollerHandler;
import handler.RegisterHandler;
import model.ServerModel;
import model.ServerModelFacade;
import model.ServerPersistence;
import plugin.iDaoAPI;
import plugin.iDatabaseFactory;

/**
 * Created by Andrew Jacobson on 7/1/2017.
 */

public class Server {
    private HttpServer server;

    public static void main(String[] args){
        System.out.println(System.getProperty("user.dir"));
        String jarPath = "jars/MyMongo.jar";
        String database = "MongoDatabaseFactory";
        int delta = 5;
        int port = 8080;
        Boolean wipeDB = false;

        // GET THE COMMAND LINE ARGUMENTS

        if (args.length >= 4) {
            jarPath = args[0];
            database = args[1];
            delta = Integer.parseInt(args[2]);
            port = Integer.parseInt(args[3]);
            if(args.length >= 5) {
                if(args[4].equals("-wipe")) {
                    wipeDB = true;
                }
            }
        }
        URLClassLoader cl = null;
        try {
            cl = loadLibrary(jarPath);
            iDaoAPI databaseAPI;
            iDatabaseFactory databaseFactory = AbstractDatabaseFactory.getDatabaseFactory("plugin." + database, cl);
            if (databaseFactory != null){
                databaseAPI = databaseFactory.getDatabase();
            }
            else {
                System.out.println("Invalid Database Argument");
                return;
            }

            ServerPersistence.setDelta(delta);

            // SET THE SERVER MODEL'S oh I seDATABASE API INSTANCE
            ServerModelFacade.getInstance().setDatabaseAPI(databaseAPI);
            if(wipeDB) {
                databaseAPI.selfDestruct();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }


        ServerModelFacade.getInstance().getDatabaseData();
        new Server().run(port);
    }

    /**
     * Initializes the server on the specified port.
     * @param portNumber the port to run the server on.
     */
    private void run(int portNumber){
        try {
            server = HttpServer.create(new InetSocketAddress(portNumber), 12);
        } catch (IOException e){
            System.out.println(e.getMessage());
            return;
        }

        server.setExecutor(null);
        server.createContext("/game/register", new RegisterHandler());
        server.createContext("/game/login", new LoginHandler());
        server.createContext("/command", new CommandHandler());
        server.createContext("/poll", new PollerHandler());
        server.start();
        System.out.println("Server Started");
    }

    public static synchronized URLClassLoader loadLibrary(String path) throws Exception
    {
        JarFile jarFile = new JarFile(path);
        Enumeration<JarEntry> e = jarFile.entries();

        URL[] urls = { new URL("jar:file:" + path+"!/") };
        URLClassLoader cl = URLClassLoader.newInstance(urls);

        while (e.hasMoreElements()) {
            JarEntry je = e.nextElement();
            if(je.isDirectory() || !je.getName().endsWith(".class")){
                continue;
            }
            // -6 because of .class
            String className = je.getName().substring(0,je.getName().length()-6);
            className = className.replace('/', '.');
            //System.out.println(className);
            try {
                cl.loadClass(className);
            } catch (NoClassDefFoundError ex){
                System.out.println(ex.getMessage());
            } catch (ClassNotFoundException ex){
                System.out.println(ex.getMessage());
            }

        }
        return cl;
    }
}
