package serverproxy;

import com.google.gson.Gson;

import java.util.List;
import java.util.UUID;

import command.ClaimRouteCommand;
import command.CreateGameCommand;
import command.DiscardDestinationCardCommand;
import command.DrawDestinationCardCommand;
import command.GameChatCommand;
import command.DrawTrainCardCommand;
import command.JoinGameCommand;
import command.StartGameCommand;
import model.Color;
import model.DestinationCard;
import model.MapRoute;
import model.data.ClientModel;
import model.User;
import request.LoginRequest;
import request.PollerRequest;
import request.RegisterRequest;
import response.ClaimRouteResponse;
import response.CommandResponse;
import response.LoginResponse;
import response.PollerResponse;
import response.RegisterResponse;

/**
 * Created by Andrew Jacobson on 7/2/2017.
 *
 * Helps control game logic.
 */

public class InitGameProxy {
    public static Gson gson = new Gson();


    /** TEAM: SET YOUR IP ADDRESS HERE */
    //private static String serverIP = "192.168.1.253"; // Alexis's IP

    private static String serverIP = "10.24.66.177"; // This is used by all android emulators for localhost


    /**
     * @param ip the host of the server.
     */
    public static void setServerIP(String ip){
        serverIP = ip;
    }

    /**
     * Retrieves a register response from the server.
     * @param request the request object to be sent to the server.
     * @return the response from the server.
     */
    public static RegisterResponse register(RegisterRequest request){
        RegisterResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(request, "game/register", serverIP);
            response = gson.fromJson(jsonResponse, RegisterResponse.class);
        } catch (Exception e) {
            response = new RegisterResponse(true, e.getMessage(), null);
        }
        if(response == null){
            System.out.println("Why null?");
        }
        return response;
    }

    /**
     * Retrieves a login response from the server.
     * @param request the request to be sent to the server.
     * @return the server's response to the request.
     */
    public static LoginResponse login(LoginRequest request){
        LoginResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(request, "game/login", serverIP);
            response = gson.fromJson(jsonResponse, LoginResponse.class);
        } catch (Exception e) {
            response = new LoginResponse(true, e.getMessage(), null);
        }
        return response;
    }

    /**
     * Sends a request to the server to join the player to the given game.
     * @param gameID the id of the game to join.
     * @return a response from the server.
     */
    public static CommandResponse joinGame(String gameID) {
        CommandResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new JoinGameCommand(gameID, ClientModel.getInstance().getUser().getUsername()),"/game/command",serverIP);
            response = gson.fromJson(jsonResponse, CommandResponse.class);
        }
        catch(Exception e) {
            response = new CommandResponse(true,e.getMessage());
        }
        return response;
    }

    /**
     * Asks the server to create a game.
     * @param maxPlayers the maximum amount of players to allow in the game.
     * @return the server's response to the request.
     */
    public static CommandResponse createGame(int maxPlayers) {
        CommandResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new CreateGameCommand(maxPlayers,UUID.randomUUID().toString()),"/game/command",serverIP);
            response = gson.fromJson(jsonResponse, CommandResponse.class);
        }
        catch(Exception e) {
            response = new CommandResponse(true,e.getMessage());
        }
        System.out.println(response);
        return response;
    }

    /**
     * Polls the server.
     * @return the response to the poll request.
     */
    public static PollerResponse poll(int lastIndex) {
        PollerResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new PollerRequest(User.getInstance().getUsername(),lastIndex),"/game/poll",serverIP);
            response = gson.fromJson(jsonResponse, PollerResponse.class);
            System.out.println("s");
        }
        catch(Exception e) {
            response = new PollerResponse(true,e.getMessage());
        }
        if(response.getCommandCount() > 0){
            System.out.print("stop");
        }
        return response;
    }

    /**
     * Sends a start game command to the server.
     * @param gameID the id of the game.
     * @return the server's response.
     */
    public static CommandResponse startGame(String gameID) {
        CommandResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new StartGameCommand(gameID),"/game/command",serverIP);
            response = gson.fromJson(jsonResponse, CommandResponse.class);
        }
        catch(Exception e) {
            response = new CommandResponse(true,e.getMessage());
        }
        return response;
    }

    /**
     * Claims a route by sending a request to tje server.
     * @param route the route to be claimed.
     * @param userName the name of the user claiming the route
     * @param gameID the id of the game to change
     * @return the response from the server.
     */
    public static ClaimRouteResponse claimRoute(MapRoute route, String userName, String gameID, Color colorToUse)
    {
        ClaimRouteResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new ClaimRouteCommand(route, userName, gameID, colorToUse),"/game/command",serverIP);
            response = gson.fromJson(jsonResponse, ClaimRouteResponse.class);
        }
        catch(Exception e) {
            response = new ClaimRouteResponse(true,e.getMessage());
        }
        return response;
    }

    /**
     * Sends a chat message to the server.
     * @param message the message to be sent.
     * @param gameId the game to associate the message with.
     * @return a response from the server.
     */
    public static CommandResponse sendChat(String message, String gameId) {
        CommandResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new GameChatCommand(User.getInstance().getUsername(), message, gameId), "/game/command", serverIP);
            response = gson.fromJson(jsonResponse, CommandResponse.class);
        }
        catch (Exception e) {
            response = new CommandResponse(true, e.getMessage());
        }
        return response;
    }

    /**
     * Draws a train card by sending a request to the server.
     * @param username the username of the player drawing the card.
     * @param gameID the id of the game the player belongs to.
     * @param index the index of the card being drawn.
     * @return the response from the server.
     */
    public static CommandResponse drawTrainCard(String username,String gameID,int index) {
        CommandResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new DrawTrainCardCommand(username, gameID, index),"/game/command",serverIP);
            response = gson.fromJson(jsonResponse, CommandResponse.class);
        }
        catch(Exception e) {
            response = new CommandResponse(true,e.getMessage());
        }
        return response;
    }

    /**
     * Draws a destination card by sending a request to the server.
     * @param username the name of the player drawing the destination card.
     * @param gameID the id of the game the player belongs to.
     * @return the response from the server.
     */
    public static CommandResponse drawDestinationCard(String username, String gameID)
    {
        CommandResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new DrawDestinationCardCommand(username, gameID),"/game/command",serverIP);
            response = gson.fromJson(jsonResponse, CommandResponse.class);
        }
        catch(Exception e) {
            response = new CommandResponse(true,e.getMessage());
        }
        return response;
    }

    /**
     * Discards a given destination card owned by a player.
     * @param username the username of the player discarding the route card.
     * @param gameID the id of the game the player belongs to.
     * @param cardsToDiscard a list of cards to get rid of.
     * @return the response from the server.
     */
    public static CommandResponse discardDestinationCard(String username, String gameID, List<DestinationCard> cardsToDiscard)
    {
        CommandResponse response;
        try {
            String jsonResponse = ClientCommunicator.get().sendRequest(new DiscardDestinationCardCommand(username, gameID,cardsToDiscard),"/game/command",serverIP);
            response = gson.fromJson(jsonResponse, CommandResponse.class);
        }
        catch(Exception e) {
            response = new CommandResponse(true,e.getMessage());
        }
        return response;
    }
}
