package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by deriktj on 7/23/2017.
 */

/**  A collection of DestinationCard, along with some methods to use it.
 *
 *  @invariant deck != null
 */
public class DestinationCardDeck
{

    /** A list of destination cards */
    private List<DestinationCard> deck;

    /** An integer that tracks the number of cards left in the deck */
    private int deckSize;

    /** Constructor
     *
     * @pre none
     * @post deck != null
     * @post deckSize = 30
     */
    public DestinationCardDeck() {
        deck = new ArrayList<>();
        deckSize = 30;
    }

    /** Draws the top card of the deck
     * @pre none
     * @post the drawn card is removed from the deck
     *
     * @return the destination card drawn from the top of the deck
     */
    public DestinationCard draw() {
        return deck.remove(deck.size() - 1);
    }

    /** Discards a list of cards and adds them to the bottom of the deck
     *
     * @param cards != null
     * @pre none
     * @post the cards, if any, are added to the bottom of the deck
     */
    public void discard(List<DestinationCard> cards) {
        for(DestinationCard card : cards){
            discard(card);
        }
    }

    /** Discards a single card and adds it to the bottom of the deck
     *
     * @param card != null
     * @pre none
     * @post the card is added to the bottom of the deck
     */
    public void discard(DestinationCard card) {
        deck.add(0,card);
    }

    /**
     * Initializes the deck to have the 30 destination cards found in the game
     * @pre none
     * @post the deck has the 30 destination cards in it
     */
    public void initializeDeck() {
        deck.add(new DestinationCard("Los Angeles","New York City",21));
        deck.add(new DestinationCard("Duluth","Houston",8));
        deck.add(new DestinationCard("Sault St Marie","Nashville",8));
        deck.add(new DestinationCard("New York City","Atlanta",6));
        deck.add(new DestinationCard("Portland","Nashville",17));
        deck.add(new DestinationCard("Vancouver","Montreal",20));
        deck.add(new DestinationCard("El Paso","Duluth",10));
        deck.add(new DestinationCard("Toronto","Miami",10));
        deck.add(new DestinationCard("Portland","Phoenix",11));
        deck.add(new DestinationCard("Dallas","New York City",11));
        deck.add(new DestinationCard("Calgary","Salt Lake City",7));
        deck.add(new DestinationCard("Calgary","Phoenix",13));
        deck.add(new DestinationCard("Los Angeles","Miami",20));
        deck.add(new DestinationCard("Winnipeg","Little Rock",11));
        deck.add(new DestinationCard("San Francisco","Atlanta",17));
        deck.add(new DestinationCard("Kansas City","Houston",5));
        deck.add(new DestinationCard("Los Angeles","Chicago",16));
        deck.add(new DestinationCard("Denver","Pittsburgh",11));
        deck.add(new DestinationCard("Chicago","Santa Fe",9));
        deck.add(new DestinationCard("Vancouver","Santa Fe",13));
        deck.add(new DestinationCard("Boston","Miami",12));
        deck.add(new DestinationCard("Chicago","New Orleans",7));
        deck.add(new DestinationCard("Montreal","Atlanta",9));
        deck.add(new DestinationCard("Seattle","New York City",22));
        deck.add(new DestinationCard("Denver","El Paso",4));
        deck.add(new DestinationCard("Helena","Los Angeles",8));
        deck.add(new DestinationCard("Winnipeg","Houston",12));
        deck.add(new DestinationCard("Montreal","New Orleans",13));
        deck.add(new DestinationCard("Sault St Marie","Oklahoma City",9));
        deck.add(new DestinationCard("Seattle","Los Angeles",9));
        Collections.shuffle(deck);
    }

    //TODO: reduce the jank of the way this is implemented

    /** gets the size of the deck list
     * @pre none
     * @post none
     * @return the size of the deck list
     */
    public int getSize() {
        return deck.size();
    }

    /** Returns the variable deckSize
     *
     * @pre none
     * @post none
     * @return the variable deckSize
     */
    public int getDeckSize()
    {
        return deckSize;
    }

    /** sets the variable deckSize
     * @pre deckSize >= 0 and deckSize <= 30
     * @post deckSize is set to the parameter, deckSize
     * @param deckSize
     */
    public void setDeckSize(int deckSize)
    {
        this.deckSize = deckSize;
    }
}
