package service;

import java.util.List;

import command.Command;
import command.ICommand;
import model.ServerModel;
import model.ServerModelFacade;
import model.User;
import request.PollerRequest;
import response.PollerResponse;

/**
 * Created by deriktj on 7/11/2017.
 */

public class PollerService {

    public static PollerResponse poll(PollerRequest pollerRequest) {
        PollerResponse response = new PollerResponse(false,"");
        String username = pollerRequest.getUsername();
        ServerModelFacade model = ServerModelFacade.getInstance();
        List<ICommand> list = model.getCommandListForUser(username);
        if(list != null) {
            int lastIndex = pollerRequest.getIndex();
            //System.out.println("last index for " + username + " is " + lastIndex + " compared to list size " + list.size());
            for(int i = lastIndex + 1; i < list.size(); i++){
                response.addCommand((Command) list.get(i));
            }
            response.setLastIndex(list.size() - 1);
        }
        return response;
    }
}

