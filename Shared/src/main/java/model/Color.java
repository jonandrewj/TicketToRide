package model;

/**
 * Created by deriktj on 7/16/2017.
 */

public enum Color
{
    PINK,
    WHITE,
    BLUE,
    YELLOW,
    ORANGE,
    BLACK,
    RED,
    GREEN,
    WILD
}
