package fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cs340.byu.tickettoride.R;

/**
 * Created by Andrew Jacobson on 7/16/2017.
 */

public class PlayerListAdapter extends BaseAdapter {

    private Context context;
    public ArrayList<PlayerData> players;

    public class PlayerData {
        public String username;

        public PlayerData(String username) {
            this.username = username;
        }
    }

    public PlayerListAdapter(Context context) {
        this.context = context;
        players = new ArrayList<>();
    }

    public void addPlayer(String username){
        players.add(new PlayerData(username));
    }

    @Override
    public int getCount() {
        return players.size();
    }

    @Override
    public Object getItem(int position) {
        return players.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gameView = inflater.inflate(R.layout.list_game, parent, false);

        TextView gameText = (TextView)gameView.findViewById(R.id.gameID);
        TextView descriptionText = (TextView)gameView.findViewById(R.id.gameDescription);
        ImageView icon = (ImageView)gameView.findViewById(R.id.gameIcon);

        PlayerData player = players.get(position);
        gameText.setText("Player " + player.username + " has joined");
        return gameView;
    }
}
