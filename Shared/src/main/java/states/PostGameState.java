package states;

import model.Color;
import model.MapRoute;
import model.Player;
import model.TrainCardHand;

/**
 * Created by parad on 7/31/2017.
 *
 * Represents the player in the post game. They can't do anything, so everything returns false.
 */

public class PostGameState extends TurnState {
    public PostGameState() {
        super("PostGameState");
    }

    @Override
    public boolean acceptDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean startTurn(Player target) {
        return false;
    }

    @Override
    public boolean drawCard(Player target, boolean isFaceUp, Color cardColor) {
        return false;
    }

    @Override
    public boolean drawDestinationCards(Player target) {
        return false;
    }

    @Override
    public boolean claimRoute(Player target, MapRoute route, TrainCardHand hand) {
        return false;
    }

    @Override
    public void endGame(Player target) { }
}
