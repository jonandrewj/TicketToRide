package command;

/**
 * Created by deriktj on 8/1/2017.
 */

public class UpdateDestinationScoreCommand extends Command implements ILoggable
{
    private String gameID;
    private String username;
    private int completeScore;
    private int incompleteScore;

    public UpdateDestinationScoreCommand(String gameID, String username, int completeScore, int incompleteScore) {
        this.gameID = gameID;
        this.username = username;
        this.completeScore = completeScore;
        this.incompleteScore = incompleteScore;
    }

    @Override
    public Boolean execute() {
        getContext().setDestinationScoreForPlayer(gameID,username,completeScore,incompleteScore);
        return true;
    }

    @Override
    public String getGameId()
    {
        return gameID;
    }

    @Override
    public String getUsername()
    {
        return username;
    }

    @Override
    public String getLogMessage()
    {
        return username + " was awarded " + completeScore + " points from completed destination cards and penalized " + incompleteScore + " for incomplete destination cards!";
    }
}
