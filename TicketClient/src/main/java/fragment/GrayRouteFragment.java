package fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import activity.GameOverActivity;
import activity.GamePlayActivity;
import asynctask.ClaimRouteTask;
import cs340.byu.tickettoride.R;
import fragment.adapter.PlayerListAdapter;
import model.Color;
import model.Game;
import model.MapRoute;
import model.TrainCardHand;
import model.data.ClientModel;
import model.facade.PreGameModelFacade;

import static activity.GamePlaySingleton.gamePlaySingleton;
import static model.Color.*;


public class GrayRouteFragment extends Fragment implements Observer {


    private View grayRouteFragment;
    List<String> colors = new ArrayList<>();
    List<Color> colorOptions = new ArrayList<>();
    PlayerListAdapter playerListAdapter;
    private ListView mColorList;
    final Context context;
    AlertDialog dialog;
    MapRoute mapRoute;
    String user;
    String gameID;
    String[] colorChoices = new String[8];
    int choice;


    public GrayRouteFragment()  {


        context = getContext();

        playerListAdapter = null;
        colors.add("PINK");
        colors.add("WHITE");
        colors.add("BLUE");
        colors.add("YELLOW");
        colors.add("ORANGE");
        colors.add("BLACK");
        colors.add("RED");
        colors.add("GREEN");
        colors.add("WILD");

        colorChoices[0] = ("PINK");
        colorChoices [1] = ("WHITE");
        colorChoices [2] = ("BLUE");
        colorChoices[3] = ("YELLOW");
        colorChoices [4] =("ORANGE");
        colorChoices [5] =("BLACK");
        colorChoices [6] =("RED");
        colorChoices [7] =("GREEN");


        colorOptions.add(PINK);
        colorOptions.add(WHITE);
        colorOptions.add(BLUE);
        colorOptions.add(YELLOW);
        colorOptions.add(ORANGE);
        colorOptions.add(BLACK);
        colorOptions.add(RED);
        colorOptions.add(GREEN);
        colorOptions.add(WILD);
    }


    public MapRoute getMapRoute() {
        return mapRoute;
    }

    public void setMapRoute(MapRoute mapRoute) {
        this.mapRoute = mapRoute;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setGameID(String gameID) {
        this.gameID = gameID;
    }

    void claimRouteCaller(String gameID, String user,Color colorToUse)
    {
        TrainCardHand hand = ClientModel.getInstance().getGameByID(gameID).getTrainCardHandForPlayer(user);
        int specificColorCards;
        if(hand.getHandMap().get(mapRoute.getGrayRouteTrainColor()) == null)
        {
            specificColorCards = 0;
        }
        else
        {
            specificColorCards = hand.getHandMap().get(mapRoute.getGrayRouteTrainColor());
        }
        int wildCards;
        if(hand.getHandMap().get(Color.WILD) == null)
        {
            wildCards = 0;
        }
        else
        {
            wildCards = hand.getHandMap().get(WILD);
        }
        int combinedCards = specificColorCards + wildCards;
        try {
            if (combinedCards >= mapRoute.getRouteLength()) {

                ClaimRouteTask claimRouteTask = new ClaimRouteTask(mapRoute, user, gameID, gamePlaySingleton.getGameplayContext(),colorToUse);
                claimRouteTask.execute();
            } else {
                Toast.makeText(getContext(), "You don't Have enough cards for that route and color", Toast.LENGTH_SHORT).show();
            }
        } catch (NullPointerException e) {
            Toast.makeText(getContext(), "You don't Have enough cards for that route and color", Toast.LENGTH_SHORT).show();

        }
    }

    public void claimRouteHelper(String color)
    {


        if( parallelRouteProtector(user, mapRoute))
        {
            mapRoute.setGrayRouteTrainColor(getColorEnum(color));
            claimRouteCaller(gameID, user, Color.valueOf(color));
        }
        onBackPressed();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        grayRouteFragment = inflater.inflate(R.layout.fragment_gray_route, container, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("What Color of Card Would You Like To USe");

        int checkedItem = 0;
        choice = 0;
        builder.setSingleChoiceItems(colorChoices, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // user checked an item
               choice = which;
            }
        });

                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        claimRouteHelper(colorChoices[choice]);
                    }
                });
//                builder.setNegativeButton("Cancel", null);
//
// create and show the alert dialog
                dialog = builder.create();
                dialog.setCancelable(false);
                dialog.show();

//
//
//            }
//        });

        return grayRouteFragment;
    }
    String getColor(MapRoute route)
    {
        String color = "ORANGE";
        model.Color routeColor = route.getRouteColor();
        if (routeColor.equals(model.Color.RED))
        {
            color = "RED";
        }
        if (routeColor.equals(model.Color.BLACK))
        {
            color = "BLACK";
        }
        if (routeColor.equals(model.Color.BLUE))
        {
            color = "BLUE";
        }

        if (routeColor.equals(Color.WILD))
        {
            color = "GRAY";
        }
        if (routeColor.equals(model.Color.GREEN))
        {
            color = "GREEN";
        }
        if (routeColor.equals(model.Color.PINK))
        {
            color = "MAGENTA";
        }
        if (routeColor.equals(model.Color.WHITE))
        {
            color = "WHITE";
        }
        if (routeColor.equals(model.Color.YELLOW))
        {
            color = "YELLOW";
        }

        return color;
    }
    Color getColorEnum(String routeColor)
    {
        model.Color color = Color.ORANGE;

        if (routeColor.equals("RED"))
        {
            color = Color.RED;
        }
        if (routeColor.equals("BLACK"))
        {
            color = model.Color.BLACK;
        }
        if (routeColor.equals("BLUE"))
        {
            color = model.Color.BLUE;
        }
        if (routeColor.equals("WILD"))
        {
            color = Color.WILD;
        }
        if (routeColor.equals("GREEN"))
        {
            color = model.Color.GREEN;
        }
        if (routeColor.equals("PINK"))
        {
            color = model.Color.PINK;
        }
        if (routeColor.equals("WHITE"))
        {
            color = model.Color.WHITE;
        }
        if (routeColor.equals("YELLOW"))
        {
            color = Color.YELLOW;
        }

        return color;
    }

    public void onBackPressed() {
        Intent gameIntent = new Intent(getContext(), GamePlayActivity.class);
        startActivity(gameIntent);
    }


    @Override
    public void update(Observable o, Object arg) {
        if(arg != null){
            Intent gameIntent = new Intent(this.getActivity(), GameOverActivity.class);
            startActivity(gameIntent);
        }
    }

    @Override
    public void onStart(){
        super.onStart();
        ClientModel.getInstance().addObserver(this);
    }

    @Override
    public void onStop(){
        super.onStop();
        ClientModel.getInstance().deleteObserver(this);
    }

    public Boolean parallelRouteProtector(String username, MapRoute mapRoute)
    {
        List<MapRoute> availableRoutes = PreGameModelFacade.getGameByID(ClientModel.getInstance().getMyJoinedGame()).getMapRoutes();

        Game game = PreGameModelFacade.getGameByID(ClientModel.getInstance().myJoinedGame);
        if (game.getPlayerCount() == 4 || game.getPlayerCount() == 5) {
            return true;
        }
        for (MapRoute route : availableRoutes)
        {
            if (route.getOwnerID() != null)
            {
                if(route.getOwnerID().equals(username))
                {
                    if(mapRoute.getCity1().getName().equals(route.getCity1().getName()) && mapRoute.getCity2().getName().equals(route.getCity2().getName()))
                    {
                        Toast.makeText(getContext(), "You Cant Claim Parallel Routes", Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
