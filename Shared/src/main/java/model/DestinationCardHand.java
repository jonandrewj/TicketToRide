package model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by deriktj on 7/23/2017.
 */

public class DestinationCardHand
{
    private String username;
    private List<DestinationCard> hand;

    public DestinationCardHand(String username) {
        this.username = username;
        hand = new ArrayList<>();
    }

    public List<DestinationCard> getHand() {
        return hand;
    }

    public String getUsername() {
        return username;
    }

    public void addCardToHand(DestinationCard card) {
        hand.add(card);
    }

    public void addCardToHand(String city1, String city2, int points) {
        hand.add(new DestinationCard(city1,city2,points));
    }

    public void discardDestinationCardsFromHand(List<DestinationCard> cardsToDiscard) {
        for(DestinationCard cardToDiscard : cardsToDiscard) {
            discardDestinationCardFromHand(cardToDiscard);
        }
        for(DestinationCard card : hand) {
            card.setCanBeDiscarded(false);
        }
    }

    public void discardDestinationCardFromHand(DestinationCard cardToDiscard) {
        // Concurrent modification safe
        for(Iterator<DestinationCard> it = hand.iterator(); it.hasNext();) {
            DestinationCard card = it.next();
            if(card.equals(cardToDiscard)) {
                it.remove();
                continue;
            }
        }
    }
}
