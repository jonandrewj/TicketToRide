package model;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import command.ICommand;
import plugin.iDaoAPI;

/**
 * Created by deriktj on 7/9/2017.
 */

public class ServerModel //implements IModelFacade
{

    private GameList gameList;
    private Set<Account> registeredAccounts;
    private iDaoAPI databaseAPI;

    // A command queue for users
    private List<ICommand> commandListForUsers;


    public ServerModel()
    {
        gameList = new GameList();
        registeredAccounts = new HashSet<>();

        // A command queue for each user
        commandListForUsers = new ArrayList<>();
    }

    public GameList getGameList()
    {
        return gameList;
    }

    public void setGameList(List<Game> list) {
        gameList.setGameList(list);
    }

    public Set<Account> getRegisteredAccounts()
    {
        return registeredAccounts;
    }

    public void setResiteredAccounts(Set<Account> accounts) {
        this.registeredAccounts = accounts;
    }

    public List<ICommand> getCommandListForUsers()
    {
        return commandListForUsers;
    }

    public void setCommandListForUsers(List<ICommand> commands) {
        this.commandListForUsers = commands;
    }

    public void setDatabaseAPI(iDaoAPI databaseAPI) {
        this.databaseAPI = databaseAPI;
    }

    public iDaoAPI getDatabaseAPI() {
        return databaseAPI;
    }
}