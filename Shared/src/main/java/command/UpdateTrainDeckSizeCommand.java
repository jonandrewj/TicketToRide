package command;

/**
 * Created by deriktj on 8/5/2017.
 */

public class UpdateTrainDeckSizeCommand extends Command implements ILoggable
{
    private String gameID;
    private int size;

    public UpdateTrainDeckSizeCommand(String gameID, int size) {
        this.gameID = gameID;
        this.size = size;
    }

    @Override
    public Boolean execute() {
        getContext().updateDeckSize(gameID,size);
        return true;
    }

    @Override
    public String getGameId()
    {
        return gameID;
    }

    @Override
    public String getUsername()
    {
        return null;
    }

    @Override
    public String getLogMessage()
    {
        return "The deck was reshuffled! There are now " + size + " cards in the deck";
    }
}
