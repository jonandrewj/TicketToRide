package asynctask;

import android.os.AsyncTask;

import java.util.List;

import fragment.CardSelectionFragment;
import model.DestinationCard;
import model.Game;
import model.Player;
import response.CommandResponse;
import serverproxy.ServerFacade;
import model.data.ClientModel;
import response.ResponseBase;
import fragment.GameSelectFragment;

/**
 * Created by Alyx on 7/9/17.
 */

public class DiscardDestinationCardsTask extends AsyncTask<CardSelectionFragment, Integer, Boolean> {
    /** Register result, contains the result of the register attempt */
    private CommandResponse discardResponse;
    /** Access the model */
    private ClientModel model;

    /** Access the database */
    private ServerFacade facade;

    /** The fragment that called this task */
    private CardSelectionFragment caller;

    private String username;
    private String gameID;
    private List<DestinationCard> cardsToDiscard;

    public DiscardDestinationCardsTask(String username, String gameID, List<DestinationCard> cardsToDiscard) {
        facade = new ServerFacade();
        discardResponse = null;
        model = ClientModel.getInstance();

        this.cardsToDiscard = cardsToDiscard;
        this.username = username;
        this.gameID = gameID;
    }


    protected Boolean doInBackground(CardSelectionFragment... urls) {
        caller = urls[0];
        // Get info from the UI
        Game game = ClientModel.getInstance().getGameByID(gameID);
        Player currentPlayer = game.getPlayerByName(username);
        if (!currentPlayer.canAcceptDestinationCards(cardsToDiscard.size())) return false;

        discardResponse =  facade.discardDestinationCard(username,gameID,cardsToDiscard);

        if(discardResponse != null && !discardResponse.isHasError()){

            return true;

        }
        else {
            return false;
        }

    }

    protected void onProgressUpdate(Integer... progress) {
        // progressBar.setProgress(progress[0]);
    }

    protected void onPostExecute(Boolean success) {
        caller.printSuccessToast(success);

    }

}
