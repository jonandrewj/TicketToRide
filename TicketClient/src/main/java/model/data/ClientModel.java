package model.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Observable;

import command.DrawTrainCardCommand;
import command.GameChatCommand;
import command.ICommand;
import asynctask.Poller;
import command.StartTurnCommand;
import log.EventType;
import model.Color;
import command.ILoggable;
import log.HistoryMessage;
import model.DestinationCard;
import model.DestinationCardHand;
import model.Game;
import model.IModelFacade;
import model.MapRoute;
import model.Player;
import model.TrainCard;
import model.TrainCardDeck;
import model.TrainCardHand;
import model.User;
import model.facade.GameModelFacade;
import model.facade.PreGameModelFacade;
import log.GameHistory;

/**
 * Created by Alyx on 7/9/17.
 *
 * Has the majority of the data that the client needs to have.
 */

public class ClientModel extends Observable implements IModelFacade {

    // All Games
    private ArrayList<Game> games;
    private ArrayList<MapRoute> MapRoutes;

    public String currentTurnPlayer = "";

    /**
     * Contains all the logs for all active games in the client, stored by GameId.
     */
    private HashMap<String, GameHistory> gameLogs = new HashMap<>();

    // User that is logged in.
    private User user;
    private Poller poller;
    private int lastIndex;
    public String myJoinedGame = "";
    public boolean isInGame = false;

    public Poller getPoller(){
        if (poller == null){
            poller = new Poller();
        }
        return poller;
    }

    private static ClientModel _instance;

    /**
     * @return the instance of the client.
     */
    public static ClientModel getInstance() {
        if (_instance == null) {
            _instance = new ClientModel();
        }
        return _instance;
    }

    private ClientModel() {
        games = new ArrayList<>();
        user = User.getInstance();
        lastIndex = -1;
    }

    /**
     * Sets the current game the player belongs to.
     * @param gameID the id of the game the player is currently playing.
     */
    public void setJoinedGame(String gameID){
        myJoinedGame = gameID;
        setChanged();
        notifyObservers();
    }

    /**
     * @return the current logged in user.
     */
    public User getUser() {
        return user;
    }

    public ArrayList<Game> getGames() {
        return games;
    }

    /**
     * Simple wrapper method to notify observers.
     */
    public void doNotify(){
        setChanged();
        notifyObservers();
    }


    // PHASE ONE OVERRIDES -------------------------------------------------------------------------
    @Override
    public Boolean addGame(Game game)
    {
        System.out.println("creating game " + game.getGameID());
        game.buildTrainCardDeck(0,0);
        game.getTrainDeck().setDeckSize(110);
        games.add(game);
        GameHistory newHistory = new GameHistory();
        gameLogs.put(game.getGameID(), newHistory);
        setChanged();
        notifyObservers();
        return true;
    }

    @Override
    public Boolean addPlayerToGame(String username, String gameID)
    {
        System.out.println("adding " + username + " to " + gameID);
        Game currentGame = PreGameModelFacade.getGameByID(gameID);
        currentGame.addPlayer(username);
        setChanged();
        notifyObservers();
        //Presenter.gameListFragment.moveToPreGameView();
        return true;
    }

    @Override
    public Boolean startGame(String gameID)
    {
        Game currentGame = PreGameModelFacade.getGameByID(gameID);
        currentGame.setGameStarted(true);

        for (Player p : currentGame.getPlayers()) {
            if (user.getUsername().equals(p.getUsername())) {
                GameModelFacade.initializeGame(gameID);
                setChanged();
                PreGameModelFacade.givePlayersOrderAndColor(currentGame);
                break;
            }
        }

        return true;
    }

    /**
     * Applies a command. If the command is ILoggable, the command is sent to the GameHistory.
     * @param command the command to execute.
     * @return true if the command was a success, which we assume is always.
     */
    @Override
    public Boolean applyCommand(ICommand command)
    {
        command.setContext(this);
        command.execute();
        if (command instanceof ILoggable) {
            insertMessage((ILoggable)command);
        }
        setChanged();
        notifyObservers();
        return true;
    }

    public void setMapRoutes(List<MapRoute> mapRoutes) {
        MapRoutes = (ArrayList<MapRoute>) mapRoutes;
    }

    public ArrayList<MapRoute> getMapRoutes() {
        return MapRoutes;
    }

    @Override
    public Boolean drawTrainCard(String gameID, String username, int index, Color color, Color colorToReplace)
    {
        Game game = getGameByID(gameID);
        if(game == null) return false;

        // notice that the index is what determines the face-uppiness of a card.

        game.getTrainDeck().setDeckSize(game.getTrainDeck().getDeckSize() - 1);
        game.getTrainDeck().setCardAtIndex(index,new TrainCard(colorToReplace));

        if(username == null) {
            /* part of a slightly jank way to reshuffle the visible cards */
            return false;
        }
        TrainCardHand hand = game.getTrainCardHandForPlayer(username);
        hand.addCardToHand(color,1);
        game.getPlayerByName(username).drawCard(TrainCardDeck.DECK_INDEX != index, color);
        return true;
    }

    @Override
    public Boolean drawDestinationCard(String gameID, String username, String city1, String city2, Integer points)
    {
        Game game = getGameByID(gameID);
        if(game == null) return false;

        game.getDestinationCardDeck().setDeckSize(game.getDestinationCardDeck().getDeckSize() - 1);
        DestinationCardHand hand = game.getDestinationCardHandForPlayer(username);
        hand.addCardToHand(city1,city2,points);
        game.getPlayerByName(username).drawDestinationCards();

        return true;
    }

    @Override
    public Boolean discardDestinationCards(String gameID, String username, List<DestinationCard> cardsToDiscard)
    {
        Game game = getGameByID(gameID);
        if(game == null) return false;

        game.getDestinationCardDeck().discard(cardsToDiscard);
        game.getDestinationCardDeck().setDeckSize(game.getDestinationCardDeck().getDeckSize() + cardsToDiscard.size());
        DestinationCardHand hand = game.getDestinationCardHandForPlayer(username);
        hand.discardDestinationCardsFromHand(cardsToDiscard);
        game.getPlayerByName(username).acceptDestinationCards();
        return true;
    }

    public int getLastIndex()
    {
        return lastIndex;
    }

    public void setLastIndex(int lastIndex)
    {
        this.lastIndex = lastIndex;
    }

    @Override
    public Boolean addChatMessageForGame(String gameID, String message, String username) {
        return true;
    }

    @Override
    public Boolean claimRouteForGame(MapRoute route, String gameID, String username, Color colorToUse) {
        System.out.println("Claiming Route" + route.getCity1().getName() + " to " + route.getCity2().getName() +" to Game: " + gameID);
        Game game = ClientModel.getInstance().getGameByID(gameID);
        game.claimRoute(route, username, colorToUse);
        Player player = game.getPlayerByName(username);
        TrainCardHand hand = game.getTrainCardHandForPlayer(username);
        player.claimRoute(route,hand);
        setChanged();
        try {
            notifyObservers();
        } catch (Exception e){
            // Do nothing.
        }

        return true;
    }

    @Override
    public void setDestinationScoreForPlayer(String gameID, String username, int completeScore, int incompleteScore)
    {
        getGameByID(gameID).getPlayerByName(username).setScoreFromCompleteDestinationCards(completeScore);
        getGameByID(gameID).getPlayerByName(username).setScoreFromIncompleteDestinationCards(incompleteScore);
    }

    @Override
    public void setLongestRouteScoreForPlayer(String gameID, String username, int score)
    {
        getGameByID(gameID).getPlayerByName(username).setScoreFromLongestTrain(score);
    }

    @Override
    public void updateDeckSize(String gameID, int size)
    {
        getGameByID(gameID).getTrainDeck().setDeckSize(size);
    }

    /**
     * Starts the turn for the given player.
     * @param gameId the game pertaining to the player.
     * @param username the name of the player whose turn it is to start.
     * @return if the player's turn successfully started.
     */
    public Boolean startTurn(String gameId, String username) {
        Game g = getGameByID(gameId);
        if (g.getGamestate() == Game.Gamestate.SETUP) {
            changeGameState(gameId, Game.Gamestate.PLAY);
        }
        Player actor = g.getPlayerByName(username);
        if (myJoinedGame.equals(gameId)) {
            currentTurnPlayer = username;
        }
        return actor.startTurn();
    }

    @Override
    public Boolean changeGameState(String gameId, Game.Gamestate newState) {
        this.getGameByID(gameId).setGamestate(newState);
        if(newState.equals(Game.Gamestate.END)) {
            setChanged();
            notifyObservers(new Boolean(true));
        }
        return true;
    }

    /**
     * @return the UUID of the current joined game.
     */
    public String getMyJoinedGame() {
        return myJoinedGame;
    }

    /**
     * Retrieves the game history for the specified game.
     * @param gameId the id of the game to get the game history for.
     * @return the game history of the specified game.
     */
    public GameHistory getGameHistory(String gameId) {
        return this.gameLogs.get(gameId);
    }

    /**
     * Gives the specified game a new message to its log, so long as that game isn't null and the
     * game hasn't started yet.
     * @param loggable the command to put into the log.
     */
    private void insertMessage(ILoggable loggable) {
        String actor = loggable.getUsername();
        if (actor == null) {
            // Assume that if there is no owner, then this is caused by a "game event."
            actor = "Game event";
        }
        Date timestamp = loggable.getTimestamp();
        String message = loggable.getLogMessage();
        String gameId = loggable.getGameId();

        if (!checkAddable(loggable)) {
            return;
        }

        GameHistory history = gameLogs.get(gameId);

        EventType eventType;
        if (loggable instanceof GameChatCommand) {
            eventType = EventType.CHAT;
        }
        else if (loggable.getUsername() == null || loggable instanceof StartTurnCommand) {
            eventType = EventType.EVENT;
        }
        else {
            eventType = EventType.PLAYER_ACTION;
        }
        HistoryMessage historyMessage = new HistoryMessage(message, actor, timestamp, eventType);

        if (history != null) {
            history.addMessage(historyMessage);
        }
    }

    /**
     * gets the game with the given id.
     * @param gameID the id of the game to retrieve.
     * @return the game object.
     */
    public Game getGameByID(String gameID) {
        for(Game game : games) {
            if (game.getGameID().equals(gameID)) {
                return game;
            }
        }
        return null;
    }

    /**
     * Checks if an ILoggable message should be added to the game. The conditions are as follows:
     *  - the game isn't null
     *  - the loggable is not an instance of a drawcard command. If it is, the game must be started
     *    in order for it to be added.
     * @param loggable the loggable command.
     * @return true if the conditions are met.
     */
    private boolean checkAddable(ILoggable loggable) {
        Game current = getGameByID(loggable.getGameId());
        if (current == null) {
            return false;
        }
        // If it's not a DrawTrainCardCommand, return true; otherwise, check if game time started.
        return !(loggable instanceof DrawTrainCardCommand) || current.isGameStarted();
    }
}
