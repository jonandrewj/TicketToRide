package command;

/**
 * Created by Andrew Jacobson on 7/9/2017.
 */

public class JoinGameCommand extends Command implements ILoggable
{

    String gameID;
    String username;

    /**
     * Creates a command to add a user to the specified game.
     * @param gameID the id of the game to which to add the player.
     * @param username the name of the player to add to the game.
     */
    public JoinGameCommand(String gameID, String username) {
        super();
        this.gameID = gameID;
        this.username = username;
    }

    /**
     * Executes the command, adding the player to the game.
     */
    @Override
    public Boolean execute()
    {
        return getContext().addPlayerToGame(username,gameID);
    }

    @Override
    public String getGameId() {
        return gameID;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getLogMessage() {
        return "joined the game";
    }
}
