package states;

import model.Color;
import model.MapRoute;
import model.Player;
import model.TrainCardHand;

/**
 * Created by Joseph on 7/29/2017.
 *
 * Used to determine available actions during a turn.
 *
 * @invariant if the function returns true, the state will be changed.
 * @invariant if a method returns false, the state will not be changed.
 */
public interface ITurnState {
    /**
     * Checks if the player can accept/reject destination cards.
     * @param target the player to check against.
     * @return if the player can perform this action.
     */
    boolean acceptDestinationCards(Player target);

    /**
     * If the user receives this call while in the NotYourTurnState, the user's turn will begin.
     * @param target the player whose state we check.
     * @return true if the user's turn will begin.
     */
    boolean startTurn(Player target);

    /**
     * Draws a card from the given area.
     * @param target the player whose state we check.
     * @param isFaceUp if the card is a face-up card.
     * @param cardColor if the card is face up, the color. Ignored if isFaceUp is false.
     * @return true if the user can perform the action.
     */
    boolean drawCard(Player target, boolean isFaceUp, Color cardColor);

    /**
     * Checks if the user can draw a destination card.
     * @param target the player whose state we check.
     * @return true if the user can, otherwise, false.
     */
    boolean drawDestinationCards(Player target);

    /**
     * Checks if the user is able to claim a route.
     * @param target the player whose state we check.
     * @param route the route the player is attempting to claim.
     * @param hand the player's current hand.
     * @return true if the user is able to perform that action.
     */
    boolean claimRoute(Player target, MapRoute route, TrainCardHand hand);

    /**
     * Ends the game for the player.
     * @param target the player whose game is to end.
     */
    void endGame(Player target);
}
