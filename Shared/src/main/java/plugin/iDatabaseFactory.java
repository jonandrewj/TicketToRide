package plugin;

/**
 * Created by Andrew Jacobson on 8/5/2017.
 */

public interface iDatabaseFactory {
    public iDaoAPI getDatabase();
}
