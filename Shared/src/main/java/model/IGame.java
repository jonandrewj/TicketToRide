package model;

import java.util.UUID;

/**
 * Created by deriktj on 7/9/2017.
 */
public interface IGame
{
    void addPlayer(String playerToAdd);
    void setGameStarted(boolean gameStarted);
    void removePlayer(String playerToRemove);
    boolean isGameStarted();
    int getPlayerCount();
    int getPlayerMax();
    void setPlayerMax(int max);
    String getGameID();
}
