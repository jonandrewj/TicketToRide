package fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import model.data.ClientModel;
import asynctask.LoginTask;
import asynctask.RegisterTask;
import cs340.byu.tickettoride.R;

/**
 * Created by kendall on 7/6/2017.
 */


public class LoginFragment extends Fragment {

    // Buttons in the UI
    private EditText mUsernameField;
    private EditText mPasswordField;
    private Button loginButton;
    private Button registerButton;

    // Values from the fields
    private String usernameText;
    private String passwordText;

    private View loginView;


    public LoginFragment()
    {
        usernameText = "";
        passwordText = "";
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        
        loginView = inflater.inflate(R.layout.fragment_login, container, false);
        
        mUsernameField = (EditText) loginView.findViewById(R.id.username);
        mUsernameField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                usernameText = s.toString();
            }
        });

        mPasswordField = (EditText) loginView.findViewById(R.id.password);
        mPasswordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                passwordText = s.toString();
            }
        });

        registerButton = (Button) loginView.findViewById(R.id.registerButton);
        registerButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                register();

            }
        });

        loginButton  = (Button) loginView.findViewById(R.id.loginButton);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });


        return loginView;
    }

    public void register()
    {
        // Create task
        RegisterTask registerController = new RegisterTask();

        // Set fields
        registerController.setRegisterFields(usernameText, passwordText);

        // Execute
        registerController.execute(this);
    }

    public void login()
    {
        // Create Task
        LoginTask loginController = new LoginTask();

        // Set fields
        loginController.setLoginFields(usernameText, passwordText);

        // Execute
        loginController.execute(this);
    }

    @Override
    public void onDestroyView() {
        //mContainer.removeAllViews();
        ViewGroup mContainer = (ViewGroup) getActivity().findViewById(R.id.fragment_login);
        mContainer.removeAllViews();
        super.onDestroyView();
    }

    public void moveToGameSelectionScreen()
    {
        //Finsihes this activity and moves to game selection activity
        FragmentManager fragmentManager2 = getFragmentManager();
        FragmentTransaction fragmentTransaction2 = fragmentManager2.beginTransaction();
        GameSelectFragment fragment2 = new GameSelectFragment();
        //fragmentTransaction2.addToBackStack("xyz");
        //fragmentTransaction2.replace(android.R.id.content,fragment2, null);

        fragmentTransaction2.replace(((ViewGroup)(getView().getParent())).getId(), fragment2);
        fragmentTransaction2.addToBackStack(null);
        // fragmentTransaction2.add(android.R.id.content, fragment2);
        fragmentTransaction2.commit();
    }


    /**
     * Prints the entered message to the screen. If "success" is true,
     * it will assume the authentication was successful and move to
     * game selection screen.
     * @param message Message that will be printed to screen
     * @param success Authentication status (either success || !success)
     */
    public void authenticationUpdate(String message, boolean success){

        // Print toast
        Toast toast = Toast.makeText(this.getContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        // Move on if successful
        if(success) {
            ClientModel.getInstance().getPoller().startPoller();
            moveToGameSelectionScreen();
        }

    }
}

