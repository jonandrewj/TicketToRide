package activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

import cs340.byu.tickettoride.R;

/**
 * Created by kendall on 7/13/2017.
 */

public class Draw extends View {

    private Paint paint;
    View parent;


    Resources res = getContext().getResources();
    int id = R.drawable.woodone;

    private Bitmap backingBitmap = BitmapFactory.decodeResource(res, id);
    Canvas canvas = new Canvas(backingBitmap);
    public Draw(Context context, View view) {
        super(context);
        parent = (View)view.getParent();
        // create the Paint and set its color
        paint = new Paint();
        paint.setColor(Color.BLACK);
    }

    @Override
    protected void onDraw(Canvas canvas) {


    }

    public void drawCities(Canvas canvas)
    {
        canvas.drawCircle(320/280*parent.getWidth(), 180/250 * parent.getHeight(), 100, paint);
    }
}
