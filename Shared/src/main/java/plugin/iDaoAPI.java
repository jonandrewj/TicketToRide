package plugin;

import java.util.List;
import java.util.Set;

import command.ICommand;
import model.Account;
import model.Game;

/**
 * Created by Andrew Jacobson on 8/3/2017.
 */

public interface iDaoAPI {

    // GAME DAO

    /**
     * Gets the full game table, returns empty list if table is empty
     * @return a list containing games (or empty list if no games)
     */
    List<Game> getGames();

    /**
     * Sends a game state to be added to the game table - we don't use this.
     * @param game
     */
    void addGame(Game game);

    /**
     * Updates states of all the games. This also migrates the temporary commands to the
     * larger command queue table
     * @param game
     */
    void updateGames(List<Game> game);

    // GAME COMMAND DAO

    /**
     * Gets the list of temporary commands to be re-applied on the server.
     * @return list of commands; empty list if no commands
     */
    List<ICommand> getDeltaCommands();

    /**
     * Adds a command to the temporary command table
     * @param command
     */
    void addDeltaCommand(ICommand command);

    // USER ACCOUNT DAO

    /**
     * Gets all user accounts
     * @return set of user accounts, empty set if there are no accounts
     */
    Set<Account> getUserAccounts();

    /**
     * Adds a user account to the database
     * @param account
     */
    void addUserAccount(Account account); // ADDS A USER ACCOUNT

    // COMMAND QUEUE DAO
    List<ICommand> getCommandQueue(); // RETURNS THE FULL COMMAND QUEUE

    // GENERIC NON-DAO SPECIFIC COMMANDS
    void selfDestruct(); //CLEARS ALL THE TABLES


}
