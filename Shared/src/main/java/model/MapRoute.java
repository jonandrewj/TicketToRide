package model;

import model.Color;

/**
 * Created by kendall on 7/13/2017.
 */

public class MapRoute {

    private MapCity city1;
    private MapCity city2;
    private Boolean routeClaimed = false;
    private int routeLength;
    private Color routeColor;
    private int xPositionModifier;
    private int yPositionModifier;
    private String ownerID;
    private Color grayRouteTrainColor;

    public MapRoute(MapCity city1, MapCity city2, int routeLength, Color routeColor, int xPositionModifier, int yPositionModifier)
    {
        this.city1 = city1;
        this.city2 = city2;
        this.routeLength = routeLength;
        this.routeColor = routeColor;
        this.xPositionModifier = xPositionModifier;
        this.yPositionModifier = yPositionModifier;

    }

    public void setGrayRouteTrainColor(Color grayrouteTrainColor) {
        this.grayRouteTrainColor = grayrouteTrainColor;
    }

    public Color getGrayRouteTrainColor() {
        return grayRouteTrainColor;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public int getXPositionModifier() {
        return xPositionModifier;
    }

    public int getYPositionModifier() {
        return yPositionModifier;
    }

    public MapCity getCity1() {
        return city1;
    }

    public MapCity getCity2() {
        return city2;
    }

    public Boolean getRouteClaimed() {
        return routeClaimed;
    }

    public int getRouteLength() {
        return routeLength;
    }

    public void setRouteClaimed(Boolean routeClaimed) {
        this.routeClaimed = routeClaimed;
    }

    public Color getRouteColor() {
        return routeColor;
    }

    public int getRouteWorth() {
        switch (this.routeLength){
            case 1: return 1;
            case 2: return 2;
            case 3: return 4;
            case 4: return 7;
            case 5: return 10;
            case 6: return 15;
            default: return 0;
        }
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof MapRoute)) {
            return false;
        }
        MapRoute route = (MapRoute) o;
        if (!getCity1().getName().equals(route.getCity1().getName())) {
            return false;
        }
        if (!getCity2().getName().equals(route.getCity2().getName())) {
            return false;
        }
        if (getXPositionModifier() != route.getXPositionModifier()) {
            return false;
        }
        if (getYPositionModifier() != route.getYPositionModifier()) {
            return false;
        }
        return true;
    }
    
}

