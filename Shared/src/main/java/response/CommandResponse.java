package response;

/**
 * Created by deriktj on 7/13/2017.
 */


public class CommandResponse extends ResponseBase {

    /**
     * Creates a new response for a command.
     * @param hasError if the response contains an error.
     * @param errorMessage the error message, if hasError is set.
     */
    public CommandResponse(boolean hasError, String errorMessage) {
        super(hasError, errorMessage);
    }

}
