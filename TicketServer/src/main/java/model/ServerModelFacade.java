package model;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import command.ChangeGameStateCommand;
import command.ClaimRouteCommand;
import command.CreateGameCommand;
import command.DiscardDestinationCardCommand;
import command.DrawDestinationCardCommand;
import command.DrawTrainCardCommand;
import command.GameChatCommand;
import command.ICommand;
import command.JoinGameCommand;
import command.StartGameCommand;
import command.UpdateDestinationScoreCommand;
import command.UpdateLongestRouteScoreCommand;
import command.UpdateTrainDeckSizeCommand;
import model.algorithm.LongestPathAsyncTask;
import model.algorithm.PointsForDestinationCompletionTask;
import command.StartTurnCommand;
import plugin.iDaoAPI;


/**
 * Created by Andrew Jacobson on 7/17/2017.
 */

public class ServerModelFacade implements IModelFacade {


    private ServerModel model = new ServerModel();

    private ServerPersistence persistence = new ServerPersistence();

    private static ServerModelFacade singleton = new ServerModelFacade();

    public static ServerModelFacade getInstance() {
        return singleton;
    }

    private ServerModelFacade() {

    }

    public void getDatabaseData(){
        // TODO GET DATA FROM DATABASE
        ServerPersistence.getData();

        newAccount("derik","jensen",new AuthToken());
        newAccount("test","test",new AuthToken());
    }

    public void setDatabaseAPI(iDaoAPI api){
        model.setDatabaseAPI(api);
    }
    public iDaoAPI getDatabaseAPI(){
        return model.getDatabaseAPI();
    }

    public void initializeGame(String gameID){
        Game game = model.getGameList().getGameByID(gameID);
        //game.routes = new ArrayList<>();
        game.getDestinationCardDeck().initializeDeck();
        executeInitialCommands(gameID);
        givePlayersOrderAndColor(game);
    }


    public void executeInitialCommands(String gameID){
        Game game = model.getGameList().getGameByID(gameID);
        int i = 0;
        for (Player p : game.getPlayers()){
            int start = i;
            int end = i + 4;
            for (int j = start; j < end; j++){
                // Add a command to the return for that train card to be claimed
                int temp = j % 5;
                drawInitialTrainCard(game.getGameID(),p.getUsername(),temp);
            }
            i = end;

            for (int j = 0; j < 3; j++){
                drawInitialDestinationCard(game.getGameID(),p.getUsername(),null,null,null);
            }
        }
    }

    public void givePlayersOrderAndColor(Game game){
        List<Player> players = game.getPlayers();
        switch (players.size()){
            case 2:
                players.get(0).setOrder(1);
                players.get(0).setColor(Color.GREEN);
                players.get(1).setOrder(0);
                players.get(1).setColor(Color.BLACK);
                break;
            case 3:
                players.get(0).setOrder(1);
                players.get(0).setColor(Color.YELLOW);
                players.get(1).setOrder(0);
                players.get(1).setColor(Color.WHITE);
                players.get(2).setOrder(2);
                players.get(2).setColor(Color.RED);
                break;
            case 4:
                players.get(0).setOrder(3);
                players.get(0).setColor(Color.GREEN);
                players.get(1).setOrder(0);
                players.get(1).setColor(Color.BLACK);
                players.get(2).setOrder(2);
                players.get(2).setColor(Color.RED);
                players.get(3).setOrder(1);
                players.get(3).setColor(Color.BLUE);
                break;
            case 5:
                players.get(0).setOrder(1);
                players.get(0).setColor(Color.GREEN);
                players.get(1).setOrder(4);
                players.get(1).setColor(Color.BLACK);
                players.get(2).setOrder(2);
                players.get(2).setColor(Color.RED);
                players.get(3).setOrder(3);
                players.get(3).setColor(Color.BLUE);
                players.get(4).setOrder(0);
                players.get(4).setColor(Color.YELLOW);
                break;
        }
    }

    private void drawInitialTrainCard(String gameID, String username, int index) {
        Game game = model.getGameList().getGameByID(gameID);

        TrainCardDeck deck = game.getTrainDeck();
        TrainCardHand hand = game.getTrainCardHandForPlayer(username);
        Player actor = game.getPlayerByName(username);
        TrainCard cardDrawn = deck.draw(index);
        Color color = cardDrawn.getColor();

        actor.drawCard(TrainCardDeck.DECK_INDEX != index, color);
        hand.addCardToHand(color,1);
        TrainCard cardToReplace = deck.getCardAtIndex(index);
        Color colorToReplace = cardToReplace.getColor();
        DrawTrainCardCommand drawTrainCardCommand = new DrawTrainCardCommand(username,gameID,index);
        drawTrainCardCommand.setColor(color);
        drawTrainCardCommand.setToReplace(colorToReplace);
        addCommandToList(drawTrainCardCommand);
        endTurnStuff(game, actor);
        handleDeckRules(gameID);
    }

    @Override
    public Boolean drawTrainCard(String gameID, String username, int index, Color color, Color colorToReplace)
    {
        Game game = model.getGameList().getGameByID(gameID);

        TrainCardDeck deck = game.getTrainDeck();
        TrainCardHand hand = game.getTrainCardHandForPlayer(username);
        Player actor = game.getPlayerByName(username);
        color = deck.getCardAtIndex(index).getColor();

        if (!actor.canDrawCard(TrainCardDeck.DECK_INDEX != index, color)){ return false; }
        actor.drawCard(TrainCardDeck.DECK_INDEX != index, color);
        TrainCard cardDrawn = deck.draw(index);
        color = cardDrawn.getColor();
        hand.addCardToHand(color,1);
        TrainCard cardToReplace = deck.getCardAtIndex(index);
        colorToReplace = cardToReplace.getColor();
        DrawTrainCardCommand drawTrainCardCommand = new DrawTrainCardCommand(username,gameID,index);
        drawTrainCardCommand.setColor(color);
        drawTrainCardCommand.setToReplace(colorToReplace);
        addCommandToList(drawTrainCardCommand);

        if(deck.shouldShuffle()) {
            deck.shuffleDeck();
            UpdateTrainDeckSizeCommand updateTrainDeckSizeCommand = new UpdateTrainDeckSizeCommand(gameID,deck.getDeck().size());
            addCommandToList(updateTrainDeckSizeCommand);
        }

        endTurnStuff(game, actor);
        handleDeckRules(gameID);
        return true;
    }

    private void handleDeckRules(String gameID) {
        Game game = model.getGameList().getGameByID(gameID);
        TrainCardDeck deck = game.getTrainDeck();
        List<TrainCard> rawDeck = deck.getDeck();
        int wildCount = 0;
        for(int i = 0; i < 5; i++){
            if(deck.getCardAtIndex(i).getColor().equals(Color.WILD)) {
                wildCount++;
            }
        }
        if(wildCount < 3) {
            return;
        }
        int nonWildCount = 0;
        for(int i = 0; i < rawDeck.size(); i++){
            if(!rawDeck.get(i).getColor().equals(Color.WILD)) {
                nonWildCount++;
            }
        }
        if(nonWildCount >= 3) {
            /* reshuffle visible cards */
            for(int i = 0; i < 5; i++){
                TrainCard cardDrawn = deck.draw(i);
                Color color = cardDrawn.getColor();
                deck.addToDiscard(color,1);
                TrainCard cardToReplace = deck.getCardAtIndex(i);
                Color colorToReplace = cardToReplace.getColor();
                DrawTrainCardCommand drawTrainCardCommand = new DrawTrainCardCommand(null,gameID,i);
                drawTrainCardCommand.setColor(color);
                drawTrainCardCommand.setToReplace(colorToReplace);
                addCommandToList(drawTrainCardCommand);
            }
        }
        else {
            return;
        }
        /* check again */
        handleDeckRules(gameID);
    }

    private void drawInitialDestinationCard(String gameID, String username, String city1, String city2, Integer points) {
        Game game = model.getGameList().getGameByID(gameID);

        Player actor = game.getPlayerByName(username);

        actor.drawDestinationCards();
        DestinationCardDeck deck = game.getDestinationCardDeck();
        DestinationCard cardDrawn = deck.draw();
        DestinationCardHand hand = game.getDestinationCardHandForPlayer(username);
        hand.addCardToHand(cardDrawn);
        DrawDestinationCardCommand drawDestinationCardCommand = new DrawDestinationCardCommand(username,gameID);
        drawDestinationCardCommand.setCity1(cardDrawn.getCity1());
        drawDestinationCardCommand.setCity2(cardDrawn.getCity2());
        drawDestinationCardCommand.setPoints(cardDrawn.getPoints());
        addCommandToList(drawDestinationCardCommand);
        endTurnStuff(game, actor);
    }

    @Override
    public Boolean drawDestinationCard(String gameID, String username, String city1, String city2, Integer points)
    {
        Game game = model.getGameList().getGameByID(gameID);

        Player actor = game.getPlayerByName(username);
        if (!actor.canDrawDestinationCards() && game.getGamestate() != Game.Gamestate.SETUP) {
            return false;
        }

        actor.drawDestinationCards();
        DestinationCardDeck deck = game.getDestinationCardDeck();
        DestinationCard cardDrawn = deck.draw();
        DestinationCardHand hand = game.getDestinationCardHandForPlayer(username);
        hand.addCardToHand(cardDrawn);
        DrawDestinationCardCommand drawDestinationCardCommand = new DrawDestinationCardCommand(username,gameID);
        drawDestinationCardCommand.setCity1(cardDrawn.getCity1());
        drawDestinationCardCommand.setCity2(cardDrawn.getCity2());
        drawDestinationCardCommand.setPoints(cardDrawn.getPoints());
        addCommandToList(drawDestinationCardCommand);
        endTurnStuff(game, actor);
        return true;
    }

    @Override
    public Boolean discardDestinationCards(String gameID, String username, List<DestinationCard> cardsToDiscard)
    {
        Game game = model.getGameList().getGameByID(gameID);
        Player actor = game.getPlayerByName(username);
        if (!actor.canAcceptDestinationCards(cardsToDiscard.size())) {
            return false;
        }
        actor.acceptDestinationCards();
        DestinationCardHand hand = game.getDestinationCardHandForPlayer(username);
        hand.discardDestinationCardsFromHand(cardsToDiscard);
        DiscardDestinationCardCommand discardDestinationCardCommand = new DiscardDestinationCardCommand(username,gameID,cardsToDiscard);
        addCommandToList(discardDestinationCardCommand);

        if (haveAllAcceptedDestinations(game) && !game.haveTurnsStarted()) {
            Player one = game.findPlayerByTurnOrder(0); //start the first player's turn
            one.startTurn();
            StartTurnCommand startTurnCommand = new StartTurnCommand();
            startTurnCommand.setUsername(one.getUsername());
            startTurnCommand.setGameId(gameID);
            addCommandToList(startTurnCommand);
            game.setGamestate(Game.Gamestate.PLAY);
            game.setTurnsStarted(true);
        }
        else if(game.haveTurnsStarted()){
            endTurnStuff(game, actor);
        }

        return true;
    }

    private boolean haveAllAcceptedDestinations(Game game) {
        List<Player> players = game.getPlayers();
        for (Player player : players) {
            if (!player.initialCardsDiscarded()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Starts the turn for the given player.
     * @param gameID the game pertaining to the player.
     * @param username the name of the player whose turn it is to start.
     * @return if the player's turn successfully started.
     */
    public Boolean startTurn(String gameID, String username) {
        Game g = model.getGameList().getGameByID(gameID);
        Player actor = g.getPlayerByName(username);
        addCommandToList(new StartTurnCommand(gameID,username));
        return actor.startTurn();
    }

    @Override
    public Boolean changeGameState(String gameId, Game.Gamestate newState) {
        model.getGameList().getGameByID(gameId).setGamestate(newState);
        return true;
    }

    /**
     * @param username the user to get the command list for
     * @return returns the command list for the user
     */
    public List<ICommand> getCommandListForUser(String username) {
        //TODO: Filter by which commands are relevant to each user?
        List<ICommand> cq = model.getCommandListForUsers();
        return cq;
    }

    /**
     * Adds the specified game to the server.
     * @param game the game to add.
     */
    @Override
    public Boolean addGame(Game game)
    {
        game.buildTrainCardDeck(14,12);
        model.getGameList().add(game);
        CreateGameCommand createGameCommand = new CreateGameCommand(game.getPlayerMax(),game.getGameID());
        addCommandToList(createGameCommand);
        System.out.println("There are now " + model.getGameList().getSize());
        return true;
    }

    /**
     * Adds the specified player (by username) to the given game (by game id).
     * @param username the username of the player to add to the game.
     * @param gameID the id of the game to join the player.
     */
    @Override
    public Boolean addPlayerToGame(String username, String gameID)
    {
        Game game = model.getGameList().getGameByID(gameID);

        if (game.isGameStarted() || game.getPlayerByName(username) != null) {
            return false;
        }
        game.addPlayer(username);
        JoinGameCommand joinGameCommand = new JoinGameCommand(gameID,username);
        addCommandToList(joinGameCommand);
        if(game.getPlayerCount() == game.getPlayerMax()) {
            startGame(gameID);
        }

        return true;

    }

    /**
     * Starts the game given by the game id.
     * @param gameID the id of the game to start.
     */
    @Override
    public Boolean startGame(String gameID)
    {
        model.getGameList().getGameByID(gameID).setGameStarted(true);
        initializeGame(gameID); // THIS IS THE FUNCTION TO CODE

        StartGameCommand startGameCommand = new StartGameCommand(gameID);
        addCommandToList(startGameCommand);
        return true;
    }

    /**
     * Runs the given command.
     * @param command the command to run.
     */
    @Override
    public Boolean applyCommand(ICommand command)
    {
        command.setContext(this);
        Boolean result = command.execute();

        // ANDREW USED TO TEST THE MONGO DB
        //model.getDatabaseAPI().updateGames(model.getGameList().getGames());
        return result;
    }

    @Override
    public Boolean addChatMessageForGame(String gameID, String message, String username) {
        GameChatCommand gameChatCommand = new GameChatCommand(username, message, gameID);
        this.addCommandToList(gameChatCommand);
        return true;
    }

    @Override
    public Boolean claimRouteForGame(MapRoute route, String gameID, String username, Color colorToUse)
    {
        Game game = model.getGameList().getGameByID(gameID);
        Player p = game.getPlayerByName(username);
        TrainCardHand tch = game.getTrainCardHandForPlayer(username);

        if(p.canClaimRoute(route,tch)) {
            game.claimRoute(route, username, colorToUse);
            if(game.getTrainDeck().shouldShuffle()) {
                game.getTrainDeck().shuffleDeck();
                UpdateTrainDeckSizeCommand updateTrainDeckSizeCommand = new UpdateTrainDeckSizeCommand(gameID,game.getTrainDeck().getDeck().size());
                addCommandToList(updateTrainDeckSizeCommand);

            }
            p.claimRoute(route, tch);
            addCommandToList(new ClaimRouteCommand(route, username, gameID, colorToUse));
            endTurnStuff(game, p);
            return true;
        }
        return false;
    }

    @Override
    public void setDestinationScoreForPlayer(String gameID, String username, int completeScore, int incompleteScore)
    {
        model.getGameList().getGameByID(gameID).getPlayerByName(username).setScoreFromCompleteDestinationCards(completeScore);
        model.getGameList().getGameByID(gameID).getPlayerByName(username).setScoreFromIncompleteDestinationCards(completeScore);
        addCommandToList(new UpdateDestinationScoreCommand(gameID,username,completeScore,incompleteScore));
    }

    @Override
    public void setLongestRouteScoreForPlayer(String gameID, String username, int score)
    {
        model.getGameList().getGameByID(gameID).getPlayerByName(username).setScoreFromLongestTrain(score);
    }

    @Override
    public void updateDeckSize(String gameID, int size)
    {
        model.getGameList().getGameByID(gameID).getTrainDeck().setDeckSize(size);
    }

    /**
     * Adds a command for each player to execute.
     * @param command The command to push to each player.
     */
    private void addCommandToList(ICommand command) {
        model.getCommandListForUsers().add(command);
        ServerPersistence.commandApplied(command);

    }

    public List<ICommand> getCommandList() {
        return model.getCommandListForUsers();
    }

    /**
     * Gets the account with the associated username.
     * @param username The username of the account to get.
     * @return the account belonging to the user.
     */
    public Account getAccount(String username) {
        for (Account account : model.getRegisteredAccounts()) {
            if(account.getUsername().equals(username)){
                return account;
            }
        }
        return null;
    }

    /**
     * Creates a new account with the given data.
     * @param username the username
     * @param password the user's password
     * @param authToken an authorization token given by the client.
     */
    public void newAccount(String username, String password, AuthToken authToken) {
        Account existingPlayer = getAccount(username);

        if (existingPlayer != null) {
            return;
        }
        Account newAccount = new Account(username,password,authToken);
        model.getRegisteredAccounts().add(newAccount);
        ServerPersistence.addUserAccount(newAccount);
    }

    public void calculateEndGamePointsForGame(Game game) {
        LongestPathAsyncTask longestPathTask = new LongestPathAsyncTask(game);
        longestPathTask.execute();
        PointsForDestinationCompletionTask pointsForDestinationCompletionTask = new PointsForDestinationCompletionTask(game);
        pointsForDestinationCompletionTask.execute();
    }

    public void sendDestinationScoreData(String gameID) {
        Game game = model.getGameList().getGameByID(gameID);
        for(Player player : game.getPlayers()) {
            UpdateDestinationScoreCommand command = new UpdateDestinationScoreCommand(gameID,player.getUsername(),player.getScoreFromCompleteDestinationCards(),player.getScoreFromIncompleteDestinationCards());
            addCommandToList(command);
        }
    }

    public void sendLongestRouteScoreData(String gameID) {
        Game game = model.getGameList().getGameByID(gameID);
        for(Player player : game.getPlayers()) {
            UpdateLongestRouteScoreCommand command = new UpdateLongestRouteScoreCommand(gameID,player.getUsername(),player.getScoreFromLongestTrain());
            addCommandToList(command);
        }
    }

    /**
     * A method to be called at the end of each potentially turn-ending action.
     *
     * This method helps to determine if the game should enter its end game state and/or the final
     * turn state. If the player's turn hasn't ended, then this will stop execution. Otherwise, it
     * will check if the player still has more than 2 train pieces; once this has occurred, it will
     * check if the player has played their last turn for this game. If they have, then the game
     * will have its state modified to the end game state.
     *
     * @param game the game the action was applied to.
     * @param actor the user applying the action.
     */
    private void endTurnStuff(Game game, Player actor) {
        if(game.getGamestate() == Game.Gamestate.SETUP) {
            Boolean done = true;
            for(Player player : game.getPlayers()) {
                done = done && player.initialCardsDiscarded();
            }
            if(done) {
                game.setGamestate(Game.Gamestate.PLAY);
            }
            return;
        }
        if (actor.isPlayerTurn()) {
            return;
        }

        if (game.getGamestate() == Game.Gamestate.LAST_TURN) {
            actor.setLastTurnTaken(true);
        }

        final int MAXIMUM_TRAINS_REMAINING = 2;
        if (actor.getTrainPiecesLeft() <= MAXIMUM_TRAINS_REMAINING && game.getGamestate() != Game.Gamestate.LAST_TURN) {
            game.setGamestate(Game.Gamestate.LAST_TURN);
            ChangeGameStateCommand cgsc = new ChangeGameStateCommand(game.getGameID(), Game.Gamestate.LAST_TURN);
            addCommandToList(cgsc);
        }

        setNextPlayerOrEndGame(game, actor);
    }

    /**
     * Gets the next player. If that player has already taken their last turn, the game will end.
     */
    private void setNextPlayerOrEndGame(Game game, Player actor) {
        // This will loop around to the next player.
        int nextTurnOrder = (actor.getOrder() + 1) % game.getPlayerMax();
        Player next = game.findPlayerByTurnOrder(nextTurnOrder);
        ICommand command;
        if (next.wasLastTurnTaken()) {
            game.setGamestate(Game.Gamestate.END);
            calculateEndGamePointsForGame(game);
            command = new ChangeGameStateCommand(game.getGameID(), Game.Gamestate.END);
        }
        else {
            next.startTurn();
            command = new StartTurnCommand(game.getGameID(), next.getUsername());
        }
        addCommandToList(command);
    }

    public void setAccounts(Set<Account> accounts) {
        model.setResiteredAccounts(accounts);
    }

    public void setGames(List<Game> games) {
        model.setGameList(games);
    }

    public void setCommandList(List<ICommand> commands) {
        model.setCommandListForUsers(commands);
    }

    public GameList getGameList() {
        return model.getGameList();
    }
}
