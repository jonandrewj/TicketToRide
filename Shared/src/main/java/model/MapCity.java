package model;

/**
 * Created by kendall on 7/12/2017.
 */

public class MapCity {
    private float x;
    private float y;
    private String name;
    public String idTag = null;
    //public static List<MapCity> listOfCities;

    // CONSTRUCTORS --------------------------------------------------------------------------------
    public MapCity(String name, float x, float y)
    {
        this.y = y;
        this.x = x;
        this.name =name;
    }
    public MapCity(String name, String tag, float x, float y)
    {
        this.y = y;
        this.x = x;
        this.name =name;
        this.idTag = tag;
    }
    public MapCity() {

    }

    // GETTERS AND SETTERS -------------------------------------------------------------------------
    public String getIdTag() {
        return idTag;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public String getName() {
        return name;
    }

}
