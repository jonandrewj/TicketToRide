package fragment.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cs340.byu.tickettoride.R;
import model.data.ClientModel;
import model.facade.PreGameModelFacade;

/**
 * Created by kendall on 7/7/2017.
 */

public class GameListAdapter extends BaseAdapter {

    private Context context;
    public List<GameData> games;

    public class GameData {
        public String gameID;
        public int numPlayers;
        public int playerMax;

        public GameData(String gameID, int numPlayers, int playerMax) {
            this.gameID = gameID;
            this.numPlayers = numPlayers;
            this.playerMax = playerMax;
        }
    }

    public void addGame(String gameId, int numPlayers, int maxPlayers){
        games.add(new GameData(gameId, numPlayers, maxPlayers));
    }

    public GameListAdapter(Context context) {
        this.context = context;
        games = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return games.size();
    }

    @Override
    public Object getItem(int position) {
        return games.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gameView = inflater.inflate(R.layout.list_game, parent, false);

        TextView gameText = (TextView)gameView.findViewById(R.id.gameID);
        TextView descriptionText = (TextView)gameView.findViewById(R.id.gameDescription);
        ImageView icon = (ImageView)gameView.findViewById(R.id.gameIcon);

        GameData game = games.get(position);
        gameText.setText("Game: " + game.gameID);
        if (game.numPlayers < game.playerMax) {
            descriptionText.setText("Players : " + game.numPlayers + "/" + game.playerMax);
        }
        else {
            descriptionText.setText("FULL GAME : " + game.numPlayers + "/" + game.playerMax);
        }
        String username = ClientModel.getInstance().getUser().getUsername();
        if (PreGameModelFacade.isUserInGame(username, game.gameID)){
            descriptionText.setText("Players : " + game.numPlayers + "/" + game.playerMax + "  --Press to Rejoin");
        }
        return gameView;
    }



}
