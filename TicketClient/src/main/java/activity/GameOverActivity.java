package activity;

import android.content.pm.ActivityInfo;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;
import java.util.TreeMap;

import cs340.byu.tickettoride.R;
import fragment.adapter.ScoreboardAdapter;
import model.Player;
import model.data.ClientModel;

/**
 * Created by Alyx on 7/31/17.
 */

public class GameOverActivity extends AppCompatActivity implements Observer{

    private View gameOverView;
    private ListView playerScores;
    private ClientModel model;
    private int highScore;
    private Player winner;
    private List<Player> ties;
    private String winnerName;

    // For recycler view
    private Map<String, List<String>> mExpandableItemsMap;
    private ScoreboardAdapter expandableListAdapter;


    public GameOverActivity(){
        model = ClientModel.getInstance();
        ties = new ArrayList<>();
        winnerName = "";
        highScore = Integer.MIN_VALUE;
        winner = null;
        // Initialize fields for recyclerView
        mExpandableItemsMap = new TreeMap<>();
        expandableListAdapter = null;


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        displayScoreboard();
    }


    /**
     * Shows the user the list of games.
     */
    public void displayScoreboard(){
        playerScores = (ListView) findViewById(R.id.statsList);
        expandableListAdapter = new ScoreboardAdapter(this);
        List<Player> players = model.getGameByID(model.getMyJoinedGame()).getPlayers();
        determineWinner(players);

        TextView winnerDisplay = (TextView) findViewById(R.id.winnerName);

        if(ties.size() > 0){
            setWinner(ties);
        } else {
            setWinner(winner);
        }

        winnerDisplay.setText(winnerName);

        expandableListAdapter.setScoreboardData(players);
        playerScores.setAdapter(expandableListAdapter);
    }

    private void determineWinner(List<Player> players){
        for(Player player : players){
            if(player.getScore() > highScore){
                // Clear current winners
                ties.clear();

                // Set new winner
                winner = player;

                // Update high score
                highScore = player.getScore();

            } else if (player.getScore() == highScore){
                if(winner != null) {
                    // Add previous winner
                    ties.add(winner);
                }

                // Add the tie in there (2nd winner)
                ties.add(player);
            }
        }
    }

    public void setWinner(Player winner) {
        this.winnerName = winner.getUsername(); // HERE
    }

    public void setWinner(List<Player> ties) {
        this.winnerName = "";
        for(Player tie : ties){
            if(winnerName.length() > 0) {
                winnerName = winnerName + " and " + tie.getUsername();
            } else {
                winnerName = tie.getUsername();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.gameOverView = findViewById(android.R.id.content);
        ClientModel.getInstance().addObserver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ClientModel.getInstance().deleteObserver(this);
    }

    /**
     * Disables the back button.
     */
    @Override
    public void onBackPressed() {
        //TODO: Leave game?
    }

    /**
     * Observer method. On update, executes this.
     * @param o
     * @param arg
     */
    @Override
    public void update(Observable o, Object arg) {
        displayScoreboard();
    }
}
