package activity;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.View;

/**
 * Created by kendall on 7/15/2017.
 */

public class GamePlaySingleton {


    public static GamePlaySingleton gamePlaySingleton = null;

    private Boolean wasRouteSelected = false;
    private Canvas canvas;
    private float screenWidth;
    private float screenHeight;
    private int playerColor = Color.BLUE;
    private int routeToClaimInt;
    private View view;
    private Context gameplayContext;
    private GamePlaySingleton()
    {

    }

    public static GamePlaySingleton Inst()
    {
        if (gamePlaySingleton == null)
        {
            gamePlaySingleton = new GamePlaySingleton();

        }
        return gamePlaySingleton;
    }

    public Boolean getWasRouteSelected() {
        return wasRouteSelected;
    }

    public void setWasRouteSelected(Boolean wasRouteSelected) {
        this.wasRouteSelected = wasRouteSelected;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public void setScreenHeight(float screenHeight) {
        this.screenHeight = screenHeight;
    }

    public void setScreenWidth(float screenWidth) {
        this.screenWidth = screenWidth;
    }

    public float getScreenHeight() {
        return screenHeight;
    }

    public float getScreenWidth() {
        return screenWidth;
    }

    public void setPlayerColor(int playerColor) {
        this.playerColor = playerColor;
    }

    public int getPlayerColor() {
        return playerColor;
    }

    public void setRouteToClaimInt(int routeToClaimInt) {
        this.routeToClaimInt = routeToClaimInt;
    }

    public int getRouteToClaimInt() {
        return routeToClaimInt;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public Context getGameplayContext() {
        return gameplayContext;
    }

    public void setGameplayContext(Context gameplayContext) {
        this.gameplayContext = gameplayContext;
    }
}
