package model.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.SwingWorker;

import model.DestinationCard;
import model.DestinationCardHand;
import model.Game;
import model.MapRoute;
import model.Player;
import model.ServerModel;
import model.ServerModelFacade;

/**
 * Created by deriktj on 8/1/2017.
 */

public class PointsForDestinationCompletionTask extends SwingWorker<Boolean, Integer>
{
    private List<DestinationCardHand> destinationCardHands;
    private Map<String,Graph> graphs;
    private Game game;

    Map<String,Integer> playerCompleteDestinationPoints;
    Map<String,Integer> playerIncompleteDestinationPoints;

    public PointsForDestinationCompletionTask() {
        graphs = new HashMap<>();
        destinationCardHands = new ArrayList<>();
        playerCompleteDestinationPoints = new HashMap<>();
        playerIncompleteDestinationPoints = new HashMap<>();
    }

    public PointsForDestinationCompletionTask(Game game) {
        this();
        this.game = game;
        List<MapRoute> routes = game.getMapRoutes();
        List<DestinationCardHand> hands = game.getDestinationCardHands();
        destinationCardHands = hands;
        for(Player player : game.getPlayers()) {
            graphs.put(player.getUsername(),new Graph());
        }
        for(MapRoute route : routes) {
            String username = route.getOwnerID();
            if(username == null) { continue; }
            graphs.get(username).addNode(route.getCity1().getName());
            graphs.get(username).addNode(route.getCity2().getName());
            graphs.get(username).addEdge(route.getCity1().getName(),route.getCity2().getName(),route.getRouteLength());
        }

    }

    @Override
    protected Boolean doInBackground() throws Exception
    {
        for (Map.Entry<String,Graph> pair : graphs.entrySet())
        {
            playerCompleteDestinationPoints.put(pair.getKey(),0);
            playerIncompleteDestinationPoints.put(pair.getKey(),0);
            Graph bigGraph = pair.getValue();

            List<Graph> subGraphs = bigGraph.splitOnConnectivity();
            DestinationCardHand playerHand = null;
            for(DestinationCardHand hand : destinationCardHands) {
                if(hand.getUsername().equals(pair.getKey())) {
                    playerHand = hand;
                    break;
                }
            }
            if (playerHand == null) {
                throw new NullPointerException();
            }
            for(DestinationCard card : playerHand.getHand()) {
                Boolean destinationComplete = false;
                for(Graph graph : subGraphs) {
                    if(graph.containsBoth(card.getCity1(),card.getCity2())) {
                        destinationComplete = true;
                        break;
                    }
                }
                if(destinationComplete) {
                    //award points
                    int previousPoints = playerCompleteDestinationPoints.get(pair.getKey());
                    playerCompleteDestinationPoints.put(pair.getKey(),previousPoints + card.getPoints());
                }
                else {
                    //remove points
                    int previousPoints = playerIncompleteDestinationPoints.get(pair.getKey());
                    playerIncompleteDestinationPoints.put(pair.getKey(),previousPoints - card.getPoints());
                }
            }

        }
        return true;
    }

    @Override
    protected void done() {
        for(Map.Entry<String,Integer> playerPoints : playerCompleteDestinationPoints.entrySet()) {
            game.getPlayerByName(playerPoints.getKey()).setScoreFromCompleteDestinationCards(playerPoints.getValue());
        }
        for(Map.Entry<String,Integer> playerPoints : playerIncompleteDestinationPoints.entrySet()) {
            game.getPlayerByName(playerPoints.getKey()).setScoreFromIncompleteDestinationCards(playerPoints.getValue());
        }
        ServerModelFacade.getInstance().sendDestinationScoreData(game.getGameID());
    }
}
