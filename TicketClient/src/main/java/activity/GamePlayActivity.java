package activity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

import cs340.byu.tickettoride.R;
import fragment.GrayRouteFragment;
import model.MapCity;
import model.MapRoute;
import model.data.ClientModel;
import model.data.GameMap;
import model.facade.PreGameModelFacade;

public class GamePlayActivity extends AppCompatActivity implements Observer {

    Canvas mainCanvas;
    View view;
    float screenWidth;
    float screenHeight;
    GameMap gameMap;
    GamePlaySingleton gamePlaySingleton = GamePlaySingleton.Inst();
    private View mainMapView;

    @Override
    protected void onStart() {
        super.onStart();
        drawRoutes(mainCanvas);
        this.mainMapView = findViewById(android.R.id.content);
        ClientModel.getInstance().addObserver(this);
        update(new Observable(), null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        ClientModel.getInstance().deleteObserver(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ClientModel.getInstance().deleteObserver(this);
    }

    public GamePlayActivity(){
        gamePlaySingleton.setGameplayContext(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_play);
        gamePlaySingleton.setView(findViewById(android.R.id.content));


        gameMap = GameMap.getMap();
        ClientModel.getInstance().setMapRoutes(gameMap.mapRoutes);
        view = this.findViewById(android.R.id.content);
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                float tempX = event.getX();
                float tempy = event.getY();
                return true;
            }
        });
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        view.setOnTouchListener(new OnSwipeListener(getApplicationContext()){
            @Override
            public void onSwipeRight(){
                Intent gameInfo = new Intent(getApplicationContext(), PlayerInfoActivity.class);
                startActivity(gameInfo);
            }

            @Override
            public void onSwipeLeft(){
                Intent cardActivity = new Intent(getApplicationContext(), CardSelectionActivity.class);
                startActivity(cardActivity);
            }

            @Override
            public void onSwipeBottom(){
                Intent chatActivity = new Intent(getApplicationContext(), HistoryAndChatActivity.class);
                startActivity(chatActivity);
            }
            });

        BitmapFactory.Options myOptions = new BitmapFactory.Options();
        myOptions.inDither = true;
        myOptions.inScaled = false;
        myOptions.inPreferredConfig = Bitmap.Config.ARGB_8888;// important
        myOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.woodone,myOptions);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.RED);

        Bitmap workingBitmap = Bitmap.createScaledBitmap(bitmap, getWindowManager().getDefaultDisplay().getWidth(), getWindowManager().getDefaultDisplay().getHeight(), false);
        Bitmap mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true);

        screenWidth = getWindowManager().getDefaultDisplay().getWidth();
        screenHeight = getWindowManager().getDefaultDisplay().getHeight();
        gamePlaySingleton.setScreenHeight(screenHeight);
        gamePlaySingleton.setScreenWidth(screenWidth);

        mainCanvas = new Canvas(mutableBitmap);
        gamePlaySingleton.setCanvas(mainCanvas);

        for (int i =0; i < gameMap.mapCities.size(); i++)
        {
            MapCity tempCity = gameMap.mapCities.get(i);


           float  x = screenWidth * tempCity.getX() / 1800;
           float y = screenHeight * tempCity.getY() / 1000;
            paint.setColor(Color.WHITE);
            mainCanvas.drawCircle(x, y, 25, paint);
            paint.setColor(Color.BLACK);
            mainCanvas.drawCircle(x, y, 20, paint);
        }

        ImageView imageView = (ImageView)findViewById(R.id.map);
        imageView.setAdjustViewBounds(true);
        imageView.setImageBitmap(mutableBitmap);

        drawCityNames();
        update(null, null);
    }

    @Override
    public void onBackPressed(){
        Intent gameIntent = new Intent(this, GamePlayActivity.class);
        startActivity(gameIntent);
    }

    /**
     * Draws the game routes.
     * @param canvas the canvas to put the routes on.
     */
    public void drawRoutes(Canvas canvas)
    {
        List<MapRoute> routes = GameMap.getMap().mapRoutes;
        for (int i = 0; i < routes.size(); i++)
        {
            Paint paint = new Paint();
            int tempColor = getColorIntValue(routes.get(i));
            paint.setColor(tempColor);
            paint.setStrokeWidth(35);
            paint.setPathEffect(new DashPathEffect(new float[] {40,10}, 0));

            int xDiff = routes.get(i).getXPositionModifier();
            int yDiff = routes.get(i).getYPositionModifier();

            float  x1 = screenWidth * routes.get(i).getCity1().getX() / 1800 + xDiff;
            float y1 = screenHeight * routes.get(i).getCity1().getY() / 1000 + yDiff;
            float  x2 = screenWidth * routes.get(i).getCity2().getX() / 1800 + xDiff;
            float y2 = screenHeight * routes.get(i).getCity2().getY() / 1000 + yDiff;

            canvas.drawLine(
                    x1,
                    y1,
                    x2,
                    y2,
                    paint);
        }
    }

    /**
     * Draws the city names to the views.
     */
    public void drawCityNames()
    {
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.gameScreen);

        for (MapCity city :gameMap.mapCities)
        {
            Button cityButton = new Button(this);

            cityButton.getBackground().setAlpha(195);
            cityButton.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                        String cityName = (String) v.getTag();
                                        new RouteSelectedAlertDialogue(GamePlayActivity.this).displayAvailableRoutes(cityName);
                                      }
                                  }
            );

            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(115, 95);

            if (city.idTag == null)
            {
                cityButton.setText(city.getName());
            }
            else{
                cityButton.setText(city.getIdTag());
            }

            cityButton.setTag(city.getName());

            int x = (int) (screenWidth * city.getX() / 1800);
            int y = (int) (screenHeight * city.getY() / 1000);
            params.leftMargin = x;
            params.topMargin = y;
            rl.addView(cityButton, params);
        }
    }

    /**
     * Draws a line of ownership over the route.
     * @param route the route to draw over.
     */
    public void drawOwnerShip(MapRoute route) {
        //draw line of ownership over route.

        Paint paint = new Paint();
        //playerColor
        int tempColor = gamePlaySingleton.getPlayerColor();
        paint.setColor(tempColor);
        paint.setStrokeWidth(20);

        int xDiff = route.getXPositionModifier();
        int yDiff = route.getYPositionModifier();

        float x1 = gamePlaySingleton.getScreenWidth() * route.getCity1().getX() / 1800 + xDiff;
        float y1 = gamePlaySingleton.getScreenHeight() * route.getCity1().getY() / 1000 + yDiff;
        float x2 = gamePlaySingleton.getScreenWidth() * route.getCity2().getX() / 1800 + xDiff;
        float y2 = gamePlaySingleton.getScreenHeight() * route.getCity2().getY() / 1000 + yDiff;

        gamePlaySingleton.getCanvas().drawLine(
                x1,
                y1,
                x2,
                y2,
                paint);
        if (gamePlaySingleton.getView() != null) {
            gamePlaySingleton.getView().invalidate();
        }
    }

    /**
     * Show a toast to the user after attempting to claim a route.
     * @param success if the claim was successful.
     */
    public void onClaimRouteResponse(boolean success){
        if (success){
            Toast.makeText(getApplicationContext(), "Claiming Successful!", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(getApplicationContext(), "Route Not Claimed!", Toast.LENGTH_SHORT).show();
        }
    }

    public int getColorIntValue(MapRoute route)
    {
        int color = Color.rgb(255, 165, 0);
        model.Color routeColor = route.getRouteColor();
        if (routeColor.equals(model.Color.RED))
        {
           color = Color.RED;
        }
        if (routeColor.equals(model.Color.BLACK))
        {
            color = Color.BLACK;
        }
        if (routeColor.equals(model.Color.BLUE))
        {
            color = Color.BLUE;
        }

        if (routeColor.equals(model.Color.WILD))
        {
            color = Color.GRAY;
        }
        if (routeColor.equals(model.Color.GREEN))
        {
            color = Color.GREEN;
        }
        if (routeColor.equals(model.Color.PINK))
        {
            color = Color.MAGENTA;
        }
        if (routeColor.equals(model.Color.WHITE))
        {
            color = Color.WHITE;
        }
        if (routeColor.equals(model.Color.YELLOW))
        {
            color = Color.YELLOW;
        }

        return color;
    }

    @Override
    public void update(Observable o, Object arg)
    {

        TextView playerTurn = (TextView)findViewById(R.id.playerTurn);
        if (ClientModel.getInstance().currentTurnPlayer.equals("")){
            playerTurn.setText("");
        }
        else {
            playerTurn.setText("       Player " + ClientModel.getInstance().currentTurnPlayer + "\'s turn");
        }

        List<MapRoute> tempList = PreGameModelFacade.getGameByID(ClientModel.getInstance().getMyJoinedGame()).getMapRoutes();
        // andrew, how do i get the list of routes from the correct game. i update the list in game model facade, but after that i cant asses the listttyttttttttttttttttttttttttttttttttttttttttttttt
        for (model.MapRoute mapRoute : tempList)
        {
            if (mapRoute.getRouteClaimed())
            {
                Paint paint = new Paint();
                //playerColor needs to be update dynamically in next phase
                int tempColor = enumSwitch(PreGameModelFacade.getGameByID(ClientModel.getInstance().myJoinedGame).getPlayerByName(mapRoute.getOwnerID()).getColor());
                paint.setColor(tempColor);
                paint.setStrokeWidth(20);

                int xDiff = mapRoute.getXPositionModifier();
                int yDiff = mapRoute.getYPositionModifier();

                float x1 = gamePlaySingleton.getScreenWidth() * mapRoute.getCity1().getX() / 1800 + xDiff;
                float y1 = gamePlaySingleton.getScreenHeight() * mapRoute.getCity1().getY() / 1000 + yDiff;
                float x2 = gamePlaySingleton.getScreenWidth() * mapRoute.getCity2().getX() / 1800 + xDiff;
                float y2 = gamePlaySingleton.getScreenHeight() * mapRoute.getCity2().getY() / 1000 + yDiff;

                gamePlaySingleton.getCanvas().drawLine(
                        x1,
                        y1,
                        x2,
                        y2,
                        paint);
                if (gamePlaySingleton.getView() != null) {
                    gamePlaySingleton.getView().invalidate();
                }
            }
        }
        //drawRoutes(mainCanvas);
        if (mainMapView != null) {
            mainMapView.invalidate();
        }

        if(arg != null){
            Intent gameIntent = new Intent(this, GameOverActivity.class);
            startActivity(gameIntent);
        }

    }


    public int enumSwitch(Enum color)
    {
        int toReturn = android.graphics.Color.BLUE;
        if (color.equals(model.Color.PINK)) {
            toReturn = android.graphics.Color.MAGENTA;

        } else if (color.equals(model.Color.WHITE)) {
            toReturn = android.graphics.Color.WHITE;

        } else if (color.equals(model.Color.BLUE)) {
            toReturn = android.graphics.Color.BLUE;

        } else if (color.equals(model.Color.YELLOW)) {
            toReturn = android.graphics.Color.YELLOW;

        } else if (color.equals(model.Color.ORANGE)) {
            toReturn = android.graphics.Color.rgb(255, 169, 0);

        } else if (color.equals(model.Color.BLACK)) {
            toReturn = android.graphics.Color.BLACK;

        } else if (color.equals(model.Color.RED)) {
            toReturn = android.graphics.Color.RED;

        } else if (color.equals(model.Color.GREEN)) {
            toReturn = android.graphics.Color.GREEN;

        }
        return toReturn;
    }

    public void selectGrayColor(MapRoute mapRoute, String user, String gameID)
    {
        setContentView(R.layout.fragment_gray_route);
        GrayRouteFragment fragment = new GrayRouteFragment();
        fragment.setMapRoute(mapRoute);
        fragment.setGameID(gameID);
        fragment.setUser(user);
        getSupportFragmentManager().beginTransaction().replace(R.id.grayClaimRoute, fragment, fragment.getClass().getSimpleName()).commit();
    }
}
