package plugin;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import command.Command;
import command.ICommand;
import util.Serializer;

/**
 * Created by Andrew Jacobson on 8/3/2017.
 */

public class MongoGameCommandDao {

    // COLLECTION OF GAME COMMANDS FROM THE TICKET TO RIDE DATABASE
    MongoCollection<Document> gameCommandCollection;

    public MongoGameCommandDao(MongoDatabase database){
        gameCommandCollection = database.getCollection("gameCommands");
    }

    public void addCommand(ICommand command){
        String json = Serializer.toJson(command);
        gameCommandCollection.insertOne(Document.parse(json));
    }

    public void clear(){
        gameCommandCollection.drop();
    }

    public List<ICommand> getGameCommands(){
        List<ICommand> gameCommands = new ArrayList<>();

        MongoCursor<Document> cursor = gameCommandCollection.find().iterator();
        try {
            while (cursor.hasNext()) {
                String json = cursor.next().toJson();
                Command command = (Command) Serializer.fromJson(json,Command.class);
                ICommand castCommand = (ICommand) Serializer.fromJson(json ,Class.forName(command.getCommandClassName()));

                gameCommands.add(castCommand);
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }
        finally {
            cursor.close();
        }
        return gameCommands;
    }

    public List<ICommand> getGameCommandsForGame(String gameID){
        List<ICommand> gameCommands = new ArrayList<>();

        Bson filter = new Document("gameID", gameID);
        MongoCursor<Document> cursor = gameCommandCollection.find(filter).iterator();
        try {
            while (cursor.hasNext()) {
                String json = cursor.next().toJson();
                gameCommands.add((ICommand)Serializer.fromJson(json, Command.class));
            }
        } finally {
            cursor.close();
        }

        return gameCommands;
    }

    public void removeGameCommandsForGame(String gameID){
        Bson filter = new Document("gameID", gameID);
        gameCommandCollection.deleteMany(filter);
    }

}
