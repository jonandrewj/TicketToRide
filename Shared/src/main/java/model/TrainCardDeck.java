package model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by deriktj on 7/16/2017.
 */

public class TrainCardDeck
{
    public final static int DECK_INDEX = 5;
    // Normal deck of train cards. cards.get(5) is the top card of the deck, and cards.get(0-4) are the faceup cards.
    private List<TrainCard> cards;

    private int deckSize;

    // Discard deck. gets reshuffled when cards.size() < 6
    private List<TrainCard> discard;

    public TrainCardDeck(int wildCount, int normalTrainCount) {
        deckSize = wildCount + normalTrainCount * 8;
        cards = new ArrayList<>();
        discard = new ArrayList<>();
        for(Color color : Color.values()) {
            if(color == Color.WILD) {
                for(int i = 0; i < wildCount; i++) {
                    discard.add(new TrainCard(color));
                }
            }
            else {
                for(int i = 0; i < normalTrainCount; i++) {
                    discard.add(new TrainCard(color));
                }
            }
        }
        shuffleDeck();
    }

    public void shuffleDeck() {
        Collections.shuffle(discard);
        for(TrainCard card : discard) {
            cards.add(card);
        }
        discard.clear();
    }

    public void addToDiscard(Color color, int count)
    {
        for(int i = 0; i < count; i++) {
            discard.add(new TrainCard(color));
        }
    }

    public int getDeckSize() {
        return deckSize;
    }

    public void setDeckSize(int deckSize)
    {
        this.deckSize = deckSize;
    }

    public List<TrainCard> getDeck() {
        return cards;
    }

    public TrainCard draw(int index) throws IndexOutOfBoundsException {
        if(index > 5) {
            throw new IndexOutOfBoundsException();
        }
        TrainCard drawnCard = cards.get(index);
        cards.set(index,cards.get(5));
        cards.remove(5);
        /*
        if(cards.size() < 8) {
            shuffleDeck();
        }
        */
        return drawnCard;
    }

    public TrainCard getCardAtIndex(int index) throws IndexOutOfBoundsException {
        if(index > 5) {
            throw new IndexOutOfBoundsException();
        }
        if(cards.size() <= index) {
            return null;
        }
        TrainCard cardAtIndex = cards.get(index);
        return cardAtIndex;
    }

    public void setCardAtIndex(int index, TrainCard card) throws IndexOutOfBoundsException {
        if(index > 5) {
            throw new IndexOutOfBoundsException();
        }

        if(cards.size() <= index) {
            cards.add(index,card);
        }
        else {
            cards.set(index,card);
        }
    }

    public Boolean shouldShuffle() {
        return cards.size() < 2;
    }

}
