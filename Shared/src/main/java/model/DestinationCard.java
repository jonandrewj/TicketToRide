package model;

/**
 * Created by deriktj on 7/16/2017.
 */

public class DestinationCard
{
    private final String city1;
    private final String city2;
    private final int points;
    private Boolean canBeDiscarded;
    private Boolean isSelectedToDiscard;

    public DestinationCard(String city1, String city2, int points) {
        this.city1 = city1;
        this.city2 = city2;
        this.points = points;
        this.canBeDiscarded = true;
        this.isSelectedToDiscard = false;
    }

    public Boolean canBeDiscarded() {
        return canBeDiscarded;
    }

    public void setCanBeDiscarded(Boolean value) {
        canBeDiscarded = value;
    }

    public String getCity1()
    {
        return city1;
    }

    public String getCity2()
    {
        return city2;
    }

    public int getPoints()
    {
        return points;
    }

    public Boolean getSelectedToDiscard()
    {
        return isSelectedToDiscard;
    }

    public void setSelectedToDiscard(Boolean selectedToDiscard)
    {
        isSelectedToDiscard = selectedToDiscard;
    }

    @Override
    public boolean equals(Object o) {
        if(!(o instanceof DestinationCard)) {
            return false;
        }
        DestinationCard other = (DestinationCard) o;
        if(getCity1().equals(other.getCity1()) && getCity2().equals(other.getCity2()) && getPoints() == other.getPoints()){
            return true;
        }
        return false;
    }
}
