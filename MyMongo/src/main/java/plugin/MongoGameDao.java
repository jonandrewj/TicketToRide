package plugin;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import model.Game;
import util.Serializer;

/**
 * Created by Andrew Jacobson on 8/3/2017.
 */

public class MongoGameDao {

    // COLLECTION OF GAMES FROM THE TICKETTORIDE DATABASE
    MongoCollection<Document> gameCollection;

    public MongoGameDao(MongoDatabase database){
        gameCollection = database.getCollection("games");
    }

    public void updateGame(Game game){

        // REMOVE FROM DATABASE
        Bson filter = new Document("gameID", game.getGameID());
        gameCollection.deleteOne(filter);

        // RE-ADD
        String json = Serializer.toJson(game);
        gameCollection.insertOne(Document.parse(json));
    }

    public List<Game> getGames(){
        List<Game> games = new ArrayList<>();

        MongoCursor<Document> cursor = gameCollection.find().iterator();
        try {
            while (cursor.hasNext()) {
                String json = cursor.next().toJson();
                games.add((Game)Serializer.fromJson(json, Game.class));
            }
        } finally {
            cursor.close();
        }
        return games;
    }

    public void clear(){
        gameCollection.drop();
    }
}
