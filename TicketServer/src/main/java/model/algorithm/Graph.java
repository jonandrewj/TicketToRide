package model.algorithm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by deriktj on 8/1/2017.
 */

public class Graph {
    private List<Node> nodeList;

    public Graph() {
        nodeList = new ArrayList<>();
    }

    public void addNode(String name) {
        for(Node node : nodeList) {
            if(node.getName().equals(name)) {
                return;
            }
        }
        nodeList.add(new Node(name));
    }

    /**
     * @pre both cities specified by edge must already exist in the graph
     * @param city1
     * @param city2
     * @param length
     */
    public void addEdge(String city1, String city2, int length) {
        Edge edge = new Edge(getNode(city1), getNode(city2), length);
        getNode(city1).addEdge(edge);
        getNode(city2).addEdge(edge);
    }

    public Node getNode(String name) {
        for(Node node : nodeList) {
            if(node.getName().equals(name)) {
                return node;
            }
        }
        return null;
    }

    public Node getSomeNode() {
        return nodeList.get(0);
    }

    public void findFurthestNode(Node currentNode,Integer currentLength, FurthestRoute furthestRoute) {
        for(Edge edge : currentNode.getEdges()) {
            if(edge.hasBeenTaken()) {
                continue;
            }
            edge.setBeenTaken(true);
            currentLength += edge.getLength();
            Node nextNode = currentNode.takeEdge(edge);
            if(currentLength > furthestRoute.furthestLength) {
                furthestRoute.furthestLength = currentLength;
                furthestRoute.furthestNode = nextNode;
            }
            findFurthestNode(nextNode,currentLength,furthestRoute);
            edge.setBeenTaken(false);
            currentLength -= edge.getLength();
        }
    }

    public List<Graph> splitOnConnectivity() {
        List<Graph> listOfSubGraphs = new ArrayList<>();
        for(Node node : nodeList) {
            if(node.getConnected()) { continue; }
            Graph subGraph = new Graph();
            findConnections(node,subGraph);
            listOfSubGraphs.add(subGraph);
        }
        return listOfSubGraphs;
    }

    private void findConnections(Node node, Graph subGraph) {
        if(node.getConnected()) {
            return;
        }
        node.setConnected(true);
        for(Edge edge : node.getEdges()) {
            subGraph.addNode(edge.getTo().getName());
            subGraph.addNode(edge.getFrom().getName());
            subGraph.addEdge(edge.getFrom().getName(),edge.getTo().getName(),edge.getLength());
            findConnections(node.takeEdge(edge),subGraph);
        }
    }

    public boolean containsBoth(String city1, String city2)
    {
        Boolean firstFound = false;
        Boolean secondFound = false;
        for(Node node : nodeList) {
            if(node.getName().equals(city1)) {
                firstFound = true;
            }
            else if(node.getName().equals(city2)) {
                secondFound = true;
            }
        }
        return firstFound && secondFound;
    }

    protected static class Node {
        private String name;
        private List<Edge> edges;
        private Boolean connected;

        public Node(String name) {
            this.connected = false;
            this.name = name;
            edges = new ArrayList<>();
        }

        public String getName()
        {
            return name;
        }

        public void addEdge(Edge edge) {
            edges.add(edge);
        }

        public List<Edge> getEdges() {
            return edges;
        }

        public Node takeEdge(Edge edge) {
            if(this == edge.from) {
                return edge.getTo();
            }
            else {
                return edge.getFrom();
            }
        }

        public Boolean getConnected()
        {
            return connected;
        }

        public void setConnected(Boolean connected)
        {
            this.connected = connected;
        }
    }



    protected static class Edge {
        private Node to;
        private Node from;
        private int length;
        private Boolean taken;

        public Edge(Node to, Node from, int length) {
            this.to = to;
            this.from = from;
            this.length = length;
            this.taken = false;
        }

        public Node getTo() {
            return to;
        }

        public Node getFrom()
        {
            return from;
        }

        public int getLength() {
            return length;
        }

        public Boolean hasBeenTaken()
        {
            return taken;
        }

        public void setBeenTaken(Boolean taken)
        {
            this.taken = taken;
        }
    }

    protected static class FurthestRoute {
        public Node furthestNode;
        public Integer furthestLength;
    }
}



