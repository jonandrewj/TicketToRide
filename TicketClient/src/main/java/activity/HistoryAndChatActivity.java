package activity;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import cs340.byu.tickettoride.R;
import fragment.CardSelectionFragment;
import fragment.HistoryAndChatFragment;

public class HistoryAndChatActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_and_chat);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.chatHistoryFragContainer, new HistoryAndChatFragment()).commit();
    }
}
